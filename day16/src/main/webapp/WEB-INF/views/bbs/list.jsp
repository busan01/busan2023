<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/body.css">
<style type="text/css">
.container{}
.container table{
	border-collapse: collapse;border: 1px solid gray;
	width: 900px;margin: 10px auto;
}
.container table thead{
	background-color: gray;
	color:white;
}
.container table tr>th{border: 1px solid gray;}
.container table tr>td:nth-child(1){width: 80px;}
.container table tr>td:nth-child(3){width: 100px;}
.container table tr>td:nth-child(4){width: 120px;}
.container table tr>td{border: 1px solid gray;}
.container table tr>td>a{display: block;text-decoration: none;color:#999999;}
.container table+div>a{
	border: 1px solid gray; width: 900px; height: 35px; color:white; background-color: gray;
	border-radius: 20px; text-align: center; line-height: 35px; display: block; margin: 10px auto;
	text-decoration: none;
}
</style>
</head>
<body>
<nav>
	<div>
		<h1>비트교육센터</h1>
	</div>
	<ul>
		<li><a href="../index.do">HOME</a></li>
		<li><a href="list.do">BBS</a></li>
		<li><a href="../login/form.do">LOGIN</a></li>
	</ul>
	<form>
		<label>id</label><input/>
		<label>pw</label><input type="password"/>
		<button>login</button>
		<button type="button">join</button>
	</form>
</nav>
<div class="container">
	<h2>게시판</h2>
	<table>
		<thead>
			<tr>
				<th>num</th>
				<th>subject</th>
				<th>id</th>
				<th>date</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${list }" var="bean">
			<c:url value="detail.do" var="detail">
				<c:param name="num" value="${bean.num }"></c:param>
			</c:url>
			<tr>
				<td><a href="${detail }">${bean.num}</a></td>
				<td><a href="${detail }">${bean.sub }</a></td>
				<td><a href="${detail }">${bean.id }</a></td>
				<td><a href="${detail }">
					<fmt:formatDate value="${bean.nalja }" pattern="yy.MM.dd hh:mm"/>
					</a>
				</td>
			</tr>
			</c:forEach>		
		</tbody>
	</table>
	<div><a href="add.do">입력</a></div>
</div>
</body>
</html>