package com.bit.bbs02.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bit.bbs02.model.Bbs02Dao;
import com.bit.framework.Controller;

public class AddController implements Controller {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if("GET".equals(req.getMethod())) {
			return "bbs/add";
		}
		String sub=req.getParameter("sub");
		String id=req.getParameter("id");
		String content=req.getParameter("content");
		Bbs02Dao dao=new Bbs02Dao();
		try {
			dao.insertOne(id, sub, content);
		} catch (SQLException e) {}
		return "redirect:list.do";
	}
}
