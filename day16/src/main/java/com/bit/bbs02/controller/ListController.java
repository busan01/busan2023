package com.bit.bbs02.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bit.bbs02.model.Bbs02Dao;
import com.bit.framework.Controller;

public class ListController implements Controller {

	public String execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Bbs02Dao dao=new Bbs02Dao();
		try {
			req.setAttribute("list", dao.selectAll());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "bbs/list";
	}
}
