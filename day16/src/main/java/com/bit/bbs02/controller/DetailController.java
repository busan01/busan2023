package com.bit.bbs02.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bit.bbs02.model.Bbs02Dao;
import com.bit.framework.Controller;

public class DetailController implements Controller {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int num=Integer.parseInt(req.getParameter("num"));
		Bbs02Dao dao=new Bbs02Dao();
		try {
			req.setAttribute("bean", dao.selectOne(num));
			req.setAttribute("view", "edit");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "bbs/add";
	}

}
