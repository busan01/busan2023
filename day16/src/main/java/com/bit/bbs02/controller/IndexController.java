package com.bit.bbs02.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bit.framework.Controller;

public class IndexController implements Controller {
	
	public IndexController() {
		System.out.println("create object");
	}

	public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		return "main";
	}
}
