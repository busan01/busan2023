package com.bit.bbs02.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.bit.framework.JdbcTemplate;
import com.bit.framework.RowMapper;
import com.mysql.cj.jdbc.MysqlDataSource;

public class Bbs02Dao {
	
	DataSource dataSource;
	JdbcTemplate<Bbs02Dto> template=new JdbcTemplate<>();

	public Bbs02Dao() {
		String url="jdbc:mysql://localhost:3306/xe";
		String id="scott";
		String pw="tiger";
		MysqlDataSource dataSource=new MysqlDataSource();
		dataSource.setUrl(url);
		dataSource.setUser(id);
		dataSource.setPassword(pw);
		this.dataSource=dataSource;
	}
	
	public List<Bbs02Dto> selectAll() throws SQLException{
		String sql="select * from bbs01 order by num desc";
		template.setDataSource(dataSource);
		return template.executeList(sql,new RowMapper<Bbs02Dto>() {
			
			@Override
			public Bbs02Dto mapper(ResultSet rs) throws SQLException {
				return new Bbs02Dto(
						rs.getInt("num")
						,rs.getString("id")
						,rs.getString("sub")
						,null
						,rs.getTimestamp("nalja")
						);
			}
		});
	}
	
	public Bbs02Dto selectOne(int num) throws SQLException {
		String sql="select * from bbs01 where num=?";
		template.setDataSource(dataSource);
		RowMapper<Bbs02Dto> mapper=new RowMapper<Bbs02Dto>() {
			
			@Override
			public Bbs02Dto mapper(ResultSet rs) throws SQLException{
				return new Bbs02Dto(
						rs.getInt("num")
						,rs.getString("id")
						,rs.getString("sub")
						,rs.getString("content")
						,rs.getTimestamp("nalja")
						);
			}
		};
		return template.executeOne(sql,mapper, num);
	}
	
	public int insertOne(String id,String sub,String content) throws SQLException {
		String sql="insert into bbs01 (id,sub,content,nalja) values (?,?,?,now())";
		template.setDataSource(dataSource);
		return template.executeUpdate(sql,id,sub,content);
	}
	
	public int updateOne(int num,String sub,String content) throws SQLException {
		String sql="update bbs01 set sub=?,content=? where num=?";
		template.setDataSource(dataSource);
		return template.executeUpdate(sql,sub,content,num);
	}
	
	public int deleteOne(int num) throws SQLException {
		String sql="delete from bbs01 where num=?";
		template.setDataSource(dataSource);
		return template.executeUpdate(sql,num);
	}
	
}
