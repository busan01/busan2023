package com.bit.bbs02.model;

import java.sql.SQLException;
import java.util.List;

public class TestDao {
	
	public static void testSelectAll() throws SQLException {
		Bbs02Dao dao=new Bbs02Dao();
//		for(Bbs02Dto bean: dao.selectAll())
//			System.out.println(bean);
		System.out.println(dao.selectAll()!=null);
		System.out.println(dao.selectAll().getClass()==java.util.ArrayList.class);
		System.out.println(dao.selectAll().size()>0);
	}
	private static void testSelectOne() throws SQLException {
		//Bbs02Dto [num=1, id=guest, sub=sub01, content=]
		Bbs02Dto dummy=new Bbs02Dto(1,"guest","sub01","",null);
		Bbs02Dao dao=new Bbs02Dao();
		Bbs02Dto bean=dao.selectOne(dummy.getNum());
		System.out.println(bean!=null);
		System.out.println(dummy.equals(bean));
		
	}
	private static void testInsertOne() throws SQLException {
		Bbs02Dao dao=new Bbs02Dao();
		int before=dao.selectAll().size();
		int result=dao.insertOne("tester", "test", "test");
		int after=dao.selectAll().size();
		System.out.println(result>0);
		System.out.println(before+1==after);
	}

	public static void main(String[] args) throws SQLException {
		testInsertOne();
	}


}
