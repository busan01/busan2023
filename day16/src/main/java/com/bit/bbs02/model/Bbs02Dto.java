package com.bit.bbs02.model;

import java.sql.Timestamp;
import java.util.Objects;

public class Bbs02Dto {

	private int num;
	private String id,sub,content;
	private Timestamp nalja;
	
	public Bbs02Dto() {
	}

	public Bbs02Dto(int num, String id, String sub, String content, Timestamp nalja) {
		this.num = num;
		this.id = id;
		this.sub = sub;
		this.content = content;
		this.nalja = nalja;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getNalja() {
		return nalja;
	}

	public void setNalja(Timestamp nalja) {
		this.nalja = nalja;
	}

	@Override
	public int hashCode() {
		return Objects.hash(content, id, num, sub);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bbs02Dto other = (Bbs02Dto) obj;
		return Objects.equals(content, other.content) && Objects.equals(id, other.id) && num == other.num
				&& Objects.equals(sub, other.sub);
	}

	@Override
	public String toString() {
		return "Bbs02Dto [num=" + num + ", id=" + id + ", sub=" + sub + ", content=" + content + "]";
	}
	
}
