package com.bit.framework;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

public class JdbcTemplate<T> {
	DataSource dataSource;
	
	public JdbcTemplate() {
	}
	
	
	public JdbcTemplate(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}


	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public int executeUpdate(String sql,Object ... args) throws SQLException {
		Connection conn=null;
		PreparedStatement pstmt=null;
		try {
			conn=dataSource.getConnection();
			pstmt=conn.prepareStatement(sql);
			int cnt=1;
			for(Object ele:args) pstmt.setObject(cnt++, ele);
			return pstmt.executeUpdate();
		}finally {
			close(conn,pstmt,null);
		}
	}
	
	public T executeOne(String sql,RowMapper<T> rowMapper) throws SQLException{
		return executeList(sql,rowMapper).get(0);
	}

	public T executeOne(String sql,RowMapper<T> rowMapper,Object ... args) throws SQLException{
		return executeList(sql,rowMapper,args).get(0);
	}

	public List<T> executeList(String sql,RowMapper<T> rowMapper) throws SQLException{
		return executeList(sql,rowMapper,new Object[] {});
	}
	public List<T> executeList(String sql,RowMapper<T> rowMapper,Object ... args) throws SQLException{
		List<T> list=new ArrayList<>();
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			conn=dataSource.getConnection();
			pstmt=conn.prepareStatement(sql);
			for(int i=0; i<args.length; i++)
				pstmt.setObject(i+1, args[i]);
			rs=pstmt.executeQuery();
			while(rs.next())
				list.add(rowMapper.mapper(rs));
		}finally {
			close(conn,pstmt,rs);
		}
		
		return list;
	}
	
	public void close(Connection conn,PreparedStatement pstmt, ResultSet rs) throws SQLException {
		if(rs!=null)rs.close();
		if(pstmt!=null)pstmt.close();
		if(conn!=null)conn.close();
	}
}
