package com.bit.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DispatcherServlet extends HttpServlet {
	Map<String,Controller> handler=new HashMap<>();
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
//		Map<String,String> mapping=new HashMap<>();
		java.util.Properties mapping=new Properties();
		String servletName=config.getServletName();
		String path=config.getInitParameter(servletName);
		try {
			URL url=null;
			if(path==null) // src/main/resources에서 로딩
				url=config.getServletContext().getClassLoader().getResource(servletName+".properties");
			else // initparam에서 주어진 경로로 로딩
				url=new File(path).toURL();
			mapping.load(url.openStream());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Set<Entry<Object, Object>> set = mapping.entrySet();
		for(Entry<Object, Object> entry:set) {
			try {
				Class clz=Class.forName((String)entry.getValue());
				Controller controller=(Controller)clz.newInstance();
				handler.put((String)entry.getKey(), controller);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doDo(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doDo(req, resp);
	}
	
	protected void doDo(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// handlerMapping
		String url=req.getRequestURI();
		url=url.substring(req.getContextPath().length());
		Controller controller=handler.get(url);
		String viewName=controller.execute(req, resp);
		
		
		// ViewRosolver
		if(viewName.startsWith("redirect:")) {
			resp.sendRedirect(viewName.substring("redirect:".length()));
			return;
		}
		String prefix="/WEB-INF/views/";
		String suffix=".jsp";
		
		req.getRequestDispatcher(prefix+viewName+suffix).forward(req, resp);
	}

}








