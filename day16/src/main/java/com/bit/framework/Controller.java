package com.bit.framework;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


// command 디자인 패턴
public interface Controller {

	String execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;

}