package com.bit.sts04.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RootController {

	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("menu", "home");
		return "index";
	}
	
	@RequestMapping("/intro")
	public void intro(Model model) {
		model.addAttribute("menu", "intro");
	}
}
