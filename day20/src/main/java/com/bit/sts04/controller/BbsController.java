package com.bit.sts04.controller;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bit.sts04.model.Bbs01Vo;
import com.bit.sts04.service.Bbs01ServiceImpl;

@Controller
@RequestMapping("/bbs/")
public class BbsController {
	Logger log=LoggerFactory.getLogger(getClass());
	@Autowired
	Bbs01ServiceImpl service;
	
	@RequestMapping(value = "",method=RequestMethod.GET)
	public String list(Model model) throws SQLException {
		model.addAttribute("menu", "bbs");
		service.bbs01List(model);
		return "bbs/list";
	}
	@RequestMapping(value = "",method=RequestMethod.POST)
	public String add(@ModelAttribute Bbs01Vo bean) throws SQLException {
		log.debug(bean.toString());
		service.addBbs01(bean);
		return "redirect:./";
	}
	
	@RequestMapping("add")
	public void add(Model model) {
		model.addAttribute("menu", "bbs");
	}
	
	@RequestMapping(value = "{num}",method = RequestMethod.GET)
	public String detail(@PathVariable int num,Model model) throws SQLException {
		log.debug("param:"+num);
		model.addAttribute("menu", "bbs");
		model.addAttribute("bean", service.listOne(num));
		return "bbs/edit";
	}
	
	@RequestMapping(value = "{num}",method = RequestMethod.PUT)
	public String detail(@PathVariable int num,@ModelAttribute Bbs01Vo bean) throws SQLException {
		log.debug(bean.toString());
		service.editBbs01(bean);
		return "redirect:./";
	}

	@RequestMapping(value = "{num}",method = RequestMethod.DELETE)
	public String delete(@PathVariable int num) throws SQLException {
		log.debug("delete:"+num);
		service.deleteBbs01(num);
		return "redirect:./";
	}
	
}







