package com.bit.sts04.module;

public interface MyModule {

	void func01();
	String func02();
	void func03(String msg);
	void func04() throws Exception;
}
