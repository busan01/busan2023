package com.bit.sts04.module;

public class MyModule01 implements MyModule {

	@Override
	public void func01() {
		System.out.println("f1 핵심기능");
	}

	@Override
	public String func02() {
		String msg="f2 핵심기능";
		System.out.println(msg);
		return msg;
	}

	@Override
	public void func03(String msg) {
		System.out.println("f3 param 메시지:"+msg);
	}

	@Override
	public void func04() throws Exception {
		System.out.println("에러발생전");
		throw new Exception("사용자 정의 에러 발생...");
	}

}
