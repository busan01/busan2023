package com.bit.sts04.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

@Repository
public class Bbs01DaoImpl implements Bbs01Dao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	RowMapper<Bbs01Vo> rowMapper=new RowMapper<Bbs01Vo>() {

		@Override
		public Bbs01Vo mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Bbs01Vo(rs.getInt(1),rs.getString(2)
					,rs.getString(3),rs.getString(4),rs.getTimestamp(5));
		}
	};

	@Override
	public List<Bbs01Vo> selectAll() throws SQLException {
		String sql="select * from bbs01";
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public Bbs01Vo selectOne(int num) throws SQLException {
		String sql="select * from bbs01 where num=?";
		return jdbcTemplate.queryForObject(sql, rowMapper,num);
	}
	
	@Override
	public void nextVal() throws SQLException {
		String sql="UPDATE bbs01seq SET seq_num=(SELECT * FROM (SELECT seq_num+1 FROM bbs01seq ) A) WHERE 1=1";
		jdbcTemplate.update(sql);
	}

	@Override
	public void insertOne(Bbs01Vo bean) throws SQLException {
		String sql="INSERT INTO bbs01 SELECT * FROM (SELECT seq_num ,?,?,?,NOW() FROM bbs01seq A)b";
		jdbcTemplate.update(sql,bean.getId(),bean.getSub(),bean.getContent());
	}

	@Override
	public int updateOne(Bbs01Vo bean) throws SQLException {
		String sql="update bbs01 set sub=?,content=? where num=?";
		return jdbcTemplate.update(sql,bean.getSub(),bean.getContent(),bean.getNum());
	}

	@Override
	public int deleteOne(int num) throws SQLException {
		String sql="delete from bbs01 where num=?";
		return jdbcTemplate.update(sql,num);
	}

	@Override
	public int maxNum() throws SQLException {
		String sql="select count(*) from bbs01";
		return jdbcTemplate.queryForInt(sql);
	}

}
