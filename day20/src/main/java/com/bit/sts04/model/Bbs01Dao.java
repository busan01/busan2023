package com.bit.sts04.model;

import java.sql.SQLException;
import java.util.List;

public interface Bbs01Dao {
	
	List<Bbs01Vo> selectAll() throws SQLException;
	Bbs01Vo selectOne(int num) throws SQLException;
	void insertOne(Bbs01Vo bean) throws SQLException;
	int updateOne(Bbs01Vo bean) throws SQLException;
	int deleteOne(int num) throws SQLException;
	int maxNum() throws SQLException;
	void nextVal() throws SQLException;
}
