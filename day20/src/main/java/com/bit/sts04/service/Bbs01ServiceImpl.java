package com.bit.sts04.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.bit.sts04.model.Bbs01Dao;
import com.bit.sts04.model.Bbs01Vo;

@Service
public class Bbs01ServiceImpl {
	@Autowired
	Bbs01Dao bbs01Dao;
	
	public void bbs01List(Model model) throws SQLException {
		model.addAttribute("list", bbs01Dao.selectAll());
		model.addAttribute("tot", bbs01Dao.maxNum());
	}

	public void addBbs01(Bbs01Vo bean) throws SQLException {
		bbs01Dao.nextVal();
		bbs01Dao.insertOne(bean);
	}

	public Bbs01Vo listOne(int num) throws SQLException {
		return bbs01Dao.selectOne(num);
	}
	
	public void editBbs01(Bbs01Vo bean) throws SQLException {
		bbs01Dao.updateOne(bean);
	}
	
	public void deleteBbs01(int num) throws SQLException {
		bbs01Dao.deleteOne(num);
	}
}
