package com.bit.sts04.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;

public class MyAdvice {

	public void beforeTargetMethod(JoinPoint jp) {
		Object[] args = jp.getArgs();
		Object obj = jp.getTarget();
		String methodName = jp.getSignature().getName();
        System.out.println(methodName+","+Arrays.toString(args)+","+obj);
    }
	
	public void afterReturningTargetMethod(JoinPoint thisJoinPoint,
            Object retVal) {
		System.out.println("after success return :"+retVal);
    }
	
	public void afterThrowingTargetMethod(JoinPoint thisJoinPoint,
            Exception ex) throws Exception {
		System.out.println(ex.getMessage());
    }
	
	public void afterTargetMethod(JoinPoint thisJoinPoint) {
	    System.out.println("finally Ⱦ�ܰ��ɻ�...");
	}
}





