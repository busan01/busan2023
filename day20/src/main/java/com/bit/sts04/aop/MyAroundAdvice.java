package com.bit.sts04.aop;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;

import com.bit.sts04.module.MyModule;

public class MyAroundAdvice {

	public Object aroundTargetMethod(ProceedingJoinPoint pjp)
            throws Throwable {
		Object obj=null;
		String name=pjp.getSignature().getName();
		Object[] args = pjp.getArgs();
		MyModule module=(MyModule) pjp.getTarget();
		System.out.println(module);
		System.out.println(name+" around before :"+Arrays.toString(args));
		try {
			obj=pjp.proceed();
			System.out.println(name+" around after returnning :"+obj);
		}catch (Exception e) {
			System.out.println(name+" around after throwing msg :"+e.getMessage());
		}finally {
			System.out.println(name+" around after");
		}
		return obj;
    }
}
