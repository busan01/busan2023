<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="layout/head.jspf" %>
</head>
<body>
<%@ include file="layout/menu.jspf" %>
<div class="jumbotron">
  <h1>Intro page!</h1>
  <p>오시는 길 소개</p>
</div>
<%@ include file="layout/footer.jspf" %>
</body>
</html>