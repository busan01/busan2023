<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../layout/head.jspf" %>
<style type="text/css">
.table tr>th:nth-child(1),
.table tr>th:nth-child(3),
.table tr>th:nth-child(4)
{
width: 100px;
}
</style>
</head>
<body>
<%@ include file="../layout/menu.jspf" %>
<ol class="breadcrumb">
  <li><a href="${root }">Home</a></li>
  <li class="active">Bbs</li>
</ol>
<div class="page-header">
  <h1>게시판 <small>${tot }</small></h1>
</div>
<table class="table table-striped">
	<thead>
		<tr>
			<th>글번호</th>
			<th>제목</th>
			<th>글쓴이</th>
			<th>날짜</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${list }" var="bean">
		<tr>
			<td><a href="./${bean.num }">${bean.num }</a></td>
			<td><a href="./${bean.num }">${bean.sub }</a></td>
			<td><a href="./${bean.num }">${bean.id }</a></td>
			<td><a href="./${bean.num }">${bean.nalja }</a></td>
		</tr>
		</c:forEach>
	</tbody>
</table>
<%@ include file="../layout/footer.jspf" %>
</body>
</html>