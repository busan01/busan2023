<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../layout/head.jspf" %>
</head>
<body>
<%@ include file="../layout/menu.jspf" %>
<ol class="breadcrumb">
  <li><a href="${root }">Home</a></li>
  <li><a href="${root }bbs/">Bbs</a></li>
  <li class="active">add</li>
</ol>
<div class="page-header">
  <h1>입력페이지</h1>
</div>
<form action="./" method="post" class="form-horizontal">
  <div class="form-group">
    <label for="sub" class="col-sm-2 control-label">제목</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="sub" id="sub" placeholder="제목">
    </div>
  </div>
  <div class="form-group">
    <label for="id" class="col-sm-2 control-label">글쓴이</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="id" id="id" placeholder="글쓴이">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <textarea name="content" class="form-control"></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
	    <div class="btn-group btn-group-justified" role="group" aria-label="...">
	      <div class="btn-group" role="group" aria-label="...">
	      	<button type="submit" class="btn btn-primary">입력</button>
	      </div>
	      <div class="btn-group" role="group" aria-label="...">
	      	<button type="reset" class="btn btn-default">취소</button>
	      </div>
	      <div class="btn-group" role="group" aria-label="...">
	      	<button type="button" class="btn btn-default" onclick="history.back();">뒤로</button>
	      </div>
	    </div>
    </div>
  </div>
</form>
<%@ include file="../layout/footer.jspf" %>
</body>
</html>