<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../layout/head.jspf" %>
<style type="text/css">
.modal form{
	display: inline;
}
</style>
<script type="text/javascript">
$(()=>{
	$('.form-horizontal .btn-group>.btn-group').eq(2).hide();
	$('.form-horizontal').one('submit',e=>{
		$(e.target).prev().children().first().text('수정페이지');
		$(e.target).find('input').eq(2).removeProp('readonly');
		$(e.target).find('textarea').removeProp('readonly');
		$('.form-horizontal .btn-group>.btn-group').eq(1).hide();
		$('.form-horizontal .btn-group>.btn-group').eq(2).show();
		return false;
	});
});

</script>
</head>
<body>
<%@ include file="../layout/menu.jspf" %>
<ol class="breadcrumb">
  <li><a href="${root }">Home</a></li>
  <li><a href="${root }bbs/">Bbs</a></li>
  <li class="active">add</li>
</ol>
<div class="page-header">
  <h1>상세페이지</h1>
</div>
<form method="post" class="form-horizontal">
	<input type="hidden" name="_method" value="put">
	<input type="hidden" name="num" value="${bean.num }">
  <div class="form-group">
    <label for="sub" class="col-sm-2 control-label">제목</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="sub" id="sub" value="${bean.sub }" readonly>
    </div>
  </div>
  <div class="form-group">
    <label for="id" class="col-sm-2 control-label">글쓴이</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" name="id" id="id" value="${bean.id }" readonly>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <textarea name="content" class="form-control" readonly>${bean.content }</textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
	    <div class="btn-group btn-group-justified" role="group" aria-label="...">
	      <div class="btn-group" role="group" aria-label="...">
	      	<button type="submit" class="btn btn-primary">수정</button>
	      </div>
	      <div class="btn-group" role="group" aria-label="...">
	      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">삭제</button>
	      </div>
	      <div class="btn-group" role="group" aria-label="...">
	      	<button type="reset" class="btn btn-default">취소</button>
	      </div>
	      <div class="btn-group" role="group" aria-label="...">
	      	<button type="button" class="btn btn-default" onclick="history.back();">뒤로</button>
	      </div>
	    </div>
    </div>
  </div>
</form>
<%@ include file="../layout/footer.jspf" %>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">${bean.num }번 글을 삭제하시겠습니까?</h4>
      </div>
      <div class="modal-footer">
      	<form method="post">
      	<input type="hidden" name="_method" value="delete">
      	<input type="hidden" name="num" value="${bean.num }">
        <button type="submit" class="btn btn-danger">삭제</button>
      	</form>
        <button type="button" class="btn btn-default" data-dismiss="modal">취소</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>