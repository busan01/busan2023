import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from './counterSlice'
import MyButton from './MyButton'

export function Counter() {
  const count = useSelector((state) => state.counter.value)
  const dispatch = useDispatch()
  const incBack=useCallback(() => dispatch(increment()),[]);
  const decBack=useCallback(() => dispatch(decrement()),[]);
  return (
    <div>
      <div>
        <span>su:{count}</span>
        <MyButton msg={'Increment'} callback={incBack}/>
        <MyButton msg={'Decrement'} callback={decBack}/>
      </div>
    </div>
  )
}