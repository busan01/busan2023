import React, { memo, useEffect } from 'react'

function MyButton({msg,callback}) {
    useEffect(()=>{
        console.log('rander...')
    });
  return (
    <button aria-label={msg+" value"} onClick={callback}>
        {msg}
    </button>
  )
}

export default memo(MyButton);