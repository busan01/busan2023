var events = require('events');
var eventEmitter = new events.EventEmitter();

//ord
eventEmitter.addListener('ev1',function(){console.log('call ev1');});
//new
eventEmitter.on('scream', function(){console.log('call on');});

eventEmitter.emit('ev1');
eventEmitter.emit('ev1');
eventEmitter.emit('scream');