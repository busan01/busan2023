var express=require('express');
var router=express.Router();
var mysql = require('mysql2');// 8.0 
// var mysql = require('mysql');// 5.x 

var conn = mysql.createConnection({
    host: "localhost",
    user: process.env.MYSQL_USER||'scott',
    password: process.env.MYSQL_PW||'1234',
    database:'xe'
});


router.get('/',(req,res)=>{
    conn.connect(function(err) {
        if (err) throw err;
        var sql='select * from bbs03';
        conn.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Result: " + result);
            res.json(result);
          });
    });
});
router.get('/:num',(req,res)=>{
    var param=req.params.num;
    conn.connect(function(err) {
        if (err) throw err;
        var sql='select * from bbs03 where num=?';
        conn.query(sql,[param] ,function (err, result) {
            if (err) throw err;
            console.log("Result: " + result);
            if(result)
            res.json(result[0]);
            else
            res.json([]);
        });
    });
});

router.post('/',(req,res)=>{
    const sub=req.body.sub;
    const content=req.body.content;
    const id=req.body.id;
    // var sql=`INSERT INTO bbs03 (sub,content,id) VALUES ('${sub}','${content}','${id}')`;
    var sql=`INSERT INTO bbs03 (sub,content,id) VALUES (?,?,?)`;
    console.log(sub,content,sql);
    conn.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
        conn.query(sql,[sub,content,id], function (err, result) {
          if (err) throw err;
          console.log("1 record inserted");
          res.sendStatus(201)
        });
      });
});
module.exports=router;