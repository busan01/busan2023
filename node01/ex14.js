const fs = require('fs');
const express = require('express');
const app = express();
const path = require('path');


app.use(express.static(path.resolve(__dirname, 'public')));
// app.get('/', (req, res) => {
//     res.send(fs.readFileSync('./public/index.html',{encoding:'utf-8'}))
//   });
// app.post('/', (req, res) => {
// res.send('Got a POST request')
// });

app.put('/user/1', (req, res) => {
res.send('Got a PUT request at /user')
});
app.delete('/user/1', (req, res) => {
res.send('Got a DELETE request at /user')
});

app.use('/ex01',(req,res)=>{
    res.end('ex01 page');
});

app.listen(3000,()=>console.log('server start...'));