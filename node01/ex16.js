var express=require('express');
var app=express();
var bbs=require('./bbs/');

app.use(require('./modules/logging'));

app.get('/',(req,res)=>res.send('index page'));
app.use('/bbs/',bbs);

app.listen(3000,()=>console.log('server start...'));