var express=require('express');
var bodyParser=require('body-parser');
var app=express();
//application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:false}));

app.get('/', (req, res) => {
    res.send('api service');
});
app.use('/api/bbs/',require('./bbs/'));

app.listen(3000,()=>console.log('server start...'));