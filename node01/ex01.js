const http = require('http');

var callback=function(req,res){
  switch(req.url){
    case '/': return res.end('index');
    case '/ex01': return res.end('ex01 page');
    case '/ex02': return res.end('ex02 page');
    default:
            res.end('404');
  }
}

const server = http.createServer(callback);

server.listen(3000, () => {
  console.log('서버 실행중...');
});