var express=require('express');
var app=express();

app.get('/',(req,res)=>{
    //res.send('index');
    // res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
    // res.write('<h1>1번째</h1>');
    // res.write('<h2>2번째</h2>');
    // res.end();
    // res.json({'key1':'val1'});
    // res.redirect('/ex01');
    // res.sendStatus(404);
    // res.status(404).send('페이지 없음');
    // res.render('view 이름');
});
app.get('/ex01',(req,res,nxt)=>{
    console.log('첫번째 콜백');
    nxt();
},(req,res)=>{
    console.log('두번째 콜백');
    res.send('ex01 page');
});

app.get('/bbs/:num',(req,res)=>{
    var num=req.params.num;
    res.send('bbs num='+num);
});
app.listen(3000,()=>console.log('server run...'));