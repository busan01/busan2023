import Link from 'next/link'
export default function Home() {
  return (
    <>
      <main className={``}>
        <nav>
          <Link href={'/'}>Home</Link>
          <Link href={'/dept'}>Dept</Link>
          <Link href={'/sign'}>Login</Link>
          <Link href={'/sign/join'}>Join</Link>
        </nav>
        <h1>login page</h1>
      </main>
    </>
  )
}
