import { GetServerSideProps, NextPage } from "next";
import { useRouter } from "next/router";

const Community = (props ) => {
    const router = useRouter();

    const kakaoInit = () => {
        const kakao = (window ).Kakao;
        if (!kakao.isInitialized()) {
            kakao.init(process.env.NEXT_PUBLIC_KAKAO_SHARE_KEY);
        }

        return kakao;
    }

    const kakaoLogin = async () => {
        // 카카오 초기화
        const kakao = kakaoInit();

        // 카카오 로그인 구현
        kakao.Auth.login({
            success: () => {
                kakao.API.request({
                    url: '/v2/user/me', // 사용자 정보 가져오기
                    success: async (res ) => {
                        // 로그인 성공할 경우 
                        if (!res.kakao_account.email || res.kakao_account.email === "") {
                            alert("해당 계정의 이메일이 존재하지 않습니다.")
                        }
                        else {
                        	// 쿠키 생성
                            await axios.post("/api/community/login", { email: res.kakao_account.email })
                                .then(({ data }) => {
                                    if (data.success) {
                                        dispatch({ name: "email", value: res.kakao_account.email })
                                    }
                                    else {
                                        return alert(" 로그인에 실패하였습니다.")
                                    }
                                })
                        }
                        
                    },
                    fail: (error ) => {
                        console.log(error);
                    }
                })
            },
            fail: (error ) => {
                console.log(error);
            }
        })
    }

    return (
        <>
            <button onClick={(e ) => { kakaoLogin(); }}>
                카톡으로 로그인
            </button>
        </>
    )
}

export default Community;

export const getServerSideProps = async (context) => {
    return {
        props: {

        }
    }
}