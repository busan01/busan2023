'use strict';

var _require = require('@prisma/client'),
    PrismaClient = _require.PrismaClient;

var prisma = new PrismaClient();

var all = async function all() {
  var allUsers = await prisma.user.findMany();
  console.log(allUsers);
};
var add = async function add() {
  await prisma.user.create({
    data: {
      name: 'Alice',
      email: 'alice@prisma.io',
      posts: {
        create: { title: 'Hello World' }
      },
      profile: {
        create: { bio: 'I like turtles' }
      }
    }
  });

  var allUsers = await prisma.user.findMany({
    include: {
      posts: true,
      profile: true
    }
  });
  console.dir(allUsers, { depth: null });
};
async function main() {
  add();
}

main().then(async function () {
  await prisma.$disconnect();
}).catch(async function (e) {
  console.error(e);
  await prisma.$disconnect();
  process.exit(1);
});