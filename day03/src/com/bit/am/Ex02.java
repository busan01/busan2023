package com.bit.am;

import java.util.Arrays;

public class Ex02 {

	public static void main(String[] args) {
		// 배열복사
		// 얕은 복사
		int[] arr1= {1,3,5,7,9};
		int[] arr2=arr1;
		arr2[1]=2;
		System.out.println(Arrays.toString(arr1));
		System.out.println(Arrays.toString(arr2));
		// 깊은 복사
		int[] arr3=new int[arr1.length];
		for(int i=0; i<arr1.length; i++)
			arr3[i]=arr1[i];
		arr3[1]=3;
		System.out.println(Arrays.toString(arr1));
		System.out.println(Arrays.toString(arr3));
		int[] arr4=copyArray(arr1);
		System.out.println(Arrays.toString(arr1));
		System.out.println(Arrays.toString(arr4));
	}
	public static int[] copyArray(int[] arr) {
		arr[1]=2;
		return arr;
	}
	
	
	
	

}
