package com.bit.am;

import java.util.Arrays;

public class Ex03 {

	public static void main(String[] args) {
		// 깊은복사
		//1.
		//2.
		char[] origin= {'a','b','c','d','e'};
		char[] target1=new char[origin.length];
		System.arraycopy(origin, 0, target1, 0, target1.length);
		target1[2]='C';
		char[] target2=Arrays.copyOf(origin, origin.length);
		char[] target3=Arrays.copyOfRange(origin, 1, 4);
		target2[1]='B';
		System.out.println(Arrays.toString(origin));
		System.out.println(Arrays.toString(target1));
		System.out.println(Arrays.toString(target2));
		System.out.println(Arrays.toString(target3));
		
		Object obj=origin.clone();
		char[] target4=(char[]) obj;
		System.out.println(Arrays.toString(target4));
	}

}















