package com.bit.am;

public class Ex01 {
	// 442 8800
	public static void main(String[] args) {
		String st1="Hello";
		String st2="World";
		String st3=st1+st2;
		String st4=st1.concat(st2);
		String st5=st1+st2;
		String st6=st1.concat(st2);
		System.out.println(st3+st5=="HelloWorld");
		System.out.println(st4+st6=="HelloWorld");
		System.out.println("Hello"+"World"=="HelloWorld");
		System.out.println(("Hello"+"World").equals("HelloWorld"));
		System.out.println("Hello".concat("World")=="HelloWorld");
		System.out.println("Hello".concat("World").equals("HelloWorld"));
		System.out.println("------------------------------");
		char a=st1.charAt(1);
		System.out.println(a);
		System.out.println("------------------------------");
		System.out.println(st1.contains("ll"));
		System.out.println(st1.contains("le"));
		System.out.println(st1.indexOf("l"));
		System.out.println(st1.lastIndexOf("l"));
		System.out.println("------------------------------");
		System.out.println(st1.isEmpty());
		System.out.println("".isEmpty());
		System.out.println("".isEmpty());
		System.out.println("------------------------------");
		System.out.println(st1.substring(2));
		System.out.println(st1.substring(2,4));
		System.out.println("------------------------------");
		System.out.println("                 java    web   db      ".trim());
		System.out.println("------------------------------");
		String msg1="java";
		String msg2="javza";
		System.out.println(msg1.compareTo(msg2));
	}

}



























