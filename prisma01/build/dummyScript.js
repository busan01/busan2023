'use strict';

var _client = require('@prisma/client');

var prisma = new _client.PrismaClient();

async function main() {
  // const user = await prisma.user.create({
  //     data: {
  //       name: 'Alice',
  //       email: 'alice@prisma.io',
  //     },
  //   })
  //   console.log(user)

  // const users = await prisma.user.findMany()
  var users = await prisma.user.findMany({ include: { posts: true } });
  console.log(JSON.stringify(users, null, 2));

  // const user = await prisma.user.create({
  //   data: {
  //     name: 'Bob',
  //     email: 'bob@prisma.io',
  //     posts: {
  //       create: {
  //         title: 'Hello World',
  //       },
  //     },
  //   },
  // })
  // console.log(user)
}

main().then(async function () {
  await prisma.$disconnect();
}).catch(async function (e) {
  console.error(e);
  await prisma.$disconnect();
  process.exit(1);
});