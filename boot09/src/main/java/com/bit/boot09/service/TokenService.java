package com.bit.boot09.service;

import java.security.Key;
import java.util.Date;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {
	@Value("${secure.key}")
	String key;

	public String createToken() {
		SignatureAlgorithm alg=SignatureAlgorithm.HS256;
		String jws = Jwts.builder()
						.signWith(alg, key)
						.claim("user","scott")
						.setSubject("auth")
						.setExpiration(new Date(System.currentTimeMillis()+1000*60*5))
						.compact();
		return jws;
	}
	
	public boolean tokenTime(String token) {
		Jws<Claims> jws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
		long expire=jws.getBody().getExpiration().getTime();
		return expire>=System.currentTimeMillis();
	}
	
	public String tokenUser(String token) {
		try {
			Jws<Claims> jws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
			return (String) jws.getBody().get("user");
		}catch (Exception e) {
			return null;
		}
	}
}






