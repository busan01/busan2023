package com.bit.boot09.config;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.bit.boot09.service.TokenService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TokenFilter extends OncePerRequestFilter {
	final TokenService tokenService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		String auth=request.getHeader("Authorization");
		if(auth!=null) {
			if(auth.startsWith("Bearer ")) {
				auth=auth.replace("Bearer ", "");
			}
			String user=tokenService.tokenUser(auth);
			boolean timeCheck=tokenService.tokenTime(auth);
			if(user!=null && timeCheck) {
				SecurityContext context = SecurityContextHolder.getContext();
				context.setAuthentication(
						new UsernamePasswordAuthenticationToken(
								user, null, List.of(new SimpleGrantedAuthority("USER")))
						);
			}
		}
		chain.doFilter(request, response);
		
	}

}
