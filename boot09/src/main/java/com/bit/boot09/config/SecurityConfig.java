package com.bit.boot09.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.bit.boot09.service.TokenService;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
	@Autowired
	TokenService tokenService;
	
	@Bean
	TokenFilter getTokenFilter(TokenService tokenService) {
		return new TokenFilter(tokenService);
	}
	
	@Bean
	MySuccessHandler getMySuccessHandler() {
		return new MySuccessHandler();
	}

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.httpBasic().disable();
		http.authorizeHttpRequests().mvcMatchers("/").permitAll().anyRequest().authenticated()
			.and()
			.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.addFilterBefore(getTokenFilter(tokenService), UsernamePasswordAuthenticationFilter.class)
			.formLogin(login->login.successHandler(getMySuccessHandler()))
		;
		return http.build();
	}
}
