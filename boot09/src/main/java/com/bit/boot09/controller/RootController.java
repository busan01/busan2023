package com.bit.boot09.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bit.boot09.service.TokenService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class RootController {
	final TokenService tokenService;

	@GetMapping("/")
	public String getkey() {
		return tokenService.createToken();
	}
	
	@GetMapping("/info")
	public String getUser(String token) {
		return tokenService.tokenUser(token);
	}
}











