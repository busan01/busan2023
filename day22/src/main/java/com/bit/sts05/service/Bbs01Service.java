package com.bit.sts05.service;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bit.sts05.model.Bbs01Dao;
import com.bit.sts05.model.entry.Bbs01Vo;

@Service
public class Bbs01Service {
	@Autowired
	SqlSessionManager sqlSessionManager;
	
	public List<Bbs01Vo> list() throws SQLException{
		try(SqlSession sqlSession=sqlSessionManager.openSession();){
			Bbs01Dao dao=sqlSession.getMapper(Bbs01Dao.class);
			return dao.selectAll();
		}
	}

	public Bbs01Vo one(int num) throws SQLException {
		try(SqlSession sqlSession=sqlSessionManager.openSession();){
			Bbs01Dao dao=sqlSession.getMapper(Bbs01Dao.class);
			return dao.selectOne(num);
		}
	}
	
	public void addUpdate(Bbs01Vo bean) throws SQLException {
		try(SqlSession sqlSession=sqlSessionManager.openSession();){
			Bbs01Dao dao=sqlSession.getMapper(Bbs01Dao.class);
			int result=dao.insertOne(bean);
			System.out.println(result);
		}
	}
	public boolean deleteUpdate(int num) throws SQLException {
		try(SqlSession sqlSession=sqlSessionManager.openSession();){
			Bbs01Dao dao=sqlSession.getMapper(Bbs01Dao.class);
			return dao.deleteOne(num)>0;
		}
	}
	public boolean editUpdate(Bbs01Vo bean) throws SQLException {
		try(SqlSession sqlSession=sqlSessionManager.openSession();){
			Bbs01Dao dao=sqlSession.getMapper(Bbs01Dao.class);
			return dao.updateOne(bean)>0;
		}
	}
}














