package com.bit.sts05.model.entry;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Bbs01Vo {
	private int num;
	private String id,sub,content;
	private Timestamp nalja;
}
