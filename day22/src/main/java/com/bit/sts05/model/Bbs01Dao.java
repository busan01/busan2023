package com.bit.sts05.model;

import java.util.List;

import com.bit.sts05.model.entry.Bbs01Vo;

public interface Bbs01Dao {

	List<Bbs01Vo> selectAll();

	Bbs01Vo selectOne(int num);

	int insertOne(Bbs01Vo bean);

	int deleteOne(int num);

	int updateOne(Bbs01Vo bean);

}
