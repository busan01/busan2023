package com.bit.sts05.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bit.sts05.model.entry.Bbs01Vo;
import com.bit.sts05.service.Bbs01Service;

@Controller
@RequestMapping("/bbs")
public class BbsController {
	@Autowired
	Bbs01Service service;

	@GetMapping("/")
	public String list(HttpServletRequest req,Model model) throws SQLException {
		String param=req.getParameter("page");
		model.addAttribute("list", service.list());
		return "bbs/list";
	}
	
	@PostMapping("/")
	public String add(@ModelAttribute Bbs01Vo bean) throws SQLException {
		service.addUpdate(bean);
		return "redirect:./";
	}
	
	@GetMapping(value = "/{1}",produces = {"application/json; charset=utf-8"})
	public @ResponseBody Bbs01Vo detail(@PathVariable("1") int num ) throws SQLException {
		Bbs01Vo bean=service.one(num);
		
		return bean;
	}
	@PutMapping(value = "/{1}")
	public @ResponseBody String edit(@PathVariable("1") int num,@RequestBody Bbs01Vo bean) throws SQLException {
		service.editUpdate(bean);
		return "success";
	}
	@DeleteMapping("/{1}")
	public @ResponseBody String delete(@PathVariable("1") int num) throws SQLException {
		service.deleteUpdate(num);
		return "success";
	}
}









