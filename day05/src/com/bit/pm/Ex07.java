package com.bit.pm;

import java.io.*;

public class Ex07 {

	public static void main(String[] args) {
		File f=new File("test01.txt");
		
		java.io.InputStream is=null;
		try {
			is=new FileInputStream(f);
			while(true) {
				int su=is.read();
				if(su==-1)break;
				System.out.println(su);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(is!=null)is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
