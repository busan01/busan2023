package com.bit.pm;

import java.io.*;

public class Ex08 {

	public static void main(String[] args) {
		File source=new File("D:\\workspace\\logo_footer.png");
		File target=new File("copy.png");
		InputStream is=null;
		OutputStream os=null;
		try {
			target.createNewFile();
			is=new FileInputStream(source);
			os=new FileOutputStream(target);
			while(true) {
				int su=is.read();
				if(su==-1)break;
				os.write(su);
			}
			System.out.println("복사완료");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if(is!=null)is.close();
				if(os!=null)os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
