package com.bit.pm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Ex12 {

	public static void main(String[] args) {
		File file=new File("test03.txt");
		byte[] buf=new byte[10];
		List<Byte> list=new ArrayList<>();
		try(
				InputStream is=new FileInputStream(file);
				){
			while(true) {
				int cnt=is.read(buf);
				if(cnt==-1)break;
				for(int i=0; i<cnt; i++)
					list.add(buf[i]);
			}
			Object[] arr2=list.toArray();
			byte[] arr=new byte[arr2.length];
			for(int i=0; i<arr.length; i++)
				arr[i]=(byte) arr2[i];
			System.out.println(new String(arr));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

}
