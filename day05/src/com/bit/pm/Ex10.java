package com.bit.pm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Ex10 {

	public static void main(String[] args) {
		File file =new File("test02.txt");
		
		java.io.Reader fr=null;
		
		try {
			fr=new java.io.FileReader(file);
			int su=fr.read();
			System.out.println((char)su);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(fr!=null)fr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}

	}

}
