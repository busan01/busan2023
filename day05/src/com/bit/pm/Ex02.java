package com.bit.pm;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Ex02 {

	public static void main(String[] args) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd aaa hh:mm");
		String path="./test02";
		File f1=new File(path);
		String[] names=f1.list();
		String ord=path;
		for(int i=0; i<names.length; i++) {
			path=names[i];
			f1=new File(ord+"/"+path);
			String nalja=sdf.format(new Date(f1.lastModified()));
			String dir=f1.isDirectory()?"<DIR>":"";
			String name=f1.getName();
			System.out.println(nalja+"\t"+dir+"\t"+name);
		}
	}

}
