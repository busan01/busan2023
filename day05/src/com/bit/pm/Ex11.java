package com.bit.pm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Ex11 {

	public static void main(String[] args) throws IOException {
		File file=new File("test03.txt");
		file.createNewFile();
		byte[] buf="aaaaaaaaaabbb".getBytes();
		try (
				OutputStream os=new FileOutputStream(file);
		){
			os.write(buf);
		}
	}

}
