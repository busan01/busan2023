package com.bit.pm;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Ex01 {

	public static void main(String[] args) throws IOException {
//		java.io.File file=new File("D:\\workspace\\day05\\test01.txt");
		java.io.File file=new File(".\\test02");
		System.out.println("존재하냐?"+file.exists());
		if(file.exists()) {
			System.out.println("파일?"+file.isFile());
			System.out.println("디렉토리?"+file.isDirectory());
			System.out.println("이름?"+file.getName());
			System.out.println("이름?"+file.getParent());
			System.out.println("경로1?"+file.getPath());
			System.out.println("경로2?"+file.getAbsolutePath());
			System.out.println("경로3?"+file.getCanonicalPath());
			if(file.isFile())
				System.out.println(file.length()+"byte");
			Date date=new java.util.Date(file.lastModified());
			java.text.SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
			System.out.println(sdf.format(date));
			String[] names=file.list();
			System.out.println(Arrays.toString(names));
		}
	}

}
