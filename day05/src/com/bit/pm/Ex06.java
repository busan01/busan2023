package com.bit.pm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Ex06 {

	public static void main(String[] args) {
		String msg="한 글";
		byte[] arr=msg.getBytes();
		File f=new File("test01.txt");
		try {
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		java.io.OutputStream os=null;
		try {
			os=new java.io.FileOutputStream(f);
			for(int i=0; i<arr.length; i++)
				os.write(arr[i]);
			System.out.println("작성완료");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(os!=null)os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}







