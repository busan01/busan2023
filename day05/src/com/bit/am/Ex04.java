package com.bit.am;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class Ex04 {

	public static void main(String[] args) {
		java.util.Map map=new java.util.HashMap();
		map.put("key1", "val1");
		map.put(true, "val2");
		map.put(1234, "val1");
		map.put(null, 1234);
		map.put("key5", true);

//		System.out.println(map.get("key1"));
//		System.out.println(map.get(true));
//		System.out.println(map.get(1234));
//		System.out.println(map.get("key4"));
//		System.out.println(map.get("key5"));
		
//		java.util.Set keys=map.keySet();
//		Iterator ite = keys.iterator();
//		while(ite.hasNext())
//			System.out.println(map.get(ite.next()));
		Set entrys = map.entrySet();
		Iterator ite=entrys.iterator();
		while(ite.hasNext()) {
			java.util.Map.Entry entry=(Entry) ite.next();
			System.out.println(entry.getKey()+":"+entry.getValue());
		}
	}

}







