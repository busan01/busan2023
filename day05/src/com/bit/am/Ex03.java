package com.bit.am;

import java.util.*;

public class Ex03 implements Comparable {
	int num;
	
	public Ex03(int num) {
		this.num=num;
	}

	public static void main(String[] args) {
		Set set=new TreeSet();
		set.add(new Ex03(1));
		set.add(new Ex03(2));
		set.add(new Ex03(4));
		set.add(new Ex03(3));
		set.add(new Ex03(4));

		Iterator ite = set.iterator();
		while(ite.hasNext())
			System.out.println(ite.next());
	}
	
	@Override
	public String toString() {
		return num+"번";
	}

	@Override
	public int compareTo(Object obj) {
//		return this.num-((Ex03)obj).num;
		return ((Ex03)obj).num-this.num;
	}

}
