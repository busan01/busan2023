package com.bit.am;

class Lec08<A,B>{	// T, E, K, V
	A su;
	B msg;
	
	public A getSu() {
		return su;
	}
	public void setSu(A su) {
		this.su = su;
	}
	public B getMsg() {
		return msg;
	}
	public void setMsg(B msg) {
		this.msg = msg;
	}
	
}

public class Ex08 {

	public static void main(String[] args) {
		Lec08<Integer,String> lec=new Lec08<Integer,String>();
		lec.setSu(1234);
		lec.setMsg("abcd");
		int su=lec.getSu();
		String msg=lec.getMsg();
	}

}
