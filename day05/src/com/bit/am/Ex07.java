package com.bit.am;

interface Pen{
	void write();
}

class Pencil implements Pen{
	public void write() {
		System.out.println("잘지워지는 검정색 선을 드로잉한다");
	}
}
class Ballpen implements Pen{
	public void write() {
		System.out.println("잘지워지지 않는 검정색 선을 드로잉한다");
	}
}

class Box<T extends Pen>{
	private T pen;
	
	public void setPen(T pen) {
		this.pen = pen;
	}
	public T getPen() {
		return pen;
	}
}

class Ball{}

public class Ex07 {

	public static void main(String[] args) {
		Ballpen pen=new Ballpen();
		Pencil pen2=new Pencil();
		Ball ball=new Ball();
		
		Box<Pen> box=new Box<Pen>();
		
		box.setPen(pen2);
		Pen myPen=box.getPen();
	}

}











