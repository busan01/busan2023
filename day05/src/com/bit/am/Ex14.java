package com.bit.am;

public class Ex14 extends Thread{
	public Ex14() {
		super("새이름");
	}

	public static void main(String[] args) {
		Ex14 me=new Ex14();
		me.start();
	}

	@Override
	public void run() {
		String name=this.getName();
		System.out.println(name);
	}
}
