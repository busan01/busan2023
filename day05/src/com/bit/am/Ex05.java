package com.bit.am;

import java.util.*;
import java.util.Map.Entry;

public class Ex05 {

	public static void main(String[] args) {
		Map map=new HashMap();
		map.put(1111, "첫번째");
		map.put(3333, "세번째");
		map.put(2222, "두번째");
		map.put(4444, "네번째");
		
//		map.replace(3333, "3번째");
		map.put(3333, "3번째");
		
		map.remove(2222);
		map.remove(1111,"1번째");

		Iterator ite = map.entrySet().iterator();
		while(ite.hasNext()) {
			Entry entry=(Entry) ite.next();
			System.out.println(entry.getKey()+":"+entry.getValue());
		}
	}

}
