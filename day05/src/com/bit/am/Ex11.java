package com.bit.am;

public class Ex11 implements Runnable{

	public static void main(String[] args) {
		Ex11 ex=new Ex11();
		Thread thr=new Thread(ex);
		
		thr.start();
		
		Thread me=Thread.currentThread();
		for(int i=0; i<50; i++)
		System.out.println("main에서 "+me.getName());
		System.out.println("main end");
	}

	@Override
	public void run() {
		Thread me=Thread.currentThread();
		for(int i=0; i<50; i++)
		System.out.println("run에서 "+me.getName());
		System.out.println("new Thread end");
	}

}
