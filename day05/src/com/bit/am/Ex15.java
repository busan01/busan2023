package com.bit.am;

public class Ex15 extends Thread {

	public static void main(String[] args) {
		Ex15 thr1=new Ex15();
		Ex15 thr2=new Ex15();
		Ex15 thr3=new Ex15();
//		thr2.setPriority(MAX_PRIORITY);
//		thr3.setPriority(MIN_PRIORITY);
		thr1.setPriority(5);
		thr2.setPriority(6);
		thr3.setPriority(4);
		thr1.start();
		thr2.start();
		thr3.start();
	}

	@Override
	public void run() {
		String name=this.getName();
		System.out.println(name+":"+this.getPriority());
	}
}








