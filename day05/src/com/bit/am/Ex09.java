package com.bit.am;

class Box09<T>{
	T t;
	public T getT() {
		return t;
	}
	public void setT(T t) {
		this.t = t;
	}
}

class Lec09{
	private Lec09() {
	}
	
	public static <T> Box09 getInstance(T su) {
		Box09<T> box=new Box09<T>();
		box.setT(su);
		return box;
	} 
}

public class Ex09 {
	
	public static Box09<?> get(boolean boo){
		if(boo)
			return new Box09<String>();
		else
			return new Box09<Integer>();
	}

	public static void main(String[] args) {
//		Box09<Integer> box=Lec09.<Integer>getInstance(1234);
//		int su=box.getT();
//		System.out.println(su);
		
		Box09<?> box=new Box09();
		Box09<String> box2=(Box09<String>)box;
		box2.setT("asdfads");
		
		
		
		
//		Box09 box=new Box09<Integer>();
//		box.setT("aaaa");
//		System.out.println(box.getT());
		
//		Box09<Integer> box=new Box09<>();
		
		
	}

}
