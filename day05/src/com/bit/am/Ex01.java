package com.bit.am;

import java.util.*;

public class Ex01 {

	public static void main(String[] args) {
		Vector vac=new Vector();
		vac.addElement("첫번째");
		vac.addElement("두번째");
		vac.addElement("세번째");
		vac.addElement("네번째");
		System.out.println("----------------------------");
		Enumeration eun = vac.elements();
		while(eun.hasMoreElements()) {
			Object obj=eun.nextElement();
			obj=new Object();
		}
		System.out.println("----------------------------");
		
		System.out.println(vac.elementAt(0));
		System.out.println(vac.elementAt(1));
		System.out.println(vac.elementAt(2));
		System.out.println(vac.elementAt(3));
		
	}

}
