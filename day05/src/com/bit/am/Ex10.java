package com.bit.am;

public class Ex10 extends Thread {

	public static void main(String[] args) {
		Ex10 me=new Ex10();
		me.start();
		Thread thr=Thread.currentThread();
		for(int i=0; i<10; i++) {
			System.out.println("main에서 "+thr.getName());
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void run() {
		Thread thr=Thread.currentThread();
		for(int i=0; i<10; i++) {
		System.out.println("run에서 "+thr.getName());
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	}
}
