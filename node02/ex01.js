var crypto = require('crypto');
var msg='abcd';

var hash=crypto.createHash('sha256');
var result=hash.update(msg,'utf8');
console.log(result.digest('base64').toString());

var hmc=crypto.createHmac('sha256','myKey');
var result2=hmc.update(msg);
console.log(result2.digest('base64'));

var mykey = crypto.createCipher('aes-128-cbc', 'myKey');
var mystr = mykey.update(msg, 'utf8', 'hex')
mystr += mykey.final('hex');

console.log(mystr);

var mykey = crypto.createDecipher('aes-128-cbc', 'myKey');
var mystr = mykey.update(mystr, 'hex', 'utf8')
mystr += mykey.final('utf8');

console.log(mystr);