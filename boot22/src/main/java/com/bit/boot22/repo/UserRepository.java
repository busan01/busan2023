package com.bit.boot22.repo;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.bit.boot22.domain.User;

public interface UserRepository extends ReactiveCrudRepository<User, Long> {

}
