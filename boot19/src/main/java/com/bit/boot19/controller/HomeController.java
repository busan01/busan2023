package com.bit.boot19.controller;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.ServerRequest;

import com.bit.boot19.domain.Dept;
import com.github.jasync.sql.db.QueryResult;
import com.github.jasync.sql.db.mysql.MySQLConnection;
import com.github.jasync.sql.db.mysql.MySQLConnectionBuilder;
import com.github.jasync.sql.db.pool.ConnectionPool;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Slf4j
@RestController
@RequiredArgsConstructor
public class HomeController {
	final ConnectionPool connectionPool;

	@GetMapping
	public String hello() {
		return "hello";
	}

	@GetMapping("/rang")
	public Flux<?> async2(){
		return Flux.range(10, 5);
	}
	
	@GetMapping("/async")
	public CompletableFuture<?> async(){
		log.info("async call");
		int su=1234;
//		return CompletableFuture.completedFuture("complete");
		return CompletableFuture.supplyAsync(()->{log.info("async callback {}",su); return "result";});
	}
	
	@GetMapping("/dept")
	public CompletableFuture<?> list() throws NoSuchAlgorithmException, UnsupportedEncodingException{
		// Connection to MySQL DB
		
		     
		CompletableFuture<QueryResult> future = connectionPool.sendPreparedStatement("select * from dept");
		return future;
		// work with result ...
		// Close the connection pool
//		connection.disconnect().get();
	}
	
	@PostMapping("/dept")
	public CompletableFuture<?> add(@RequestBody Dept dept) {
		log.debug(dept.toString());
		return connectionPool.sendPreparedStatement("insert into dept (dname,loc) values (?,?)",Arrays.asList(dept.getDname(),dept.getLoc()));
	}
}
