package com.bit.boot19;

import java.net.URI;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class TestApplication {
	volatile static int su=0;

	public static void main(String[] args) {
//		SpringApplication.run(TestApplication.class, args);
		RestTemplate rt = new RestTemplate();
		String url="http://localhost:8080/dept";
		ExecutorService pool = Executors.newFixedThreadPool(10);
		CyclicBarrier cb=new CyclicBarrier(5);
		for(int i=0; i<10; i++)
			pool.submit(()->{cb.await(); log.info("call {}",rt.getForEntity(url,String.class).getBody());return null;});
		pool.shutdown();
		
//		return (Void)null;
	}

}
