package com.bit.boot19;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatReactiveWebServerFactory;
import org.springframework.boot.web.reactive.server.ReactiveWebServerFactory;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.context.annotation.Bean;

import com.github.jasync.sql.db.mysql.MySQLConnection;
import com.github.jasync.sql.db.mysql.MySQLConnectionBuilder;
import com.github.jasync.sql.db.pool.ConnectionPool;

@SpringBootApplication
public class Boot19Application {

	public static void main(String[] args) {
		SpringApplication.run(Boot19Application.class, args);
	}

//	@Bean
//	ReactiveWebServerFactory serverFactory() {
//		return new TomcatReactiveWebServerFactory(8080);
//	}
	
	@Bean
	ConnectionPool<MySQLConnection> connectionPool() {
		ConnectionPool<MySQLConnection> connection = MySQLConnectionBuilder.createConnectionPool(
	               "jdbc:mysql://localhost:3306/xe?user=scott&password=tiger&allowPublicKeyRetrieval=true");
		return connection;
	}
}
