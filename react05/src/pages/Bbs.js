import React, { useContext, useEffect, useRef, useState} from 'react'
import Card from 'react-bootstrap/Card';
import {UserContext} from '../modules/bbsReducer';
import axios from 'axios';
import { Button, Form, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function Bbs() {
    const {list,dispatch}=useContext(UserContext);
    const [show, setShow] = useState(false);
    const [num,setNum]=useState();
    const [sub,setSub]=useState();
    const [id,setId]=useState();
    const [content,setContent]=useState();
    const [editModal,setEditModal]=useState(false);
    useEffect(()=>{
      getList();
      dispatch({type:'complate'});
    },[]);
    const inputChang=(ele,callback)=>{
        if(editModal)callback(ele.value);
    }
    const modalSubmit = async e=>{
        e.preventDefault();
        if(editModal){
          console.log({num,subject:sub,id,content});
          await axios({
            url: '/bbs/v1/'+num,
            method: 'PUT',
            headers: {'Context-Type':'application/json'},
            data:{num,subject:sub,id,content}
          });
          setShow(false);
          getList();
        }
        setEditModal(!editModal);
    };
    const handleClose = () => setShow(false);
    const handleShow = (num) => {
      console.log(num);
      setShow(true);
      listOne(num);
    };
    function listOne(num){
        axios('/bbs/v1/'+num).then((json)=>{
          console.log(json);
          setNum(json.data.num);
          setSub(json.data.subject);
          setId(json.data.id);
          setContent(json.data.content);
        });
    }
    function getList(){
        axios('/bbs/v1/').then((json)=>{
          console.log(json);
          dispatch({type:'bbsList',payload:json.data});
          setEditModal(false);
        });
    }
  return (
    <>
    {list.data.map(ele=>
    <Card style={{ width: '18rem' }} key={ele.num}>
      <Card.Body>
        <Card.Title>{ele.subject}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted"></Card.Subtitle>
        <Card.Text>{ele.content}</Card.Text>
        <Card.Link href="#" onClick={e=>{
          e.preventDefault();
          handleShow(ele.num);
          }}>more...</Card.Link>
      </Card.Body>
    </Card>
    )}
    <Link to={'form'}>
    <Button>입력</Button>
    </Link>
    {/* modal */}
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title></Modal.Title>
      </Modal.Header>
      <Modal.Body>
      <Form onSubmit={modalSubmit}>
        <Form.Group className="mb-3" controlId="subject">
          <Form.Label></Form.Label>
          <Form.Control onChange={e=>inputChang(e.target,setSub)} value={sub} placeholder="subject" />
        </Form.Group>
        <Form.Group className="mb-3" controlId="id">
          <Form.Label></Form.Label>
          <Form.Control onChange={e=>inputChang(e.target,setId)} value={id} placeholder="id" />
        </Form.Group>
        <Form.Group className="mb-3" controlId="content">
          <Form.Label></Form.Label>
          <Form.Control onChange={e=>inputChang(e.target,setContent)} value={content} as="textarea" rows={3} />
        </Form.Group>
        <Form.Group className="mb-3" >
        <Button variant="primary" type="submit">수정</Button>
        <Button type='button' variant="secondary" onClick={handleClose}>
          닫기
        </Button>
        </Form.Group>
      </Form>
      </Modal.Body>
      <Modal.Footer></Modal.Footer>
    </Modal>
    </>
  )
}

export default Bbs