import axios from 'axios';
import React, { useRef } from 'react'
import { Button, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

function AddForm() {
    const navigate = useNavigate();
    const sub=useRef();
    const id=useRef();
    const content=useRef();
    const submit =e=>{
        e.preventDefault();
        console.log(sub.current.value,id.current.value,content.current.value);
        let param=`subject=${sub.current.value}&id=${id.current.value}&content=${content.current.value}`;
        console.log(param,);
        axios.post('/bbs/v1/',param);
        navigate('/bbs/');
    };
  return (
    <>
    <Form onSubmit={submit}>
      <Form.Group className="mb-3" controlId="subject">
        <Form.Label></Form.Label>
        <Form.Control ref={sub} placeholder="subject" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="id">
        <Form.Label></Form.Label>
        <Form.Control ref={id} placeholder="id" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="content">
        <Form.Label></Form.Label>
        <Form.Control ref={content} as="textarea" rows={3} />
      </Form.Group>
      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
    </>
  )
}

export default AddForm