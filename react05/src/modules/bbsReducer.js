import axios from "axios";
import { createContext } from "react";

export const UserContext=createContext();
export const init={data:[
    {num:1,subject:'test1',content:'test11'},
    {num:2,subject:'test2',content:'test22'},
    {num:3,subject:'test3',content:'test33'},
],result:'success'};

export const bbsReducer=(state,action)=>{
    if(action.type==='bbsList') {
        return {...state,data:action.payload};
    }
    if(action.type==='complate'){
        return {...state};
    }
    return state;
};