import { BrowserRouter, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Layout from './Layout';
import Main from './pages/Main';
import Bbs from './pages/Bbs';
import {UserContext,bbsReducer,init} from './modules/bbsReducer';
import { useReducer } from 'react';
import AddForm from './pages/AddForm';

function App() {
  const [list,dispatch]=useReducer(bbsReducer,init);
  return (
    <UserContext.Provider value={{list,dispatch}}>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Layout/>}>
          <Route index element={<Main/>}/>
          <Route path='/bbs/' element={<Bbs/>}/>
          <Route path='/bbs/form' element={<AddForm/>}/>
        </Route>
      </Routes>
    </BrowserRouter>
    </UserContext.Provider>
  );
}

export default App;
