import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { Link,Outlet } from 'react-router-dom'

function Layout() {
  return (
    <>
        <nav>
            <Link to={'/'}>Home</Link>
            <Link to={'/bbs/'}>BBS</Link>
            <Link to={'/login/'}>Login</Link>
        </nav>
        <Container>
          <Row className="justify-content-md-center">
            <Col>
              <Outlet/>
            </Col>
          </Row>
        </Container>
    </>
  )
}

export default Layout