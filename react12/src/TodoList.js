import { useContext, useEffect } from 'react';
import { TodosContext } from './app/ApiContext';
import { userAPI } from './features/counter/userAPI';

export const TodoList = () => {
  const { state,dispatch } = useContext(TodosContext);
  useEffect(()=>{
    userAPI.fetchAll()
          .then(e=>e.json())
          .then(data=>dispatch({ type: "TODO/LIST", payload:data}))
  },[]);
  // async function loading(){
  //   const data=await (async function(){
  //     const resp=await userAPI.fetchAll();
  //     const data=await resp.json();
  //     return [...data];
  //   })();
  //   await dispatch({ type: "TODO/LIST", payload:data});
  // };

  return (
  <ul>
    {state.todos.map(ele=><li key={ele.id}>{ele.title}</li>)}
  </ul>
  );
};