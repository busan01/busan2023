import { useContext } from 'react';
import { CountContext } from './app/CountContext';

export const CountLabel = () => {
//   const { count } = useContext(CountContext);
//   return <div>{count}</div>;
  const { state } = useContext(CountContext);
  return <div>{state.count}</div>;
};