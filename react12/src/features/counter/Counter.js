import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from './counterSlice'
import { fetchUserById} from './usersSlice'

export default function Counter() {
  const count = useSelector((state) =>{return state.counter.value})
  const users=useSelector((state) =>{return state.users.entities});
  const dispatch = useDispatch();
  return (
  <div>
      <div>
        <button
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>
        <span>{count}</span>
        <button
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>
      </div>
      <div>
        <button onClick={()=>dispatch(fetchUserById({count}))}>obj</button>
        {users.map(ele=><p key={ele.id}>{ele.title}</p>)}
      </div>
    </div>
  )
}