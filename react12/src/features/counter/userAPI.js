export const userAPI={
    fetchById(userId){
        return fetch('https://jsonplaceholder.typicode.com/todos/'+userId);
    },
    fetchAll(){
        return fetch('https://jsonplaceholder.typicode.com/todos?userId=1');
    }
}
