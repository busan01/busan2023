import { createContext, useReducer, useState } from 'react';
import countReducer from '../services/countReducer';

const CountContext = createContext({
  count: 0,
  plusCount: () => {},
});

const CountProvider = ({ children }) => {
//   const [count, setCount] = useState(0);
  const initialState = { count: 100 };
  const [state, dispatch] = useReducer(countReducer, initialState);
//   const plusCount = () => {
//     setCount(count + 1);
//   };

  return (
    <CountContext.Provider
      value={{
        state,// count,
        dispatch,// plusCount,
      }}>
      {children}
    </CountContext.Provider>
  );
};

export { CountContext, CountProvider };