import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../features/counter/counterSlice'
import usersReducer from '../features/counter/usersSlice'

import { setupListeners } from '@reduxjs/toolkit/query'
import { pokemonApi } from '../services/pokemon'
import { todosApi } from '../services/todosApi'

export const store = configureStore({
  reducer: {
    counter:counterReducer,
    users:usersReducer,
    [pokemonApi.reducerPath]: pokemonApi.reducer,
    [todosApi.reducerPath]: todosApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
    .concat(pokemonApi.middleware).concat(todosApi.middleware),
})
setupListeners(store.dispatch)