import { createContext, useReducer} from 'react';
import todosReducer from '../services/todosReducer';

const TodosContext = createContext({
  todos: ['test1'],//count: 0,
  ing:'seccess'
});

const TodosProvider = ({ children }) => {
  const initialState = { todos: ['test2'] };
  const [state, dispatch] = useReducer(todosReducer, initialState);

  return (
    <TodosContext.Provider
      value={{
        state,// count,
        dispatch,// plusCount,
      }}>
      {children}
    </TodosContext.Provider>
  );
};

export { TodosContext, TodosProvider };