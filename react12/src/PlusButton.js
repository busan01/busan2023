import { useContext } from 'react';
import { CountContext } from './app/CountContext';

export const PlusButton = () => {
//   const { plusCount } = useContext(CountContext);
//   return <button onClick={plusCount}>+ 1</button>;
  const { dispatch } = useContext(CountContext);
  return (
    <div>
  <button onClick={()=>dispatch({ type: "INCREMENT", payload: 10 })}>+ 1</button>
  <button onClick={()=>dispatch({ type: "DECREMENT", payload: 1 })}>- 1</button>
    </div>
  )
};