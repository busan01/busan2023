import * as React from 'react'
import { useGetPokemonByNameQuery } from './services/pokemon'
import { useGetTodosQuery } from './services/todosApi'
import { CountProvider } from './app/CountContext'
import { CountLabel } from './CountLabel'
import { PlusButton } from './PlusButton'
import Counter from './features/counter/Counter'
import { TodosProvider } from './app/ApiContext'
import { TodoList } from './TodoList'

export default function App() {
  const { data, error, isLoading } = useGetPokemonByNameQuery('bulbasaur')
  // const { data, error, isLoading } = useGetTodosQuery('2');
  const todosQuery = useGetTodosQuery('2');
  const data2=todosQuery['data'];
  const error2=todosQuery['error'];
  const isLoading2=todosQuery['isLoading'];
  return (
    <TodosProvider>
    <CountProvider>
      
      <CountLabel></CountLabel>
      <PlusButton></PlusButton>
      <Counter></Counter>
      <div>
      {error ? (
        <>Oh no, there was an error</>
      ) : isLoading ? (
        <>Loading...</>
      ) : data ? (
        <>
          <h3>{data.species.name}</h3>
          <img src={data.sprites.front_shiny} alt={data.species.name} />
        </>
      ) : null}
      </div>
      <div>
        <h4>todos</h4>
        <TodoList/>
        <hr/>
        <div>
        {error2 ? (
            <>Oh no, there was an error</>
          ) : isLoading2 ? (
            <>Loading...</>
          ) : data2 ? (
            <ol>
              {data2.map(ele=><li key={ele.id}>{ele.title}</li>)}
            </ol>
          ) : null}
        </div>
      </div>
    </CountProvider>
    </TodosProvider>
  )
}