<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="layout/head.jspf" %>
<style type="text/css">
#content{text-align: center;}
table,table tr>th,table tr>td{
	border-collapse: collapse;
	border: 1px solid gray;
}
table{width: 600px;margin: 0px auto;}
table tr>th:nth-child(1),table tr>th:nth-child(3),table tr>th:nth-child(4){
	width: 100px;
}
table+p>a{
	text-align: center;
	display: block;
	width: 600px;
	height: 35px;
	line-height: 35px;
	text-decoration: none;
	color:white;
	background-color: gray;
	border-radius: 20px;
	margin: 0px auto;
}
</style>
</head>
<body>
<%@ include file="layout/menu.jspf" %>
<h2>게시판</h2>
<table>
	<thead>
		<tr>
			<th>글번호</th>
			<th>제목</th>
			<th>글쓴이</th>
			<th>날짜</th>
		</tr>
	</thead>
	<tbody>
	<c:forEach items="${list }" var="bean">
		<tr>
			<td>${bean.num }</td>
			<td>${bean.sub }</td>
			<td>${bean.id }</td>
			<td>${bean.nalja.year }/${bean.nalja.monthValue }/${bean.nalja.dayOfMonth }</td>
		</tr>
	</c:forEach>
	</tbody>
</table>
<p><a href="add.do">입력</a></p>
<%@ include file="layout/footer.jspf" %>
</body>
</html>












