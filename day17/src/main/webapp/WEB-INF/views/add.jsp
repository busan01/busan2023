<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="layout/head.jspf" %>
<style type="text/css">
	.container{
		text-align: center;
	}
	.container form{width: 400px; margin: 0px auto;}
	.container form>div{text-align: center;}
	.container form>div>label{width: 100px; display: inline-block; background-color: gray;}
	.container form>div>input{width: 260px;}
	.container form>div>textarea{width: 400px;height: 300px;}
	.container form>div>button{}
	.err{
		background-color: red;
		color:white;
		text-align: center;
		height: 35px;
	}
</style>
<script type="text/javascript">
	$(()=>{
		$('.err').hide();
		if($('.err').html()!='') {
			$('.err').fadeIn(200);
			$('.err').delay(3000);
			$('.err').fadeOut(200);
		}
		$('form').submit(function(e){
			if($('form input').val()=='') return false;
			if($('form input').val().length>10) return false;
		});
	});
</script>
</head>
<body>
<div class="err">${errMsg }</div>
<%@ include file="layout/menu.jspf" %>
<h2>입력페이지</h2>
<div>
	<form action="insert.do" method="post">
		<div>
			<label>제목</label><input name="sub" value="${bean.sub }" />
		</div>
		<div>
			<label>글쓴이</label><input name="id" value="${bean.id }" />
		</div>
		<div>
			<textarea name="content">${bean.content }</textarea>
		</div>
		<div>
			<button>입력</button>
			<button type="reset">취소</button>
			<button type="button" onclick="history.back();">뒤로</button>
		</div>
	</form>
</div>
<%@ include file="layout/footer.jspf" %>
	
</body>
</html>