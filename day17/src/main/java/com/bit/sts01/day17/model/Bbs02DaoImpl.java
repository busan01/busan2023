package com.bit.sts01.day17.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

public class Bbs02DaoImpl implements Bbs01Dao{
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public List<Bbs01Dto> selectAll() throws SQLException{
		List<Bbs01Dto> list=new ArrayList<>();
		String sql="select * from bbs01";
		try(
			Connection conn=dataSource.getConnection();
			PreparedStatement pstmt=conn.prepareStatement(sql);
			ResultSet rs=pstmt.executeQuery();
				){
			while(rs.next())list.add(new Bbs01Dto(
					rs.getInt("num"),rs.getString("id"),rs.getString("sub")
					,rs.getString("content"),rs.getTimestamp("nalja").toLocalDateTime()
					));
		}
		return list;
	}

	public void insertOne(String id, String sub, String content) throws SQLException {
		String sql="insert into bbs01 (id,sub,content,nalja) values (?,?,?,now())";
		try(
			Connection conn=dataSource.getConnection();
			PreparedStatement pstmt=conn.prepareStatement(sql);	
				){
			pstmt.setString(1, id);
			pstmt.setString(2, sub);
			pstmt.setString(3, content);
			pstmt.executeUpdate();
		}
	}

	public void insertOne(Bbs01Dto bean) throws SQLException {
		insertOne(bean.getId(), bean.getSub(), bean.getContent());
	}
}
