package com.bit.sts01.day17.step02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bit.sts01.day17.module.Module03;

public class App01 {

	public static void main(String[] args) {
		ApplicationContext ac;
		ac=new ClassPathXmlApplicationContext("applicationContext.xml");
		Module03 module=ac.getBean(Module03.class);
		System.out.println(module);
	}

}
