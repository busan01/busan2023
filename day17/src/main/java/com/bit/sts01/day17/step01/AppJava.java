package com.bit.sts01.day17.step01;

import com.bit.sts01.day17.module.Module;
import com.bit.sts01.day17.module.Module01;
import com.bit.sts01.day17.module.Module02;
import com.bit.sts01.day17.service.OneService;

public class AppJava {

	public static void main(String[] args) {
//		OneService service=new OneService();
		OneService service=new OneService(new Module02());
//		service.action01(new Module02());
//		service.action02(new Module02());
		
//		Module module=new Module02();
//		service.action01(module);
//		service.action02(module);
		Module01 module01=new Module01();
		Module02 module02=new Module02();
		service.setModule(module02);
		service.action01();
		service.action02();
	}

}
