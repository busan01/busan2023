package com.bit.sts01.day17.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.bit.sts01.day17.model.Bbs01Dao;
import com.bit.sts01.day17.model.Bbs01DaoImpl;

public class ListController implements Controller {
	Bbs01Dao dao;
	
	public void setDao(Bbs01Dao dao) {
		this.dao = dao;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav=new ModelAndView();
		mav.addObject("list", dao.selectAll());
		mav.setViewName("list");
		return mav;
	}

}
