package com.bit.sts01.day17.step02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bit.sts01.day17.module.Module06;

public class App04 {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext ac=new ClassPathXmlApplicationContext();
		ac.setConfigLocation("/applicationContext.xml");
		ac.refresh();
		Module06 module=ac.getBean(Module06.class);
		System.out.println(module.getSet());

	}

}
