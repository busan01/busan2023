package com.bit.sts01.day17.step02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bit.sts01.day17.module.Module08;

public class App06 {

	public static void main(String[] args) {
		ApplicationContext ac;
		ac=new ClassPathXmlApplicationContext("/applicationContext.xml");
		Module08 module=ac.getBean(Module08.class);
		System.out.println(module.getProps());
	}

}
