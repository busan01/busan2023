package com.bit.sts01.day17.step02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bit.sts01.day17.module.Module07;

public class App05 {

	public static void main(String[] args) {
		ApplicationContext ac;
		ac=new ClassPathXmlApplicationContext();
		((ClassPathXmlApplicationContext)ac).setConfigLocation("/applicationContext.xml");
		((ClassPathXmlApplicationContext)ac).refresh();
		Module07 module=ac.getBean(Module07.class);
		System.out.println(module.getMap());
		

	}

}
