package com.bit.sts01.day17.controller;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class IndexController implements Controller {
	Logger log=Logger.getAnonymousLogger();

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.info("call index.do");
		ModelAndView mav=new ModelAndView();
		mav.setViewName("index");
		return mav;
	}

}
