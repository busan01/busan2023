package com.bit.sts01.day17.module;

import java.util.Properties;

public class Module08 {
	private Properties props;
	
	public void setProps(Properties props) {
		this.props = props;
	}
	
	public Properties getProps() {
		return props;
	}
}
