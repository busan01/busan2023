package com.bit.sts01.day17.module;

import java.util.Map;

public class Module07 {
	private Map<String,String> map;
	
	public void setMap(Map<String, String> map) {
		this.map = map;
	}
	public Map<String, String> getMap() {
		return map;
	}
}
