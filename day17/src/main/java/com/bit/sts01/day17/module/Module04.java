package com.bit.sts01.day17.module;

import java.util.Arrays;

public class Module04 {
	private String[] arr;
	
	public void setArr(String[] arr) {
		this.arr = arr;
	}
	
	@Override
	public String toString() {
		return Arrays.toString(arr);
	}
}
