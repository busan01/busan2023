package com.bit.sts01.day17.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class Bbs01DaoImpl extends JdbcDaoSupport implements Bbs01Dao{

	public List<Bbs01Dto> selectAll(){
		String sql="select * from bbs01 order by num desc";
		return this.getJdbcTemplate().query(sql, new RowMapper() {

			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Bbs01Dto(
						rs.getInt("num")
						,rs.getString("id")
						,rs.getString("sub")
						,rs.getString("content")
						,rs.getTimestamp("nalja").toLocalDateTime()
						);
			}
		});
	}
	
	public void insertOne(Bbs01Dto bean) {
		String sql="insert into bbs01 (id,sub,content,nalja) values (?,?,?,now())";
		this.getJdbcTemplate().update(sql,new Object[] {bean.getId(),bean.getSub(),bean.getContent()});
	}
}











