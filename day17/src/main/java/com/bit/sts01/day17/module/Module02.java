package com.bit.sts01.day17.module;

public class Module02 implements Module {

	public void func01() {
		System.out.println("Module02-func01() run...");
	}
	public void func02() {
		System.out.println("Module02-func02() run...");
	}
}
