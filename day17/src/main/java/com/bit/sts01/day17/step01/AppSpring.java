package com.bit.sts01.day17.step01;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bit.sts01.day17.service.OneService;

public class AppSpring {

	public static void main(String[] args) {
		ApplicationContext context;
		context=new ClassPathXmlApplicationContext("/applicationContext.xml");
		OneService service=context.getBean(OneService.class);
		
		service.action01();
		service.action02();
	}

}
