package com.bit.sts01.day17.module;

public class Module01 implements Module {

	public void func01() {
		System.out.println("module01-func01 run...");
	}

	public void func02() {
		System.out.println("module01-func02 run...");
	}
}
