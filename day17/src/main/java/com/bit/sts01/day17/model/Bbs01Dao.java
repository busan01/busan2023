package com.bit.sts01.day17.model;

import java.sql.SQLException;
import java.util.List;

public interface Bbs01Dao {
	public List<Bbs01Dto> selectAll() throws SQLException;
	public void insertOne(Bbs01Dto bean) throws SQLException;
}












