package com.bit.sts01.day17.step02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bit.sts01.day17.module.Module04;

public class App02 {

	public static void main(String[] args) {
		ApplicationContext ac=new ClassPathXmlApplicationContext("/applicationContext.xml");
		Module04 module=ac.getBean(Module04.class);
		System.out.println(module.toString());

	}

}
