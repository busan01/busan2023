package com.bit.sts01.day17.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.bit.sts01.day17.model.Bbs01Dao;
import com.bit.sts01.day17.model.Bbs01DaoImpl;
import com.bit.sts01.day17.model.Bbs01Dto;

public class InsertController implements Controller {
	Bbs01Dao dao;
	
	public void setDao(Bbs01Dao dao) {
		this.dao = dao;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("redirect:list.do");
		String sub=request.getParameter("sub");
		//sub.length()
		String id=request.getParameter("id");
		String content=request.getParameter("content");
		Bbs01Dto bean = new Bbs01Dto(id,sub,content);
		try {
//			dao.insertOne(id,sub,content);
			dao.insertOne(bean);
		}catch (Exception e) {
			mav.setViewName("add");
			mav.addObject("bean", bean);
			mav.addObject("errMsg", "제목은 10자 이내로 작성합니다");
			return mav;
		}
		return mav;
	}

}
