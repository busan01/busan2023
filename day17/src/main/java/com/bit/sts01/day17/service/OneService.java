package com.bit.sts01.day17.service;

import com.bit.sts01.day17.module.Module;
import com.bit.sts01.day17.module.Module01;
import com.bit.sts01.day17.module.Module02;

public class OneService {
	// IoC 제어의 역전
	private Module module=null;
	
	public OneService() {
	}
	// DI 의존 관계 주입	
	public OneService(Module module) {
		super();
		this.module = module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public void action01() {
//		
//		module=new Module01();
//		module=new Module02();
		
		module.func01();
	}
	public void action02() {
		module.func02();		
	}
}
