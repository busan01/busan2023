package com.bit.sts01.day17.step02;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bit.sts01.day17.module.Module05;

public class App03 {

	public static void main(String[] args) {
		ApplicationContext ac=new ClassPathXmlApplicationContext("/applicationContext.xml");
		Module05 module=ac.getBean(Module05.class);
		System.out.println(module.getList());

	}

}
