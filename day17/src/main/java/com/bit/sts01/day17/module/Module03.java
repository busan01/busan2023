package com.bit.sts01.day17.module;

public class Module03 {
	private int su1;
	private double su2;
	private char ch1;
	private boolean boo;
	private String msg;
	
	public Module03() {
	}

	public void setSu1(int su1) {
		this.su1 = su1;
	}

	public void setSu2(double su2) {
		this.su2 = su2;
	}

	public void setCh1(char ch1) {
		this.ch1 = ch1;
	}

	public void setBoo(boolean boo) {
		this.boo = boo;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "Module03 [su1=" + su1 + ", su2=" + su2 + ", ch1=" + ch1 + ", boo=" + boo + ", msg=" + msg + "]";
	}
	
}
