package com.bit.sts01.day17.module;

import java.util.Set;

public class Module06 {
	private Set<String> set;
	public void setSet(Set<String> set) {
		this.set = set;
	}
	public Set<String> getSet() {
		return set;
	}
}
