import Link from "next/link";

 
export default function Layout({ children }) {
  return (
    <>
    <Link replace href={{pathname:'/'}} >home</Link>{' '}
    <Link replace href={{pathname:'/dept'}} >dept</Link>{' '}
    <Link replace as={'/imgs'} href={{pathname:'/imgs/dog'}}  >img</Link>{' '}
    <Link replace href={{pathname:'/count'}} >count</Link>{' '}
    <Link replace href={{pathname:'/user'}} >userInfo</Link>
      <main>{children}</main>
      <p>footer</p>
    </>
  )
}