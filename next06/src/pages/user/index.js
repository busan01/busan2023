import React, { useEffect } from 'react';
import { useSelector } from 'react-redux'
import { useAppDispatch } from '../../store';
import { loadUser } from "../../store/userSlice";
import Link from 'next/link';
import Layout from './layout';

function UserProfile () {
  const dispatch = useAppDispatch(); 
  const {user} = useSelector((state) => state.users);
  
  useEffect(()=> {
    /* 
      위에서 createAsyncTunk로 생성해서 export한 유저 정보를 받아오는 비동기 액션 함수를 
      함수 컴포넌트가 렌더링 될 때 실행되게 useEffect에서 dispatch 하였다.
    */
    dispatch(loadUser(1));
    return ()=>{}
  }, []);
  
  return(
    <Layout>
      <div>
          <div>
            <p>성 : 
            <span>{user?.first_name}</span>
            </p>
          </div>
          <div>
            <p>이름 : 
            <span>{user?.last_name}</span>
            </p>
                
          </div>
          <div>
            <p>이메일 : 
            <span>{user?.email}</span>
            </p>
          </div>
      </div>
    </Layout>
  );
}

export default UserProfile;
