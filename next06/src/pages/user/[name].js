import React, { useEffect } from 'react';
import { useSelector } from 'react-redux'
import wrapper, { useAppDispatch } from '../../store';
import { loadUser } from "../../store/userSlice";
import { useRouter } from 'next/router';
import Link from 'next/link';
import Layout from './layout';

// export const getStaticProps = wrapper.getStaticProps( 
//   (store) => async ({preview}) => {
//     // 필요한 작업을 작성하는 구간
//    await store.dispatch(loadUser());
//    return {
//      // props: {}  -> 반환할 값 입력 구간
//     revalidate: 60 // 60초 마다 getStaticProps를 재 실행한다.
//   }
// });
export const getServerSideProps = wrapper.getServerSideProps(
  store => async ({req, res, ...etc}) => {
  await store.dispatch(loadUser(useRouter().query.name));
  return {
    // props: {} -> 반환할 값이 있을경우 작성
  };
});

function UserProfile () {
  const router=useRouter();
  // const dispatch = useAppDispatch(); 
  const {user} = useSelector((state) => state.users);
  
  return(
    <Layout>
      <div>
          <div>
            <p>성 : 
            <span>{user?.first_name}</span>
            </p>
          </div>
          <div>
            <p>이름 : 
            <span>{user?.last_name}</span>
            </p>
                
          </div>
          <div>
            <p>이메일 : 
            <span>{user?.email}</span>
            </p>
          </div>
      </div>
    </Layout>
  );
}

export default UserProfile;