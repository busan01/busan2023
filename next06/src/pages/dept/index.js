import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { useEffect } from 'react'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  // new IntersectionObserver((a,b)=>{},{})
  useEffect(()=>{
    const count = 10 // 한 번 새로운 item들이 추가될 때 추가되는 item의 갯수
let index = 0 // item의 index

// 옵션 객체
const options = {
    // null을 설정하거나 무엇도 설정하지 않으면 브라우저 viewport가 기준이 된다.
    root: null,
    // 타겟 요소의 10%가 루트 요소와 겹치면 콜백을 실행한다.
    threshold: 0.1,
    trackVisibility: false
}
let observer = new IntersectionObserver(function(entries, observer) {
    console.log(entries);
    entries.forEach(entry => {
    const list = document.querySelector('.list')
	// 타겟 요소가 루트 요소와 교차하는 점이 없으면 콜백을 호출했으되, 조기에 탈출한다.
    if (entry.intersectionRatio <= 0) return
    // 타겟 요소와 루트 요소가 교차하면
    if (entry.isIntersecting) {
        for (let i = index; i < index + count; i++) {
        // item을 count 숫자 만큼 생성하고 list에 추가해주기
        let item = document.createElement('p')
        
        item.keys='key'+i
        item.textContent = i
        item.className += 'item'
        list.appendChild(item)
        }
        
        // index에 +count해서 갱신해주기
        index += count
    }
    })
} ,options)
// list의 끝부분을 알려주는 p 타겟 요소를 관찰
observer.observe(document.querySelector('.list-end'))
  },[]);
  return (
    <>
      <main>
        <div className="list">
        </div>
        <p className="list-end"></p>
      </main>
      <script src='../loop.js'></script>
    </>
    )
}
