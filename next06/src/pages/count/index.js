import { useCallback } from 'react'
import { useSelector } from 'react-redux' // 스토어에서 state를 불러오기 위한 hook
// import { RootState } from '../../store' // 스토어에 저장된 state의 type
import { useAppDispatch } from '../../store' //스토어 생성단계에서 export한 커스텀 dispatch hook
import {increment, decrement} from '../../store/counterSlice'
import Link from 'next/link'

const Home = () => {
  const dispatch = useAppDispatch();
  const {val} = useSelector((state) => state.counter);

  const onIncrement = useCallback(()=>dispatch(increment()),[])
  const onDecrement = useCallback(()=>dispatch(decrement()),[]) 

  return (
    <div>
      <main>
         <div>
              <span>{val}</span>
              <div>
                  <button onClick={onIncrement}>+</button>
                  <button onClick={onDecrement}>-</button>
              </div>
         </div>
      </main>
    </div>
  )
}

export default Home