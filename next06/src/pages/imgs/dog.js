import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { useEffect } from 'react'
import { useRef } from 'react'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  const img=useRef();
  useEffect(()=>{
    const callback=(entries,b)=>{
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          document.querySelector('img').src=document.querySelector('img').dataset.src;
        }
      });
    };
    const obs=new IntersectionObserver(callback,{threshold: 0.1,});
    obs.observe(document.querySelector('img'));
  },[img]);

  return (
    <>
      <main>
        <h2>하단스크롤</h2>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <div>
        <img src='https://via.placeholder.com/500x400'data-msg="hello" data-src='https://img.animalplanet.co.kr/news/2020/07/06/700/da34f5o2h1n2je1uc579.jpg'></img>
        </div>
      </main>
    </>
    )
}
