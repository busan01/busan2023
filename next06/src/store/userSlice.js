import axios from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { createSlice } from "@reduxjs/toolkit";


axios.defaults.baseURL = "https://reqres.in/";

export const loadUser = createAsyncThunk('users/loadUser', async (id, thunkApi) => {
   try {
     const response = await axios.get(`api/users/${id}?delay=3`);
     return response.data.data;
   } catch(err) {
     return err;
     //or return thunkApi.rejectWithValue(err);
   }
})


const initialState = {
   user: null,
  loadUserLoading: false,
  loadUserDone: false,
  loadUserError: null
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  
  //createAsyncThunk에서 전달 된 생명주기 액션들은 여기서 적절한 작업 수행
  extraReducers: (builder) => { 
    builder
      .addCase(loadUser.pending, (state) => {
        state.loadUserLoading = true;
      })
      .addCase(loadUser.fulfilled, (state, action) => {
        state.loadUserDone = true;
        state.user = action.payload;
      })
      .addCase(loadUser.rejected, (state, action) => {
        state.loadUserLoading = false;
        state.loadUserDone = true;
        state.loadUserError = action.payload;
      })
  }
});