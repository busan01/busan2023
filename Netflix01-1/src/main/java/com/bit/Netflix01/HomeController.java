package com.bit.Netflix01;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/dept")
public class HomeController {

	@PostMapping("/")
	public ResponseEntity<?> home(@RequestParam(defaultValue = "") String msg) {
		return ResponseEntity.ok("[\r\n"
				+ "  {\r\n"
				+ "    \"deptNum\": 1,\r\n"
				+ "    \"dname\": \"user01\",\r\n"
				+ "    \"loc\": testarea\r\n"
				+ "  },\r\n"
				+ "  {\r\n"
				+ "    \"userId\": 2,\r\n"
				+ "    \"dname\": \"user02\",\r\n"
				+ "    \"loc\": testarea\r\n"
				+ "  },\r\n"
				+ "  {\r\n"
				+ "    \"userId\": 3,\r\n"
				+ "    \"dname\": \"user03\",\r\n"
				+ "    \"loc\": testarea\r\n"
				+ "  }]");
	}
}
