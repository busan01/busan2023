package com.bit.pm;

public class Ex01 {
	public static int su1=11111;
	private static Ex01 me=new Ex01();
	
	
	private Ex01() {
		System.out.println("객체생성");
	}
	
	public static Ex01 getInstance() {
		return me;
	}

	public void func01() {
		System.out.println("기능");
	}
}
