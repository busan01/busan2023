package com.bit.am;

public class Ex11 {

	public static void main(String[] args) {
		String st1="문자열";
		String st2=new String("문자열");
		String st3="문자열";
		String st4=new String("문자열");
		System.out.println(st1==st2);
		System.out.println(st1==st3);
		System.out.println('a'+'b');
		String st5=new String(new char[] {'a','b'});
		System.out.println(st5);
		String st6=new String(new byte[] {-22, -80, -128, -22, -80, -127});
		System.out.println(st6);
	}

}
