package com.bit.am;

class Car07{
	public int speed=0;
	public int max=150;
	public final String model;
	public String color;
	
	public Car07() {
		this("그렌저",180);
	}
	
	public Car07(String model,int max) {
		this(model,max,"쥐색");
	}
	
	public Car07(String model,int max,String color) {
		this.model=model;
		this.max=max;
		this.color=color;
	}
	
	public void speedUp() {
		if(speed+13<=max)
			speed+=13;
		else
			speed=max;
	}
	
	public void speedDown() {
		if(speed<17)
			speed=0;
		else
			speed-=17;
	}
	
	public void showSpeed() {
		System.out.println(color+" "+model+"은 현재 속도는 "+speed+"km 입니다");
	}
}

public class Ex07 {

	public static void main(String[] args) {
		Car07 myCar=new Car07("모닝",120);
		myCar.max=160;
		myCar.showSpeed();
		for(int i=0; i<20; i++) {
			myCar.speedUp();
			myCar.showSpeed();
		}
		for(int i=0; i<20; i++) {
			myCar.speedDown();
			myCar.showSpeed();
		}
		myCar=new Car07("아반떼", 150,"빨강색");
		myCar.showSpeed();
		for(int i=0; i<20; i++) {
			myCar.speedUp();
			myCar.showSpeed();
		}
		myCar.color="얼룩무늬";
		for(int i=0; i<20; i++) {
			myCar.speedDown();
			myCar.showSpeed();
		}
	}

}













