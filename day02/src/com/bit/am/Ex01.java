package com.bit.am;

public class Ex01 {
	//메서드
//	[접근제한자] [static] 리턴타입 메서드명(매개변수...) {}
	public static void func01() {
		System.out.println("메서드1");
		double su=func02(2,3.14);
		System.out.println(su);
	}
	
	public static double func02(int su,double su2) {
		System.out.println("메서드"+su);
		int a=2;
		return su2;
	}
	
	public static void main(String[] args) {
		int a=1;
		System.out.println("main");
		func01();
		System.out.println("a="+a);
	}

}
