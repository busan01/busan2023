package com.bit.am;

class Lec09{
	
	public final void func01() {}
}

public class Ex09 extends Lec09{
	int su3=3333;
	final int su4=4444;
	
	public static void func01(final int su5) {
		System.out.println(su5);
	}

	public static void main(String[] args) {
		int su1=1111;
		final int su2=2222;
		func01(55555);
		func01(6666);
		new Ex09().func01();
	}

}
