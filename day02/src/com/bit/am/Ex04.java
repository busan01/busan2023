package com.bit.am;

public class Ex04 {
	// 접근제한자 클래스명(매개변수...){}
	int su;
	public Ex04() {
		this(1111);
	}
	public Ex04(int a) {
		super();
		su=a;
		func01();
		return;
	}
	
	public void func01() {
		System.out.println("non-static method"+su);
	}

	public static void main(String[] args) {
		// 생성자
		Ex04 me=new Ex04();
		Ex04 you=new Ex04(2222);
//		me.func01();
//		you.func01();

	}

}













