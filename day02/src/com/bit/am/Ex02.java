package com.bit.am;

/*
 * static		call 	static		[클래스명.]메서드명();   
 * static		call 	non-static	참조변수.메서드명(); 
 * non-static	call	non-static	[this.]메서드명();           참조변수.메서드명();	 
 * non-static	call	static		[클래스명.]메서드명();
 *  
 */
public class Ex02 {
	public void func03() {
		System.out.println("non-static function3");
	}
	
	public void func02(Ex02 me) {
		System.out.println("non-static function2");
		me.func03();
		this.func03();
		System.out.println(me==this);
	}
	
	public static void func01() {
		System.out.println("static function");
	}

	public static void main(String[] args) {
		Ex01.func01();
		Ex02.func01();
		Ex02 me=new Ex02();
		me.func02(me);
	}

}
