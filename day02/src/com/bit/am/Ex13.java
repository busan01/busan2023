package com.bit.am;

import java.util.Scanner;

public class Ex13 {

	public static void main(String[] args) {
		// 주민번호 체크
		// 주민번호를 입력하세요> 991231-1234567
		// 당신은 25세 남성입니다
		// 
		// 주민번호를 입력하세요> 9912311234567
		// 입력을 확인하세요 (000000-0000000)
		// 주민번호를 입력하세요> 991231-234567
		// 입력을 확인하세요 (6자리-7자리)
		// 주민번호를 입력하세요> a91231-1234567
		// 입력을 확인하세요 (숫자로 입력하세요)
		// 주민번호를 입력하세요> 001231-4234567
		// 당신은 24세 여성입니다
		
		Scanner sc=new Scanner(System.in);
		System.out.print("주민번호를 입력하세요> ");
		String input=sc.nextLine();
		char[] arr=input.toCharArray();
		int age=2023-(1900+(arr[0]-'0')*10+(arr[1]-'0'))+1;
		if(arr[7]>'2') age-=100;
		System.out.println(age+"세"+(arr[7]%2=='1'%2?'남':'여')+"자");
	}

}













