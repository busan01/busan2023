package com.bit.am;

public class Ex08 {		// public default
	public int su1;		//모두 접근허용
//	protected int su4;
	int su2;			// 동일패키지내에서만 접근허용
	private int su3;	// 클래스 내부에서만 접근
	
	
	public Ex08() {
		
	}
	
	public void func01() {
		System.out.println("기능");
	}

	public static void main(String[] args) {
//		System.out.println(com.bit.pm.Ex01.su1);
//		com.bit.pm.Ex01 you=new  com.bit.pm.Ex01();
//		System.out.println(you.getClass());
//		com.bit.pm.Ex01.getInstance().func01();
////		com.bit.pm.Ex01.getInstance()=null;
//		com.bit.pm.Ex01.getInstance().func01();
//		com.bit.pm.Ex01.getInstance().func01();
		com.bit.pm.Ex01 you=com.bit.pm.Ex01.getInstance();
		you.func01();
		you=com.bit.pm.Ex01.getInstance();
		you.func01();
		you.func01();
	}

}






