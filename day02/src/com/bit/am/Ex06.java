package com.bit.am;

class Car06{
	public static int speed=0;
	public static int max=150;
	
	public static void speedUp() {
		if(speed+13<=max)
			speed+=13;
		else
			speed=max;
	}
	
	public static void speedDown() {
//		if(speed-15>=0)
//			speed-=15;
//		else
//			speed=0;
		if(speed<17)
			speed=0;
		else
			speed-=17;
	}
	
	public static void showSpeed() {
		System.out.println("현재 속도는 "+speed+"km 입니다");
	}
}


public class Ex06 {

	public static void main(String[] args) {
		Car06.showSpeed();
		for(int i=0; i<20; i++) {
			Car06.speedUp();
			Car06.showSpeed();
		}
		for(int i=0; i<20; i++) {
			Car06.speedDown();
			Car06.showSpeed();
		}
	}

}
