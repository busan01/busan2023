import React from "react";

class Ex22 extends React.Component{
    constructor(props){super(props);
        this.msg=props.msg;
    }
    render(){
        return <p>내용{" "}
            <strong>{this.msg}</strong>
            <a href="#">link</a>
        </p>;
    }
}

class Ex02 extends React.Component{
    constructor(){
        super();
    }

    render(){
        return (
            <div>
                <h2>Ex02 page</h2>
                <Ex22 msg="item1"/>
                <Ex22 msg="item2"/>
                <Ex22 msg="item3"/>
                <Ex22 msg="item4"/>
            </div>
        );
    }
}

export default Ex02;