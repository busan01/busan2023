import React from 'react'
import Ex01 from './Ex01';

function Li03({msg,num}){
    return <li>{msg} - {num}</li>
}

function Ex03() {
    let su=1;
  return (
    <div>
        <h2>Ex03 page</h2>
        <Ex01/>
        <ul>
            <Li03 msg='item1' num={su++}/>
            <Li03 msg='item2' num={su++}/>
            <Li03 msg='item3' num={su++}/>
            <Li03 msg='item4' num={su++}/>
        </ul>
    </div>
  )
}

export default Ex03