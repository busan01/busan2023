import React, { useState } from 'react'

function Ex04() {
    const [arr,setArr]=useState([
        {num:1,sub:'sub1',user:'tester'},
        {num:2,sub:'sub2',user:'tester'},
        {num:3,sub:'sub3',user:'tester'},
        {num:4,sub:'sub4',user:'tester'},
        {num:5,sub:'sub5',user:'tester'},
    ]);
    const [msg,setMsg]=useState('');

    const addArr=()=>{
        const max=arr.reduce((prv,nxt)=>prv.num>nxt.num?prv:nxt);

        setArr([...arr,{num:max.num+1,sub:msg,user:'tester'}]);
        setMsg('');
    };

    function inputChange(ele){
        setMsg(ele.value);
    }
    function removeArr(num){
        const arr2=arr.filter(ele=>ele.num!==num);
        setArr(arr2);
    }

    return (
        <div>
            <ul>
                {arr.map(ele=><li key={ele.num}>
                    <span style={{marginRight:50}}>{ele.num}</span>
                    <span style={{marginRight:50}}>{ele.sub}</span>
                    <span style={{marginRight:50}}>{ele.user}</span>
                    <span onClick={e=>{
                        removeArr(ele.num);
                    }} style={{color:'red',cursor:'pointer'}}>X</span>
                </li>)}
            </ul>
            <input value={msg} onChange={(e)=>{inputChange(e.target);}}/>
            <button onClick={(e)=>{addArr()}}>추가</button>
        </div>
    )
}

export default Ex04