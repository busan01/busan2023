package com.bit.am;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Ex01 {
	static File f=new File("test01.bin");

	public static void main(String[] args) throws IOException {
		f.createNewFile();
		
		try(
				OutputStream os=new FileOutputStream(f);
				BufferedOutputStream bos=new BufferedOutputStream(os);
				){
			bos.write("abcde".getBytes());
		}
		System.out.println("작성완료");
	}

}








