package com.bit.am;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class Ex06 {

	public static void main(String[] args) {
		File file=new File("test03.txt");

		try(
				OutputStream os=new FileOutputStream(file);
				PrintStream ps=new PrintStream(os);
				){
			ps.println("abcd");
			ps.println("한글");
			ps.println("1234");
			ps.print(1234);
			ps.print(3.14);
			ps.print(true);
			
			System.out.println();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
