package com.bit.am;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class Ex02 {

	public static void main(String[] args) {
//		byte[] buf=new byte[2];
		try(
				InputStream is=new FileInputStream(Ex01.f);
				BufferedInputStream bis=new BufferedInputStream(is);
				){
			int cnt=-1;
			while((cnt=bis.read())!=-1) {
				System.out.println(cnt);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
