package com.bit.pm;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Ex01 {

	public static void main(String[] args) {
		byte[] arr= {127,0,0,1};
		
//		java.net.InetAddress addr=null;
		InetAddress[] addrs=null;
		try {
//			addr=InetAddress.getByAddress(arr);
//			addr=InetAddress.getByName("google.com");
			addrs=InetAddress.getAllByName("naver.com");
			
//			System.out.println(addr.getHostName());
			for(InetAddress addr : addrs)
			System.out.println(addr.getHostAddress());
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}

}
