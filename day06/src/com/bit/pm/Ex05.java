package com.bit.pm;

import java.awt.Frame;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Ex05 extends Frame implements ActionListener {
	static TextField tf;
	static PrintStream ps;
	
	public Ex05() {
		tf=new TextField();
		tf.addActionListener(this);
		add(tf);
		setBounds(100,100,500,100);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ps.println(tf.getText());
		tf.setText("");
	}

	public static void main(String[] args) {
		Ex05 me=new Ex05();
//		byte[] addr= {127,0,0,1};
		byte[] addr= {(byte)192,(byte)168,20,25};
		int port=3000;
		
		java.net.Socket sock=null;
		try {
			sock=new Socket(InetAddress.getByAddress(addr),port);
			OutputStream os = sock.getOutputStream();
			InputStream is = sock.getInputStream();
			InputStreamReader isr=new InputStreamReader(is);
			BufferedReader br=new BufferedReader(isr);
			ps=new PrintStream(os);
			String msg=null;
			while((msg=br.readLine())!=null) {
				System.out.println(">>"+msg);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(sock!=null)sock.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}


}
