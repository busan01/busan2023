package com.bit.cs;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client extends Frame implements ActionListener {
	static TextArea ta;
	static TextField tf;
	static PrintStream ps;
	
	public Client() {
		setLayout(new BorderLayout());
		
		ta=new TextArea();
		tf=new TextField();
		tf.addActionListener(this);
		add(BorderLayout.CENTER,ta);
		add(BorderLayout.SOUTH,tf);
		
		setBounds(100,100,300,600);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String msg=tf.getText();
		ps.println(msg);
		tf.setText("");
	}

	public static void main(String[] args) {
		new Client();
		try(
				Socket sock=new Socket("192.168.20.25",3000);
				InputStream is=sock.getInputStream();
				OutputStream os=sock.getOutputStream();
				InputStreamReader isr=new InputStreamReader(is);
				BufferedReader br=new BufferedReader(isr);
				){
			ps=new PrintStream(os);
			while(true) {
				String msg=br.readLine();
				ta.setText(ta.getText()+msg+"\n");
			}
				
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}














