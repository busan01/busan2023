package com.bit.boot08.mapper.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BbsAuth03Vo {
	int num;
	String email, auth;
}
