package com.bit.boot08.mapper.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bbs03Vo {
	int num;
	String sub,id,content;
}
