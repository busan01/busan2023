package com.bit.boot08.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.bit.boot08.mapper.domain.BbsUser03Vo;

@Mapper
public interface BbsUser03Mapper {

	@Select("select email,pw,name from bbsUser03 where email=#{email}")
	BbsUser03Vo findByEmail(String email);
}
