package com.bit.boot08.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.bit.boot08.mapper.domain.Bbs03Vo;

@Mapper
public interface Bbs03Mapper {

	@Select("select * from bbs03 order by num")
	List<Bbs03Vo> findAll();
}
