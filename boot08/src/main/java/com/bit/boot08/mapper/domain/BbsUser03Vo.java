package com.bit.boot08.mapper.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BbsUser03Vo {
	int num;
	String email,name,pw;
	List<String> auths;
}
