package com.bit.boot08.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface BbsAuth03Mapper {

	@Select("select auth from bbsAuth03 where email=#{email}")
	List<String> findByAuth(String email);
}
