package com.bit.boot08.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bit.boot08.service.Bbs03Service;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class Bbs03Controller {
	final Bbs03Service bbs03Service;
	
	@GetMapping("/")
	public ResponseEntity<?> list(){
		return ResponseEntity.ok(bbs03Service.selectAll());
	}
}












