package com.bit.boot08.controller;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bit.boot08.service.MyUserDetails;

@Controller
public class HomeController {
	
	@GetMapping("/")
	public String index() {
		return "index";
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	@GetMapping("/info")
	@ResponseBody
	public UserDetails info(Authentication authentication,@AuthenticationPrincipal UserDetails userDetails,Principal principal) {
		Object principal2=authentication.getPrincipal();
		UserDetails my=(UserDetails)principal2;
		User my2=(User)principal2;
		System.out.println(my);
		System.out.println(my2);
		System.out.println(userDetails);
		System.out.println(principal);
		
		SecurityContext context = SecurityContextHolder.getContext();
		System.out.println(context.getAuthentication().getDetails());
		System.out.println(context.getAuthentication().getName());
		System.out.println(context.getAuthentication().getAuthorities());
		System.out.println(context.getAuthentication().getPrincipal());
		
		return my;
	}
}













