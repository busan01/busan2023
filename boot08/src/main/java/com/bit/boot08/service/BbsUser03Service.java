package com.bit.boot08.service;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

import com.bit.boot08.mapper.BbsAuth03Mapper;
import com.bit.boot08.mapper.BbsUser03Mapper;
import com.bit.boot08.mapper.domain.BbsUser03Vo;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BbsUser03Service {
	final SqlSession sqlSession;
	
	public BbsUser03Vo selectOne(String email) {
		BbsUser03Mapper mapper1=sqlSession.getMapper(BbsUser03Mapper.class);
		BbsAuth03Mapper mapper2=sqlSession.getMapper(BbsAuth03Mapper.class);
		BbsUser03Vo bean = mapper1.findByEmail(email);
		bean.setAuths(mapper2.findByAuth(email));
		return bean;
	}
}
