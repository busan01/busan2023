package com.bit.boot08.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

import com.bit.boot08.mapper.Bbs03Mapper;
import com.bit.boot08.mapper.domain.Bbs03Vo;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class Bbs03Service {
	final SqlSession sqlSession;

	public List<Bbs03Vo> selectAll(){
		Bbs03Mapper mapper=sqlSession.getMapper(Bbs03Mapper.class);
		return mapper.findAll();
	} 
}
















