package com.bit.boot08.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bit.boot08.mapper.domain.BbsUser03Vo;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SecurityDetailService implements UserDetailsService {
	final BbsUser03Service service;
	final PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		
		BbsUser03Vo bean = service.selectOne(email);
		ArrayList arr=new ArrayList<>();
		bean.getAuths().forEach(auth->arr.add(new SimpleGrantedAuthority(auth)));
		
//		return new MyUserDetails(
//				bean.getName(),bean.getEmail(), passwordEncoder.encode(bean.getPw()),arr);
		return MyUserDetails.builder()
				.name(bean.getName())
				.email(bean.getEmail())
				.password(bean.getPw())
				.authorities(arr)
				.build();
	}

}










