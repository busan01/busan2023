package com.bit.boot08.service;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@ToString
public class MyUserDetails implements UserDetails{ // extends User{
	@Getter
	String name;
	Collection<? extends GrantedAuthority> authorities;
	String email, password;

//	public MyUserDetails(String name,String email, String password, Collection<? extends GrantedAuthority> authorities) {
////		super(email, password, authorities);
//		this.name=name;
//	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return email;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}


}
