insert into users (enabled,password,username) values ('TRUE','{bcrypt}$2a$10$ikoTFDsQsDkfO/f3A/wmb.P4hsSz0.IgDE79k8N/P9PaA6l7IDo7q','user11');
INSERT INTO AUTHORITIES (AUTHORITY,username) VALUES ('ROLE_USER','user11');
insert into users (enabled,password,username) values ('TRUE','{bcrypt}$2a$10$ikoTFDsQsDkfO/f3A/wmb.P4hsSz0.IgDE79k8N/P9PaA6l7IDo7q','user12');
INSERT INTO AUTHORITIES (AUTHORITY,username) VALUES ('ROLE_USER','user12');
insert into users (enabled,password,username) values ('TRUE','{bcrypt}$2a$10$ikoTFDsQsDkfO/f3A/wmb.P4hsSz0.IgDE79k8N/P9PaA6l7IDo7q','user13');
INSERT INTO AUTHORITIES (AUTHORITY,username) VALUES ('ROLE_USER','user13');