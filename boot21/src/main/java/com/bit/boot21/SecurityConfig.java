package com.bit.boot21;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
//	@Bean
//	public WebSecurityCustomizer webSecurityCustomizer() {
//		 return (web) -> web.ignoring().requestMatchers(req->req.getRequestURI().startsWith("/h2-console"));
//	}
//	@Autowired
//	UserDetailsService userDetailsService;
//
//	DigestAuthenticationEntryPoint entryPoint() {
//		DigestAuthenticationEntryPoint result = new DigestAuthenticationEntryPoint();
//		result.setRealmName("My App Realm");
//		result.setKey("3028472b-da34-4501-bfd8-a355c42bdf92");
//		return result;
//	}
//
//	DigestAuthenticationFilter digestAuthenticationFilter() {
//		DigestAuthenticationFilter result = new DigestAuthenticationFilter();
//		result.setUserDetailsService(userDetailsService);
//		result.setAuthenticationEntryPoint(entryPoint());
//		return result;
//	}


	@Bean
	SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http 
			.csrf().disable()
			.headers().frameOptions().disable().and()
			.httpBasic(Customizer.withDefaults())
//			.httpBasic((basic) -> basic
//	            .addObjectPostProcessor(new ObjectPostProcessor<BasicAuthenticationFilter>() {
//	                @Override
//	                public <O extends BasicAuthenticationFilter> O postProcess(O filter) {
//	                    filter.setSecurityContextRepository(new HttpSessionSecurityContextRepository());
//	                    return filter;
//	                }
//	            })
//	        )
			.authorizeHttpRequests(cust->cust
					.requestMatchers(AntPathRequestMatcher.antMatcher("/")).permitAll()
					.requestMatchers(AntPathRequestMatcher.antMatcher("/user")).permitAll()
					.requestMatchers(AntPathRequestMatcher.antMatcher("/h2-console/**")).permitAll()
					.anyRequest().authenticated())
//			.authorizeHttpRequests(auth->
//				auth
//				.mv("/").permitAll()
//				.anyRequest().authenticated()
//			)
//			.sessionManagement((session) -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
//			.exceptionHandling(e -> e.authenticationEntryPoint(authenticationEntryPoint()))
//			.addFilterBefore(digestFilter());
			.addFilterBefore(getLoginFilter(),SecurityContextPersistenceFilter.class)
			.formLogin(form->form.isCustomLoginPage())
			.logout(form->form.logoutUrl("/logout").logoutSuccessUrl("/"))
			;
		return http.build();
	}
	@Bean
	LoginFilter getLoginFilter() {
		return new LoginFilter();
	}
	@Bean
	public UserDetailsService userDetailsService(DataSource dataSource) {
	    JdbcUserDetailsManager manager = new JdbcUserDetailsManager((dataSource));
	    manager.createUser(User.withDefaultPasswordEncoder().username("user1").password("1234").roles("USER").build());
	    manager.createUser(User.withDefaultPasswordEncoder().username("user2").password("1234").roles("USER").build());
	    manager.createUser(User.withDefaultPasswordEncoder().username("user3").password("1234").roles("USER").build());
	    return manager;
	}
	@Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
	@Bean
    public AuthenticationManager authenticationManager(
            UserDetailsService userDetailsService,
            PasswordEncoder passwordEncoder) {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);

        return new ProviderManager(authenticationProvider);
    }
}
