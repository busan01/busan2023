package com.bit.boot21;

import java.io.IOException;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoginFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		System.out.println(SecurityContextHolder.getContext().getAuthentication());
		if(request.getRequestURI().startsWith("/info")&&SecurityContextHolder.getContext().getAuthentication()==null) {
			UsernamePasswordAuthenticationToken token = UsernamePasswordAuthenticationToken.authenticated(
					User.builder().username("user2").password("1234").build(),null,Arrays.asList(new SimpleGrantedAuthority("USER")));
	        SecurityContext context = SecurityContextHolder.createEmptyContext();
	        context.setAuthentication(token);
	        SecurityContextHolder.getContext().setAuthentication(token);
			SecurityContextRepository securityContextRepository =
					new HttpSessionSecurityContextRepository();
			securityContextRepository.saveContext(context, request, response);
		}
		System.out.println("사용자필터");
		System.out.println(SecurityContextHolder.getContext());
		filterChain.doFilter(request, response);	
	}

}
