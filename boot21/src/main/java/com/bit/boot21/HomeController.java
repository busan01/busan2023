package com.bit.boot21;

import java.util.Arrays;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Controller
public class HomeController {
	private SecurityContextRepository securityContextRepository =
	        new HttpSessionSecurityContextRepository();
	private final AuthenticationManager authenticationManager;
	private final SecurityContextHolderStrategy securityContextHolderStrategy = SecurityContextHolder.getContextHolderStrategy();

	@ResponseBody
	@GetMapping("/info")
	public String info(@CurrentSecurityContext SecurityContext context) {
		return "["+context.getAuthentication().getName()
				+ "] <p><a href=\"/\">home</a> <a href=\"/info\">info</a> <a href=\"/user\">user</a>"
				+ " <a href=\"/login\">login</a> <a href=\"/logout\">logout</a></p><h4>info</h4>";
	}

	@ResponseBody
	@GetMapping("/info2")
	public String info2(@CurrentSecurityContext SecurityContext context) {
		return "["+context.getAuthentication().getName()
				+ "] <p><a href=\"/\">home</a> <a href=\"/info\">info</a> <a href=\"/info2\">info2</a> <a href=\"/user\">user</a>"
				+ " <a href=\"/login\">login</a> <a href=\"/logout\">logout</a></p><h4>info</h4>";
	}
	
	@ResponseBody
	@GetMapping("/user")
    public String someMethod(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException, Exception {
        UsernamePasswordAuthenticationToken token = UsernamePasswordAuthenticationToken.unauthenticated("user11", "1234");//UsernamePasswordAuthenticationToken.authenticated("user", "password",Arrays.asList(new SimpleGrantedAuthority("USER")));
        Authentication authentication = this.authenticationManager.authenticate(token);
        SecurityContext context2 = this.securityContextHolderStrategy.createEmptyContext(); 
        context2.setAuthentication(authentication); 
        this.securityContextHolderStrategy.setContext(context2);
        securityContextRepository.saveContext(context2, req, res);
//		UsernamePasswordAuthenticationToken token = UsernamePasswordAuthenticationToken.authenticated(
//				User.builder().username("user2").password("1234").build(),null,Arrays.asList(new SimpleGrantedAuthority("USER")));
//        SecurityContextHolder.getContext().setAuthentication(token);
		return "["+context2.getAuthentication().getName()
				+ "] <p><a href=\"/\">home</a> <a href=\"/info\">info</a> <a href=\"/logout\">logout</a></p>";
    }

	@ResponseBody
	@GetMapping("/")
	public String method(Authentication authentication,@CurrentSecurityContext SecurityContext context) {
		log.info(context.toString());
		if (authentication instanceof AnonymousAuthenticationToken) {
			return "anonymous";
		} else {
			return "["+context.getAuthentication().getName()
					+ "] <p><a href=\"/\">home</a> <a href=\"/info\">info</a> <a href=\"/user\">user</a>"
					+ " <a href=\"/login\">login</a> <a href=\"/logout\">logout</a></p><h4>index</h4>";
		}
	}
}
