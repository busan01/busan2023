package com.bit.boot18.entity;

import lombok.Data;

@Data
public class UserUpdateDto {
    private Long id;
    private User user;
}
