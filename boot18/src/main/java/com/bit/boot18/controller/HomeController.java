package com.bit.boot18.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bit.boot18.entity.User;
import com.bit.boot18.entity.UserUpdateDto;
import com.bit.boot18.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class HomeController {
	private final UserService userService;

    @GetMapping
    public List<User> findAll() {
        return userService.findAll();
    }

    @GetMapping("/{userId}")
    public User findByIndexId(@PathVariable(value = "userId") Long userId ) {
        return userService.findByIndexId(userId);
    }

    @PostMapping
    public String save(@RequestBody User user) {
        return userService.save(user);
    }

    @PatchMapping
    public String update(@RequestBody UserUpdateDto user) {
        return userService.update(user.getId(), user.getUser());
    }

    @DeleteMapping
    public String delete(@RequestParam Long id) {
        return userService.delete(id);
    }
}
