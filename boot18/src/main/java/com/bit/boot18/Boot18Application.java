package com.bit.boot18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.bit.boot18.config.RedisConfig;

@SpringBootApplication
@Import({RedisConfig.class})
public class Boot18Application {

	public static void main(String[] args) {
		SpringApplication.run(Boot18Application.class, args);
	}

}
