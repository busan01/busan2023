package com.bit.boot18.repo;

import org.springframework.data.repository.CrudRepository;

import com.bit.boot18.entity.User;

public interface UserQueryRepository extends CrudRepository<User, Integer> {

}
