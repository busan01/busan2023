package com.bit.boot18.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bit.boot18.entity.User;

public interface UserSpringJpaRepository extends JpaRepository<User, Long> {

	Optional<User> findByUserId(String string);

}
