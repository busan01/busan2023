
import { redirect, useRouter } from 'next/navigation';
import React from 'react'

export default function three() {
  const router=useRouter();
  const add= (e)=>{
    e.preventDefault();
    fetch('/api/user',{
      method:'post',
      body: JSON.stringify({name:e.target.name.value,email:e.target.email.value})
    }).then(e=>e.ok?
    router.push('/dept/two'):null
    )
  }
  return (
    <div>
    <h1>add</h1>
    <form onSubmit={add}>
      <label>name<input name='name'/></label>
      <label>email<input name='email'/></label>
      <button>입력</button>
      <button type='button' onClick={e=>router.back()}>back</button>
    </form>
    </div>

  )
}

