import { useRouter } from "next/router";


export default function Id(props) {
  const {data}=props;
  const router = useRouter();
  console.log(props,router);
  if (router.isFallback) {
    return (
      <div>
        Loading...
      </div>
    )
  }
  return (<>
  <h1>{data.title}</h1>
  </>);
}

export async function getStaticPaths() {
    // const paths = ["/dept/1","/dept/2"]
    const paths = [{ params: { id: '1' } },
    { params: { id: '2' } }]
    return {
      paths,
      fallback:true,//'blocking',// true//false,
    };
}

export async function getStaticProps({ params }) {
    // const data = await(await fetch(`https://jsonplaceholder.typicode.com/todos/${params.id}`)).json();
    // return {
    //     props: {data,},
    // };
    return new Promise((res,rej)=>{
        setTimeout(async() => {
            const data = await(await fetch(`https://jsonplaceholder.typicode.com/todos/${params.id}`)).json();
            res({props:{data}});
    },5000);
    });
  }