import Link from 'next/link';
import React from 'react'

export default function One({data}) {
  return (
    <div>one page

        <ul>
            {data.map(el=><li key={el.id}>
                <Link href={`/dept/${el.id}`}>
                {el.title.slice(0,30)} {el.title.length>30?'...':null}
                </Link>
                </li>)}
        </ul>
    </div>
    
  )
}

export async function getServerSideProps() {
    // export async function getStaticProps() {
    // Get external data from the file system, API, DB, etc.
    const data = await(await fetch('https://jsonplaceholder.typicode.com/todos/?_start=0&_end=9')).json();
  
    // The value of the `props` key will be
    //  passed to the `Home` component
    return {
      props: {data}
    }
  }
