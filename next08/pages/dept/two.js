import { PrismaClient } from '@prisma/client';
import Link from 'next/link';
import React from 'react'

export default function Two({data}) {
  return (
    <div>
    <h1>Two</h1>
    <ul>
      {data.map(
        (el,idx)=>(<li>{el.name}</li>)
      )}
    </ul>
      <Link href={'three'}>add</Link><br/>
      <Link href={'four'}>four {'=>'} /</Link>
      <Link href={'/dept/five'}>five {'=>'} /</Link>
    </div>

  )
}

export async function getServerSideProps() {
  const prisma = new PrismaClient();
  const data = await prisma.user.findMany();
  await prisma.$disconnect();
  return {
    props: {data}
  }
}
