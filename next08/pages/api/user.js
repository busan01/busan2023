import { PrismaClient } from '@prisma/client'
async function main(name,email) {
  const prisma = new PrismaClient();
    const user = await prisma.user.create({
      // data: {name,email,posts:{},},
      data: {name,email,},
    })
    prisma.$disconnect();
    console.log(user)
  }
export default function user(req, res) {
  const {name,email}=JSON.parse(req.body);
  main(name, email);
  res.status(200).json({ name,email })
}
