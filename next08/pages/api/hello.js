import { PrismaClient } from '@prisma/client'
async function main() {
  const prisma = new PrismaClient();
    const user = await prisma.user.create({
      data: {
        name: 'Alice',
        email: 'alice@prisma.io',
      },
    })
    prisma.$disconnect();
    console.log(user)
  }
export default function handler(req, res) {
  main();
  res.status(200).json({ name: 'John Doe' })
}
