/** @type {import('next').NextConfig} */
const nextConfig = {
  // reactStrictMode: true,
  async redirects() {
    return [
      {
        source: "/dept/five",
        destination: "/",
        permanent: true,
      },
    ];
  },
};

module.exports = nextConfig;
