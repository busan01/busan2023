package com.bit.boot07.scurity;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserService implements UserDetailsService{

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("param:"+username);
		// Bean bean=findById(username);
		// @select(select id,pw from user where id=username;
//		if(true) throw new UsernameNotFoundException(username+" 사용자가 존재하지 않습니다");
		UserDetails user =
				 User.withDefaultPasswordEncoder()
					.username(username)
					.password("1234")//pw
					.roles("USER","ADMIN")
					.build();
		return user;
	}

}
