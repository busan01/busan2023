package com.bit.sts05.model;

import java.sql.SQLException;
import java.util.List;

import com.bit.sts05.model.entry.Bbs01Vo;

public interface Bbs01Dao {

	List<Bbs01Vo> selectAll() throws SQLException;
	List<Bbs01Vo> selectAll(int limit,int offset) throws SQLException;
	Bbs01Vo selectOne(int num) throws SQLException;
	void insertOne(Bbs01Vo bean) throws SQLException;
	int updateOne(Bbs01Vo bean) throws SQLException;
	int deleteOne(int num) throws SQLException;
}
