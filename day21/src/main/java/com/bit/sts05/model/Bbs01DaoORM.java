package com.bit.sts05.model;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bit.sts05.model.entry.Bbs01Vo;

@Repository
public class Bbs01DaoORM implements Bbs01Dao {
	@Autowired
	SqlSession sqlSession;

	@Override
	public List<Bbs01Vo> selectAll() throws SQLException {
		return sqlSession.selectList("bbs.all");
	}

	@Override
	public List<Bbs01Vo> selectAll(int limit, int offset) throws SQLException {
		Map<String,Integer> params=new HashMap<String,Integer>();
		params.put("limit", limit);
		params.put("offset", offset);
		return sqlSession.selectList("bbs.all2",params);
	}

	@Override
	public Bbs01Vo selectOne(int num) throws SQLException {
		return sqlSession.selectOne("bbs.one",num);
	}

	@Override
	public void insertOne(Bbs01Vo bean) throws SQLException {
		sqlSession.insert("bbs.add",bean);
	}

	@Override
	public int updateOne(Bbs01Vo bean) throws SQLException {
		return sqlSession.update("bbs.edit", bean);
	}

	@Override
	public int deleteOne(int num) throws SQLException {
		return sqlSession.delete("bbs.del",num);
	}

}
