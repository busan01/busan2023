package com.bit.sts05.aop;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.bit.sts05.model.Bbs01Dao;

@Component
@Aspect
public class MyAop {
	Logger log=LoggerFactory.getLogger(Bbs01Dao.class);

	
	@Around(value = "execution(* com.bit.sts05.model.*.*(..))")
	public Object around(ProceedingJoinPoint pjp) {
		Object obj=null;
		log.debug("param:"+Arrays.toString(pjp.getArgs()));
		try {
			obj=pjp.proceed();
			log.debug("return:"+obj.toString());
		} catch (Throwable ex) {
			log.error(ex.getMessage());
		} finally {
		}
		
		return obj;
	}
	
//	@Before("execution(* com.bit.sts05.model.*.select*(..))")
//	public void before(JoinPoint jp) {
//		System.out.println("before...");
//	}
//	@AfterReturning(value = "execution(* com.bit.sts05.model.*.select*(..))",returning = "returnVal")
//	public void afterSuccess(JoinPoint jp,Object returnVal) {
//		System.out.println(returnVal);
//	}
//	@AfterThrowing(value = "execution(* com.bit.sts05.model.*.select*(..))",throwing ="ex")
//	public void afterError(JoinPoint jp,Exception ex) {
//		System.out.println(ex.toString());
//	}
//	@After(value = "execution(* com.bit.sts05.model.*.select*(..))")
//	public void end(JoinPoint jp) {
//		System.out.println("finally...");
//	}
}













