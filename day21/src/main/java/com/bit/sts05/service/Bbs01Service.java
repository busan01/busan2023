package com.bit.sts05.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bit.sts05.model.Bbs01Dao;
import com.bit.sts05.model.entry.Bbs01Vo;

@Service
public class Bbs01Service {

	@Autowired
	Bbs01Dao bbs01Dao;
	
	public List<Bbs01Vo> list() throws SQLException{
		return bbs01Dao.selectAll();
	}
	public List<Bbs01Vo> list(int offset) throws SQLException{
		return bbs01Dao.selectAll(10, offset);
	}
	public List<Bbs01Vo> list(int limit,int offset) throws SQLException{
		return bbs01Dao.selectAll(limit, offset);
	}
	
	public Bbs01Vo one(int num) throws SQLException {
		return bbs01Dao.selectOne(num);
	}
	
	@Transactional
	public void addUpdate(Bbs01Vo bean) throws SQLException {
		bbs01Dao.insertOne(bean);
	}
	@Transactional
	public boolean deleteUpdate(int num) throws SQLException {
		return bbs01Dao.deleteOne(num)>0;
	}
	@Transactional
	public void editUpdate(Bbs01Vo bean) throws SQLException {
		bbs01Dao.updateOne(bean);
	}
}














