<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../layout/header.jspf" %>
<script type="text/javascript">
	$(()=>{
		$('.list-group>a').first().click(e=>false);
		$('.list-group>a').filter(":gt(0)").click(function(e){
			var url=$(this).attr('href');
			$.getJSON(url,function(data){
				$('#editModal').find('input').first().val(data.num);
				$('#editModal').find('input').eq(1).val(data.sub);
				$('#editModal').find('input').eq(2).val(data.id);
				$('#editModal').find('textarea').val(data.content);
			}); 
			/* 
			*/
			$('#editModal').modal('show');
			$('#editModal form').submit(e=>{
				var num=$('#editModal form').find('input').first().val();
				var sub=$('#editModal form').find('input').eq(1).val();
				var content=$('#editModal form').find('textarea').val();
				var url='./'+num;
				var param={num:num,sub:sub,content:content};
				$.ajax({
					url:url,
					method:'put',
					data:JSON.stringify(param),
					contentType:'application/json; charset=utf-8',
					processData:false,
					success:function(){
						location.reload();
					},
					error:function(xhr,errmsg,err){
						alert('실패');
					}
				});
				
				return false;
			});
			return false;
		});
	$('#editModal form').find('button').eq(1).click(e=>{
		var num=$('#editModal form').find('input').first().val();
		var url='./'+num;
		$.ajax({
			url:url,
			method:'delete',
			contentType:'application/json; charset=utf-8',
			processData:false,
			success:function(){
				location.reload();
			},
			error:function(xhr,errmsg,err){
				alert('실패');
			}
		});
	});
	});
</script>
</head>
<body>
<%@ include file="../layout/menu.jspf" %>
<ol class="breadcrumb">
  <li><a href="${pageContext.request.contextPath }/">Home</a></li>
  <li class="active">Bbs</li>
</ol>
<div class="page-header">
  <h1>게시판 </h1>
</div>
<div class="list-group">
  <a href="#" class="list-group-item active">
    <span class="badge">글쓴이</span>
    <h4 class="list-group-item-heading">제목</h4>
    <p class="list-group-item-text">날짜</p>
  </a>
  <c:forEach items="${list }" var="bean">
  <a href="./${bean.num }" class="list-group-item">
    <span class="badge">${bean.id }</span>
    <h4 class="list-group-item-heading">${bean.sub }</h4>
    <p class="list-group-item-text">${bean.nalja }</p>
  </a>
  </c:forEach>
</div>
<nav aria-label="Page navigation">
  <ul class="pagination ">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="./?page=1">1</a></li>
    <li><a href="./?page=2">2</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#myModal">
  입력
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">입력페이지</h4>
      </div>
      <div class="modal-body">
      	<form class="form-horizontal" method="post">
		  <div class="form-group">
		    <label for="sub" class="col-sm-2 control-label">Subject</label>
		    <div class="col-sm-10">
		      <input name="sub" class="form-control" id="sub" placeholder="subject">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="id" class="col-sm-2 control-label">ID</label>
		    <div class="col-sm-10">
		      <input name="id" class="form-control" id="id" placeholder="ID">
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <textarea name="content" class="form-control"></textarea>
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10 text-right">
		      <button type="submit" class="btn btn-primary">입력</button>
		      <button type="reset" class="btn btn-default">취소</button>
	          <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
		    </div>
		  </div>
		</form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<%@ include file="../layout/footer.jspf" %>
<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">상세페이지</h4>
      </div>
      <div class="modal-body">
      	<form class="form-horizontal" method="post">
      		<input type="hidden" name="num" value="">
		  <div class="form-group">
		    <label for="sub" class="col-sm-2 control-label">Subject</label>
		    <div class="col-sm-10">
		      <input name="sub" class="form-control" id="sub" >
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="id" class="col-sm-2 control-label">ID</label>
		    <div class="col-sm-10">
		      <input name="id" class="form-control" id="id" readonly>
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <textarea name="content" class="form-control" ></textarea>
		    </div>
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10 text-right">
		      <button type="submit" class="btn btn-primary">수정</button>
		      <button type="button" class="btn btn-danger">삭제</button>
		      <button type="reset" class="btn btn-default">취소</button>
	          <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
		    </div>
		  </div>
		</form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
</body>
</html>




