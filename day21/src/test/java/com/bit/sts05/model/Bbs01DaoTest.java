package com.bit.sts05.model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/applicationContext.xml"})
public class Bbs01DaoTest {
	@Autowired
	Bbs01Dao bbs01Dao;

	@Test
	public void testSelectAll() throws SQLException {
		assertNotNull(bbs01Dao.selectAll());
	}
	
	@Transactional
	@Test
	public void testDeleteOne() throws SQLException {
		assertSame(1, bbs01Dao.deleteOne(1));
	}


}
