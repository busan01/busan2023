package com.bit.sts03.model;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Bbs01DaoTest {
	static ApplicationContext ac;
	Bbs01Dao bbs01Dao;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ac=new ClassPathXmlApplicationContext("/applicationContext.xml");
	}

	@Before
	public void setUp() throws Exception {
		bbs01Dao=ac.getBean(Bbs01Dao.class);
	}

	@Test
	public void testSelectAll() throws SQLException {
		assertNotNull(bbs01Dao.selectAll());
		assertNotSame(0,bbs01Dao.selectAll().size());
	}

}
