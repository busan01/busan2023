package com.bit.sts03;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class Ex03Controller {

	@RequestMapping("/step1/ex01")
	public String ex01() {
		return "ex01";
	}
	@RequestMapping(value="/step1/ex02",method=RequestMethod.GET)
	public String ex02get(Model model) {
		model.addAttribute("msg", "get");
		return "ex02";
	}
	@RequestMapping(value="/step1/ex02",method=RequestMethod.POST)
	public String ex02post(Model model) {
		model.addAttribute("msg", "post");
		return "ex02";
	}
	
	@RequestMapping("/step1/ex04")
	public String ex04(Model model, @RequestParam("msg") String msg,int su) {
		model.addAttribute("msg", msg+su);
		return "ex04";
	}
	
	@RequestMapping("/step1/{msg}/{msg2}")
	public String ex03(@PathVariable("msg") String msg,@PathVariable String msg2, Model model) {
		model.addAttribute("msg", msg+":"+msg2);
		return "ex03";
	}
}












