package com.bit.sts03;

import java.sql.Timestamp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bit.sts03.model.Bbs01Vo;

@Controller
@RequestMapping("/step2/")
public class Ex04Controller {

	@RequestMapping("ex02")
	public String ex02(@ModelAttribute("msg") Bbs01Vo bean	) {
		bean.setId("guest");
		bean.setSub("test1");
		bean.setContent("test");
		bean.setNalja(new Timestamp(System.currentTimeMillis()));
		return "ex02";
	}
}






