package com.bit.sts03.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public class Bbs01DaoImpl implements Bbs01Dao {
	
	public Bbs01DaoImpl() {
		System.out.println("dao ����");
	}
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	RowMapper<Bbs01Vo> rowMapper=new RowMapper<Bbs01Vo>() {
		
		@Override
		public Bbs01Vo mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Bbs01Vo(rs.getInt("num"),rs.getString("id"),rs.getString("sub")
					,rs.getString("content"),rs.getTimestamp("nalja"));
		}
	};

	@Override
	public List<Bbs01Vo> selectAll() throws SQLException {
		String sql="select * from bbs01 order by num desc";
		return jdbcTemplate.query(sql,rowMapper);
	}

	@Override
	public Bbs01Vo selectOne(int num) throws SQLException {
		String sql="select * from bbs01 where num=?";
		return jdbcTemplate.queryForObject(sql, rowMapper,num);
	}

	@Override
	public void insertOne(Bbs01Vo bean) throws SQLException {
		String sql="insert into bbs01 (id,sub,content,nalja) values (?,?,?,now())";
		jdbcTemplate.update(sql,bean.getId(),bean.getSub(),bean.getContent());
	}

	@Override
	public int updateOne(Bbs01Vo bean) throws SQLException {
		String sql="update bbs01 set sub=?,content=? where num=?";
		return jdbcTemplate.update(sql,bean.getSub(),bean.getContent(),bean.getNum());
	}

	@Override
	public int deleteOne(int num) throws SQLException {
		String sql="delete from bbs01 where num=?";
		return jdbcTemplate.update(sql,num);
	}

}
