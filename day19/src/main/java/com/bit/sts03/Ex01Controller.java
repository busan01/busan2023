package com.bit.sts03;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@org.springframework.stereotype.Controller
public class Ex01Controller{
	
	@RequestMapping("/ex01")
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return "ex01";
	}

}
