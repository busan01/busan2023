package com.bit.sts03;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;

//POJO

@Controller
@RequestMapping("/")
public class Ex02Controller {

	@RequestMapping("ex02")
	public String ex02(HttpServletRequest req,HttpServletResponse res) {
		return "ex02";
	}
	
	@RequestMapping("ex03")
	public String ex03(HttpServletRequest req,HttpServletResponse res) {
		return "ex03";
	}
	
	@RequestMapping("ex04")
	public String ex04(HttpServletRequest req) {
		req.setAttribute("msg", "abcd");
		return "ex04";
	}
	
	@RequestMapping("ex05")
	public String ex05(Model model) {
		model.addAttribute("msg", "ABCD");
		return "ex05";
	}
	
	@RequestMapping("ex06")
	public View ex06() {
		System.out.println("/ex06");
		return new View() {
			
			@Override
			public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response)
					throws Exception {
				response.getWriter().print("<h1>view page</h1>");
			}
			
			@Override
			public String getContentType() {
				return "text/html; charset=utf-8";
			}
		};
	}
}











