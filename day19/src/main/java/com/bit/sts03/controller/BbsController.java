package com.bit.sts03.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bit.sts03.model.Bbs01Dao;
import com.bit.sts03.model.Bbs01Vo;

@Controller
@RequestMapping("/bbs")
public class BbsController {
	@Autowired
	Bbs01Dao bbs01Dao;
	
	@RequestMapping("/")
	public String list(Model model) throws SQLException {
		model.addAttribute("list", bbs01Dao.selectAll());
		return "bbs/list";
	}
	
	@RequestMapping(value = "/add", method=RequestMethod.GET)
	public void add() {}
	@RequestMapping(value = "/add", method=RequestMethod.POST)
	public String insert(@RequestParam("sub") String sub,@RequestParam String id, String content) {
		try {
			bbs01Dao.insertOne(new Bbs01Vo(0, id, sub, content, null));
		} catch (SQLException e) {
			return "bbs/add";
		}
		return "redirect:./";
	}
	
	@RequestMapping("/detail")
	public void detail(int num,Model model) throws SQLException {
		model.addAttribute("bean", bbs01Dao.selectOne(num));
	}

	@RequestMapping(value = "/edit",method=RequestMethod.GET)
	public void edit(int num,Model model) throws SQLException {
		model.addAttribute("bean", bbs01Dao.selectOne(num));
	}

	@RequestMapping(value = "/edit",method=RequestMethod.POST)
	public String edit(int num,String sub,String content) throws SQLException {
		bbs01Dao.updateOne(new Bbs01Vo(num,null,sub,content,null));
		return "redirect:./";
	}
	
	@RequestMapping(value = "/delete",method=RequestMethod.POST)
	public String delete(int num) throws SQLException {
		bbs01Dao.deleteOne(num);
		return "redirect:./";
	}
}









