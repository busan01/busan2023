<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="layout/head.jspf" %>
</head>
<body>
<%@ include file="layout/menu.jspf" %>
<div class="jumbotron">
  <h1>환영합니다!</h1>
  <p>사이트 방문을 환영합니다</p>
  <p><a class="btn btn-primary btn-lg" href="bbs/" role="button">게시판으로 이동</a></p>
</div>
</body>
</html>