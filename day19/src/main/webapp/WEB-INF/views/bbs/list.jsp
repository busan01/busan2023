<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../layout/head.jspf" %>
<style type="text/css">
	table tr>th:nth-child(1){width: 100px;}
	table tr>th:nth-child(3){width: 100px;}
	table tr>th:nth-child(4){width: 100px;}
</style>
</head>
<body>
<%@ include file="../layout/menu.jspf" %>
<h2>게시판</h2>
<table class="table">
	<thead>
		<tr>
			<th>글번호</th>
			<th>제목</th>
			<th>글쓴이</th>
			<th>날짜</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${list }" var="bean">
			<c:url value="./detail" var="detail">
				<c:param name="num" value="${bean.num }"/>
			</c:url>
		<tr>
			<td><a href="${detail }">${bean.num }</a></td>
			<td><a href="${detail }">${bean.sub }</a></td>
			<td><a href="${detail }">${bean.id }</a></td>
			<td><a href="${detail }">${bean.nalja }</a></td>
		</tr>
		</c:forEach>		
	</tbody>
</table>
<a href="add" class="btn btn-primary btn-block" role="button">입력</a>
</body>
</html>






