<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../layout/head.jspf" %>
<style type="text/css">
	#content{ height: 300px;}
</style>
</head>
<body>
<%@ include file="../layout/menu.jspf" %>
<form method="post"><input type="hidden" name="num" value="${bean.num }">
  <div class="form-group">
    <label for="sub">subject</label>
    <input type="text" name="sub" class="form-control" id="sub" value="${bean.sub }">
  </div>
  <div class="form-group">
    <label for="id">id</label>
    <input type="text" name="id" class="form-control" id="id" value="${bean.id }" disabled="disabled">
  </div>
  <div class="form-group">
    <label for="nalja">nalja</label>
    <input type="datetime" name="nalja" class="form-control" id="id" value="${bean.nalja }" disabled="disabled">
  </div>
  <div class="form-group">
    <textarea class="form-control" name="content" id="content">${bean.content }</textarea>
  </div>
  <button type="submit" class="btn btn-primary btn-block">수정</button>
  <button type="reset" class="btn btn-default btn-block">취소</button>
  <button type="button" class="btn btn-default btn-block" onclick="history.back();">뒤로</button>
</form>
</body>
</html>








