<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../layout/head.jspf" %>
<style type="text/css">
	#content{ height: 300px;}
</style>
<script type="text/javascript">
$(()=>{
	$('form a').last().click(function(e){
		$.post(e.target.href,function(){
			location.href='./';
		});
		return false;
	});
});
</script>
</head>
<body>
<%@ include file="../layout/menu.jspf" %>
<form method="post">
  <div class="form-group">
    <label for="sub">subject</label>
    <input type="text" name="sub" class="form-control" id="sub" value="${bean.sub }" readonly="readonly">
  </div>
  <div class="form-group">
    <label for="id">id</label>
    <input type="text" name="id" class="form-control" id="id" value="${bean.id }" readonly="readonly">
  </div>
  <div class="form-group">
    <label for="nalja">nalja</label>
    <input type="datetime" name="nalja" class="form-control" id="id" value="${bean.nalja }" readonly="readonly">
  </div>
  <div class="form-group">
    <textarea class="form-control" name="content" id="content" readonly="readonly">${bean.content }</textarea>
  </div>
  <a href="edit?num=${bean.num }" class="btn btn-primary btn-block" role="button">수정</a>
  <a href="delete?num=${bean.num }" class="btn btn-danger btn-block" role="button">삭제</a>
  <button type="button" class="btn btn-default btn-block" onclick="history.back();">뒤로</button>
</form>
</body>
</html>








