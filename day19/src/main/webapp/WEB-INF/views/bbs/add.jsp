<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../layout/head.jspf" %>
<style type="text/css">
	#content{ height: 300px;}
</style>
</head>
<body>
<%@ include file="../layout/menu.jspf" %>
<form method="post">
  <div class="form-group">
    <label for="sub">subject</label>
    <input type="text" name="sub" class="form-control" id="sub" placeholder="제목없음">
  </div>
  <div class="form-group">
    <label for="id">id</label>
    <input type="text" name="id" class="form-control" id="id" placeholder="글쓴이">
  </div>
  <div class="form-group">
    <textarea class="form-control" name="content" id="content"></textarea>
  </div>
  <button type="submit" class="btn btn-primary btn-block">입력</button>
  <button type="reset" class="btn btn-default btn-block">취소</button>
  <button type="button" class="btn btn-default btn-block" onclick="history.back();">뒤로</button>
</form>
</body>
</html>








