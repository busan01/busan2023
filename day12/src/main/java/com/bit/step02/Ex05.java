package com.bit.step02;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Ex05 extends HttpServlet {
	@Override
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
		String id=req.getParameter("id");
		res.setContentType("text/html; charset=utf-8");
		PrintWriter out= res.getWriter();
		out.println("<doctype html>");
		out.println("<html><head><meta charset=\"utf-8\"></head><body>");
		out.println("<h1>result page</h1>");
		out.println("<div>id:"+id+"</div>");
		out.println("</body></html>");
	}
}















