package com.bit.json;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;

public class StudentJson extends HttpServlet {
	private String driver="com.mysql.cj.jdbc.Driver";
	private String url="jdbc:mysql://localhost:3306/day12";
	private String sql="select num,name,kor,eng,math from student";
	private String id="user01";
	private String pw="1234";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String json="{\"students\":[";
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try(
				Connection conn=DriverManager.getConnection(url, id, pw);
				Statement stmt=conn.createStatement();
				ResultSet rs=stmt.executeQuery(sql);
				){
			while(rs.next()) {
				if(!rs.isFirst())json+=",";
				json+="{\"num\":"+rs.getInt("num");
				json+=",\"name\":\""+rs.getString("name")+"\"";
				json+=",\"kor\":"+rs.getInt("kor");
				json+=",\"eng\":"+rs.getInt("eng");
				json+=",\"math\":"+rs.getInt("math")+"}";
			}
				
		} catch (SQLException e) {
					e.printStackTrace();
		}
		
		json+="]}";
		resp.setContentType("application/json; charset=utf-8");
		PrintWriter out=resp.getWriter();
		out.print(json);
	}
}







