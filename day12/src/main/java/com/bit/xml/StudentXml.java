package com.bit.xml;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StudentXml extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String info="com.mysql.cj.jdbc.Driver";
		String url="jdbc:mysql://localhost:3306/day12";
		java.util.Properties env=System.getProperties();
		Enumeration<Object> enu = env.elements();
		while(enu.hasMoreElements())
			System.out.println(enu.nextElement());
		java.util.Properties props=new Properties();
		props.setProperty("user", "user01");
		props.setProperty("password", "1234");
		java.sql.Connection conn=null;
		java.sql.Statement stmt=null;
		java.sql.ResultSet rs=null;
		try {
			Class.forName(info);
			conn=java.sql.DriverManager.getConnection(url, props);
			stmt=conn.createStatement();
			rs=stmt.executeQuery("select * from student");
			resp.setCharacterEncoding("utf-8");
			PrintWriter out=resp.getWriter();
			out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			out.println("<students>");
			System.out.println("before");
			while(rs.next()) {
				System.out.println("test");
				int num=rs.getInt(1);
				String name=rs.getString(2);
				int kor=rs.getInt(3);
				int eng=rs.getInt(4);
				int math=rs.getInt(5);
				out.println("<stu>");
				out.println("<num>"+num+"</num>");
				out.println("<name>"+name+"</name>");
				out.println("<kor>"+kor+"</kor>");
				out.println("<eng>"+kor+"</eng>");
				out.println("<math>"+kor+"</math>");			
				out.println("</stu>");
			}
			out.println("</students>");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null)rs.close();
				if(stmt!=null)stmt.close();
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
	}
}
