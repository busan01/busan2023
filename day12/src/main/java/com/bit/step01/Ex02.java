package com.bit.step01;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class Ex02 extends GenericServlet{

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html; charset=utf-8");
		PrintWriter out=res.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<mata charset=\"utf-8\">");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>ex02 페이지</h1>");
		out.println("<a href=\"ex03.html\">link</a>");
		out.println("</body>");
		out.println("</html>");
	}

}
