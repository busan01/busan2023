package com.bit.step01;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

public class Ex03 {

	public static void main(String[] args) throws ServletException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
//		Ex01 me=new Ex01();
		String info="com.bit.step01.Ex02";
		Class cls=Class.forName(info);
		Servlet me=(Servlet) cls.newInstance();
		me.service(null, null);

	}

}
