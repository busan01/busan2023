package com.bit.step01;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

// http://localhost:8080/day12/WEB-INF/classes/com/bit/step01/Ex01.java
// WEB-INF 접근불가
// servlet class - class 파일 경로(/WEB-INF/classes/com/bit/step01/Ex01.java)

public class Ex01 implements Servlet{
	ServletConfig config;
	
	@Override
	public void destroy() {
		System.out.println("was 종료시점때 동작 : 자원반납");
	}

	@Override
	public ServletConfig getServletConfig() {
		System.out.println("servletConfig....");
		return config;
	}

	@Override
	public String getServletInfo() {
		System.out.println("servletInfo...");
		return "info...";
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config=config;
		System.out.println("최초 요청한 한번 call..(servlet setting...)");
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html; charset=utf-8");
		PrintWriter out=res.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<mata charset=\"utf-8\">");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>ex01 페이지</h1>");
		out.println("<a href=\"ex02.html\">link</a>");
		out.println("</body>");
		out.println("</html>");
	}

}

















