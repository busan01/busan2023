import Link from 'next/link'
import { useContext, useEffect, useRef, useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useRouter } from 'next/router';
import { Menu2 } from '@/components/Context';

// export const getStaticPaths = async () => {
//   return {
    // paths: [
    //   { params: {} }
    // ],
//     fallback:'blocking' //true | false | 'blocking'
//   }
// };
export const getStaticProps=async()=>{
  try{
  const resp=await fetch('http://localhost:3000/api/depts');
  const depts=await resp.json();
  if (!depts) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  return {props:{depts},revalidate:10};
  }catch(e){
    return {props:{depts:[]},revalidate:10};
  }
};

export default function Home({depts}) {
  const mn2Click=useContext(Menu2);
  const router = useRouter()

  // Show a loading state when the fallback is rendered
  if(router.isFallback) {
     return <h1>Loading...</h1>
  }
  const [show, setShow] = useState(false);
  const addBtn=useRef();
  const handleSave = () => addBtn.current.click();
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const addDept= async e=>{
    e.preventDefault();
    // const body = JSON.stringify(Object.fromEntries(new FormData(e.target)));
    console.log(e.target.dname.value,e.target.loc.value);
    const body=JSON.stringify({dname:e.target.dname.value,loc:e.target.loc.value});
    const resp=await fetch('http://localhost:3000/api/depts',{
      method:'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body
    });
    console.log(resp.json,mn2Click);
    setShow(false);
    setTimeout(mn2Click,3000);
  };
  useEffect(()=>{},[show]);
  return (
    <>
      <main className={``}>
        <h1>list page</h1>
        <table className='table'>
          <thead>
            <tr><th>deptno</th><th>dname</th><th>loc</th></tr>
          </thead>
        <tbody>
          {depts.map(ele=>
          <tr key={ele.deptno}>  
            <td>{ele.deptno}</td>
            <td>{ele.dname}</td>
            <td>{ele.loc}</td>
          </tr>
          )}
        </tbody>
        </table>
        <Button variant="primary" onClick={handleShow}>
          Launch demo modal
        </Button>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <Form method='POST' onSubmit={addDept}>
            <Form.Group className="mb-3" controlId="dname">
              <Form.Label>dname</Form.Label>
              <Form.Control type="text" placeholder="Enter dname" />
              <Form.Text className="text-muted">
                반드시 명시하시오.
              </Form.Text>
            </Form.Group>
            <Form.Group className="mb-3" controlId="loc">
              <Form.Label>Location</Form.Label>
              <Form.Control type="text" placeholder="Location" />
              <Form.Text className="text-muted">
                반드시 명시하시오.
              </Form.Text>
            </Form.Group>
            <Button ref={addBtn} variant="none" type="submit" className='none'></Button>
          </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>Close</Button>
            <Button variant="primary" onClick={handleSave}>Save</Button>
          </Modal.Footer>
        </Modal>
      </main>
    </>
  )
}
