import { Menu2 } from '@/components/Context'
import '@/styles/globals.css'
import { useRef } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Link from 'next/link';

export default function App({ Component, pageProps }) {
  const mn2=useRef();
  const mn2Click=()=>mn2.current.click();
  return (
  <Menu2.Provider value={mn2Click}>
  <nav>
    <Link href={'/'}>Home</Link>
    <Link ref={mn2} href={'/dept'}>Dept</Link>
    <Link href={'/sign'}>Login</Link>
    <Link href={'/sign/join'}>Join</Link>
  </nav>
  <Component {...pageProps} />
  </Menu2.Provider>
  )
}
