// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import mysql from "mysql2"
const info={host:'localhost', user: 'scott', password:'tiger', database: 'xe'};  
const pool = mysql.createPool(info);
const promisePool = pool.promise();

export default async function handler(req, res) {
  // router.post('/',async(req,res)=>{
  //     const sql=`insert into dept (dname,loc) values ('${req.body.dname}','${req.body.loc}')`;
  //   const result = await promisePool.query(sql);
  //   await res.json(result);
  // });
  if(req.method==='GET'){
    const sql=`select * from dept`;
    const [rows, fields] = await promisePool.query(sql);
    await res.json(rows);
  }else if(req.method==='POST'){
    const sql=`insert into dept (dname,loc) values ('${req.body.dname}','${req.body.loc}')`;
    const result = await promisePool.query(sql);
    await res.json(result);
  }
}
