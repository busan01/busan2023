'use client'
import { useRouter } from "next/navigation";

export default function Ex01() {
    const router = useRouter()
    return (
        <>
            <h2>/step01/ex01 page</h2>
            <button onClick={()=>{router.back();}}>next</button>
        </>
    );
}