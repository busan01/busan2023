import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { useEffect, useState } from 'react'
import Link from 'next/link'

const inter = Inter({ subsets: ['latin'] })

// export const getServerSideProps=async()=>{
//   const res = await fetch('http://localhost:3000/api/list')
//   const depts = await res.json()

//   return {props: {depts}}
// }
const getStaticProps=async()=>{
  // const res = await fetch('http://localhost:3000/api/list')
  const res = await fetch('https://jsonplaceholder.typicode.com/posts')
  const depts = await res.json()

  return {props: {depts},
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    //revalidate: 10, // 초 단위
  }
}

export default function List({depts}) {
//   const [list,setList]=useState([]);
//   useEffect(()=>{
//     fetch('/api/list')
//       .then(e=>e.json())
//       .then(data=>setList(data))
//   },[]);
// useEffect(()=>{console.log(depts)},[]);
  return (
    <>
      <h1>list page</h1>
      <table>
        <thead>
          <tr>
            <th>deptno</th>
            <th>dname</th>
            <th>loc</th>
          </tr>
        </thead>
        <tbody>
        {console.log(depts);}
        {depts.map(ele=>
          <tr key={ele.deptno||ele.id}>
            <td><Link href={'/dept/'+[ele.deptno||ele.id]}>{ele.deptno||ele.id}</Link></td>
            <td>{ele.dname||ele.title}</td>
            <td>{ele.loc||ele.body}</td>
          </tr>  
        )}
        </tbody>
      </table>
    </>
  )
}
