import React, { useEffect, useRef } from 'react'
import { useRouter } from 'next/router';

function Dept() {
    const router=useRouter();
    const in1=useRef();
    const in2=useRef();
    const in3=useRef();
    useEffect(()=>{
        console.log(router);
        fetch('/api/dept?deptno='+router.query.deptno)
        .then(e=>e.json())
        .then(data=>{
            in1.current.value=data.deptno;
            in2.current.value=data.dname;
            in3.current.value=data.loc;
        });

    },[]);
  return (
    <>
        <div>detail page</div>
        <form>
            <div><input ref={in1}/></div>
            <div><input ref={in2}/></div>
            <div><input ref={in3}/></div>
            <div>
                <button>수정</button>
                <button type='button' onClick={router.back}>뒤로</button>
            </div>
        </form>
    </>
  )
}

export default Dept