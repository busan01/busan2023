import { Html, Head, Main, NextScript } from 'next/document'
import Link from 'next/link'

export default function Document() {
  return (
    <Html lang="en">
      <Head />
      <body>
        <nav style={{display:'grid',gridAutoFlow:'column'}}>
          <Link href={'/'}>home</Link>
          <Link href={'/dept/'}>dept</Link>
        </nav>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
