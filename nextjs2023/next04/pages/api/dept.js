// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import dbClient from "../../modules/dbClient";

export default function handler(req, res) {
  const no=req.query.deptno;
  dbClient.query("SELECT * FROM dept where deptno="+no, (error, result) => {
    if(error) {
        res.sendStatus(500);
    } else {
        res.status(200).json(result.rows[0]);
    }
  });
  // res.status(200).end();
}
