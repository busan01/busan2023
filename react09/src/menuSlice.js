import { createSlice } from '@reduxjs/toolkit'

export const menuSlice = createSlice({
  name: 'mn',
  initialState: {
    value: [
                {name:'홈',chk:true,hrf:'/',childs:[]}
                ,{name:'메뉴',chk:true,hrf:'/add',childs:[]}
            ],
  },
  reducers: {
    incrementDepth1: (state,action) => {
        console.log(action.payload,state.value);
        const {name,hrf}=action.payload;
        state.value = [...state.value,{name,hrf,chk:false,childs:[]}];
    },
    incrementDepth2: (state,action) => {
        console.log('depth2');
        console.log(action.payload,state.value);
        const {depth1,name,hrf}=action.payload;
        state.value = [...(state.value.map((ele,idx)=>
            idx===Number(depth1)?{...ele,childs:[...ele.childs,{name,hrf,chk:false,childs:[]}]}:ele
          ))];
    },
    incrementDepth3: (state,action) => {
        console.log('depth3');
        const {depth1,depth2,name,hrf}=action.payload;
        state.value = [...(state.value.map((ele,idx)=>
                idx===depth1?{...ele,childs:[...(ele.childs.map((ele2,idx2)=>
                  idx2===depth2?{...ele2,childs:[...ele2.childs,{name,hrf,chk:false,childs:[]}]}:ele2
                  ))]}:ele))]
    },
    applyMenu: (state,action)=>{
      state.value=action.payload;
      console.log(JSON.stringify(action.payload,0,3));
    },
  },
})

// Action creators are generated for each case reducer function
export const { incrementDepth1,incrementDepth2,incrementDepth3,applyMenu } = menuSlice.actions

export default menuSlice.reducer