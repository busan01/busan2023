import { useEffect, useRef, useState } from 'react';
import './Add.css';
import Frame from './components/Frame';
import { useDispatch } from 'react-redux';
import { applyMenu } from './menuSlice';
function Add() {
  const dispatch=useDispatch();
  const [depth1,setDepth1]=useState(0);
  const [depth2,setDepth2]=useState(-1);
  const inp1=useRef();
  const inp2=useRef();
  const [mnu,setMnu]=useState([
    {name:'홈',chk:true,hrf:'/',childs:[]}
    ,{name:'메뉴',chk:true,hrf:'/add',childs:[]}
  ]);
  useEffect(()=>{
    setMnu([...mnu,{name:'샘플1',chk:false,hrf:'/one',childs:[
          {name:'샘플1-1',chk:false,hrf:'/one1',childs:[]}
        ]}])
  },[]);
  useEffect(()=>console.log(mnu),[mnu]);
  const add=e=>{
    e.preventDefault();
    const name=(e.target.name.value);
    const hrf=`/${e.target.hrf.value}`;
    if(name.length===0 || hrf.length===1)return;
    if(depth1===0)
      setMnu([...mnu,{name,hrf,chk:false,childs:[]}]);
    else if(depth2===-1)
      setMnu(mnu.map((ele,idx)=>
        idx===Number(depth1)?{...ele,childs:[...ele.childs,{name,hrf,chk:false,childs:[]}]}:ele
      ));
    else
      setMnu(mnu.map((ele,idx)=>
        idx===Number(depth1)?{...ele,childs:[...(ele.childs.map((ele2,idx2)=>{
          return idx2===Number(depth2)?{...ele2,childs:[...ele2.childs,{name,hrf,chk:false,childs:[]}]}:ele2
        }))]}:ele
      ));
    
    setDepth1(0)
    setDepth2(-1)
    inp1.current.value='';
    inp2.current.value='';
    // e.target.hrf='';
  }
  const toggleChk=(idx1,idx2,idx3)=>{
    console.log(typeof idx1,idx1,typeof idx2,idx2,typeof idx3,idx3);
    setMnu(mnu.map((el1,i1)=>{
      if(i1===idx1){
        if(idx2===-1) return {...el1,chk:!el1.chk}
        return {...el1,childs:el1.childs.map((el2,i2)=>{
          if(i2===idx2){
            if(idx3===-1)return {...el2,chk:!el2.chk};
            return {...el2,childs:el2.childs.map((el3,i3)=>{
              if(i3===idx3) return {...el3,chk:!el3.chk}
              return el3;
            })};
          }
          return el2;
        })};
      }else{
        return el1;
      }
      
    }));
  };
  const changeDe1=su=>{
    setDepth1(Number(su));
  }
  const changeDe2=su=>{
    setDepth2(Number(su));
  }
  return (
    <Frame>
      <h1>Menu Edit</h1>     
      <ul>
        {mnu.map((ele,idx)=>
          <li key={`one_${idx}`}>
            {ele.name}({ele.hrf})<input type='checkbox' onChange={e=>toggleChk(idx,-1,-1)} checked={ele.chk}/>
            <ol>
              {ele.childs.map((ele2,idx2)=>
              <li key={`one_${idx2}`}>
                {ele2.name}({ele2.hrf})<input type='checkbox' onChange={e=>toggleChk(idx,idx2,-1)} checked={ele2.chk}/>
                <ol>
                  {ele2.childs.map((ele3,idx3)=>
                  <li key={`one_${idx3}`}>
                    {ele3.name}({ele3.hrf})<input type='checkbox' onChange={e=>toggleChk(idx,idx2,idx3)} checked={ele3.chk}/>
                  </li>
                  )}
                </ol>
              </li>
              )}
            </ol>
          </li>
        )}
      </ul> 
      <form onSubmit={add}>
        <label>
          <select name="depth1" onChange={e=>{changeDe1(e.target.value)}} value={depth1}>
            <option value={0}>신규</option>
            {mnu.map((ele,idx)=>idx>1&&
              <option value={idx} key={`d1_${idx}`}>{ele.name}</option>
            )}
          </select>
          <select name="depth2"  onChange={e=>{changeDe2(e.target.value)}} value={depth2}>
              <option value={-1}>-</option>
              {mnu[depth1].childs.map((ele,idx)=>
              <option key={`d2_${idx}`} value={idx}>{ele.name}</option>)}
          </select>
          </label><br/>
          <label>name<div><input ref={inp1} name='name'/></div></label><br/>
          <label>href<div className='u'>/<input ref={inp2} name='hrf'/></div></label><br/>
        <button type='submit'>추가</button>
        <button type='button' onClick={e=>{dispatch(applyMenu(mnu))}}>메뉴적용</button>
      </form>
    </Frame>
  );
}

export default Add;
