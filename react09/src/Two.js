import { useEffect} from 'react';
import './Add.css';
import Frame from './components/Frame';
import { useParams } from 'react-router';
function Two() {
  const {title}=useParams();
  useEffect(()=>{
    console.log(title);
  },[title]);
  return (
    <Frame>
      <h1>{title.toUpperCase()} page</h1>     
    </Frame>
  );
}
export default Two;
