import { useEffect } from "react";
import Menus from "./Menus";

function Frame({children}) {
  return (
    <div className="container">
      <Menus/>
      <div>{children}</div>
    </div>
  );
}

export default Frame;
