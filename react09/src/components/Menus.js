import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

function Menus() {
  const menus = useSelector((state) =>state.menuReducer.value)
  return (
    <nav>
        <ul>
        {menus&&menus.map((ele,idx)=>
          <li key={`one_${idx}`}>
            <Link to={ele.hrf} style={(ele.chk||ele.childs.some(ele=>ele.chk||ele.childs.some(el=>el.chk)))?null:{textDecoration:'line-through',color: '#ccc'}}>{ele.name}<span>[{(ele.hrf)+","+(ele.chk)}]</span></Link>
            <ol>
              {ele.childs.map((ele2,idx2)=>
              <li key={`one_${idx2}`}>
                <Link to={ele2.hrf} style={(ele2.chk||ele2.childs.some(ele=>ele.chk))?null:{textDecoration:'line-through',color: '#ccc'}}>{ele2.name}<span>[{(ele2.hrf)+","+(ele2.chk)}]</span></Link>
                <ol>
                  {ele2.childs.map((ele3,idx3)=>
                  <li key={`one_${idx3}`}>
                    <Link to={ele3.hrf} style={(ele3.chk||ele3.childs.some(ele=>ele.chk))?null:{textDecoration:'line-through',color: '#ccc'}}>{ele3.name}<span>[{(ele3.hrf)+","+(ele3.chk)}]</span></Link>
                  </li>
                  )}
                </ol>
              </li>
              )}
            </ol>
          </li>
        )}
      </ul>
    </nav>
  )
}

export default Menus