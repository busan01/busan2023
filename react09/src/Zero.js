import { useEffect, useState } from 'react';
import './Add.css';
import Frame from './components/Frame';
import { useDispatch, useSelector } from 'react-redux';
import { incrementDepth1, incrementDepth2, incrementDepth3 } from './menuSlice';
import { useParams } from 'react-router';
function Add() {
  const [depth1,setDepth1]=useState(0);
  const [depth2,setDepth2]=useState(-1);
  const mnu = useSelector((state) =>state.menuReducer.value);
  const dispatch = useDispatch();
  // const [mnu,setMnu]=useState([
  //   {name:'홈',chk:true,hrf:'/',childs:[]}
  //   ,{name:'menu',chk:true,hrf:'/add',childs:[]}
  //   ,{name:'one',chk:true,hrf:'/one',childs:[
  //     {name:'one1',chk:true,hrf:'/one1',childs:[]}
  //   ]}
  // ]);
  const {title}=useParams();
  useEffect(()=>{
    console.log(title);
  },[title]);
  const add=e=>{
    e.preventDefault();
    const name=(e.target.name.value);
    const hrf=`/${(e.target.hrf.value)}`;
    if(depth1===0)
      dispatch(incrementDepth1({name,hrf}));
    //   setMnu([...mnu,{name,hrf,chk:false,childs:[]}]);
    else if(depth2===-1)
      dispatch(incrementDepth2({depth1,name,hrf}));
    //   setMnu(mnu.map((ele,idx)=>
    //     idx===Number(depth1)?{...ele,childs:[...ele.childs,{name,hrf,chk:false,childs:[]}]}:ele
    //   ));
    else
      dispatch(incrementDepth3({depth1,depth2,name,hrf}));
    //   setMnu(mnu.map((ele,idx)=>
    //     idx===Number(depth1)?{...ele,childs:[...(ele.childs.map((ele2,idx2)=>{
    //       return idx2===Number(depth2)?{...ele2,childs:[...ele2.childs,{name,hrf,chk:false,childs:[]}]}:ele2
    //     }))]}:ele
    //   ));
    // }
    setDepth1(0)
    setDepth2(-1)
  }
  const toggleChk=idx=>{
    //setMnu(mnu.map((ele,i)=>idx===i?{...ele,chk:!(ele.chk)}:ele));
  };
  const changeDe1=su=>{
    setDepth1(Number(su));
  }
  const changeDe2=su=>{
    setDepth2(Number(su));
  }
  return (
    <Frame>
      <h1>{title.toUpperCase()} page</h1>     
      <ul>
        {mnu.map((ele,idx)=>
          <li key={`one_${idx}`}>
            {ele.name}({ele.hrf})<input type='checkbox' onChange={e=>toggleChk(idx)} checked={ele.chk}/>
            <ol>
              {ele.childs.map((ele2,idx2)=>
              <li key={`one_${idx2}`}>
                {ele2.name}({ele2.hrf})<input type='checkbox' onChange={e=>toggleChk(idx)} checked={ele2.chk}/>
                <ol>
                  {ele2.childs.map((ele3,idx3)=>
                  <li key={`one_${idx3}`}>
                    {ele3.name}({ele3.hrf})<input type='checkbox' onChange={e=>toggleChk(idx)} checked={ele3.chk}/>
                  </li>
                  )}
                </ol>
              </li>
              )}
            </ol>
          </li>
        )}
      </ul> 
      <form onSubmit={add}>
        <label>
          <select name="depth1" onChange={e=>{changeDe1(e.target.value)}}>
            <option value={0} selected={depth1<2}>신규</option>
            {mnu.map((ele,idx)=>idx>1&&
              <option value={idx} key={`d1_${idx}`} selected={idx==depth2}>{ele.name}</option>
            )}
          </select>
          <select name="depth2"  onChange={e=>{changeDe2(e.target.value)}}>
              <option value={-1}>-</option>
              {mnu[depth1].childs.map((ele,idx)=>
              <option key={`d2_${idx}`} value={idx} selected={idx==depth2}>{ele.name}</option>)}
          </select>
          </label><br/>
          <label>name<input name='name'/></label><br/>
          <label>href<input name='hrf'/></label><br/>
        <button>입력</button>
      </form>
    </Frame>
  );
}

export default Add;
