package com.bit.boot04.service;

import static org.junit.jupiter.api.Assertions.*;

import javax.transaction.Transactional;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bit.boot04.domain.entity.RequestBbs03Vo;
import com.bit.boot04.domain.entity.ResponseBbs03Vo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
//@TestMethodOrder(OrderAnnotation.class)
class Bbs03ServiceTest {
	@Autowired
	Bbs03Service bbs03Service;

	@Test
	@Order(1)
	void testInsertOne() throws Exception{
		bbs03Service.insertOne(new RequestBbs03Vo(0, "tester", "테스트1", "테스트"));
		bbs03Service.insertOne(new RequestBbs03Vo(0, "tester", "테스트2", "테스트"));
		bbs03Service.insertOne(new RequestBbs03Vo(0, "tester", "테스트3", "테스트"));
	}
	
	@Test
	@Order(2)
	void testSelectAll() throws Exception{
		for(ResponseBbs03Vo bean: bbs03Service.selectAll()) {
			log.debug(bean.toString());
		}
	}
	
	@Test
	@Order(3)
	void testUpdateOne() throws Exception{
		ResponseBbs03Vo result = bbs03Service.updateOne(new RequestBbs03Vo(4, "test", "테스트4", "테스트"));
		log.debug(result.toString());
	}

	@Test
	@Order(4)
	void testSelectOne() throws Exception{
		ResponseBbs03Vo result=bbs03Service.selectOne(6);
		assertNotNull(result);
	}
	
	@Transactional
	@Test
	@Order(5)
	void testDeleteOne() throws Exception{
		bbs03Service.deleteOne(4);
	}
}









