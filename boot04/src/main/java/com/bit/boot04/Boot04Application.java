package com.bit.boot04;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.bit.boot04"})
public class Boot04Application {

	public static void main(String[] args) {
		SpringApplication.run(Boot04Application.class, args);
	}

}
