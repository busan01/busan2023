package com.bit.boot04.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bit.boot04.domain.entity.RequestBbs03Vo;
import com.bit.boot04.service.Bbs03Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/bbs/v1")
@RequiredArgsConstructor
public class Bbs03Controller {
	final Bbs03Service bbs03Service;
	
	@GetMapping("/")
	public ResponseEntity<?> list(){
		try {
			return ResponseEntity.ok(bbs03Service.selectAll());
		}catch (Exception e) {
			return ResponseEntity.internalServerError().build();
		}
	}
	
	@GetMapping("/{num}")
	public ResponseEntity<?> pollList(@PathVariable int num){
		try {
//			return ResponseEntity.ok(bbs03Service.selectOne(num));
			return ResponseEntity.ok(bbs03Service.selectOne("한글"));
		}catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	@PostMapping("/")
	public ResponseEntity<?> pushList(@ModelAttribute RequestBbs03Vo bean){
		try {
			bean.setNum(bbs03Service.insertOne(bean));
			return ResponseEntity.status(HttpStatus.CREATED).body(bean);
		}catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	@PutMapping("/{num}")
	public ResponseEntity<?> setList(@PathVariable int num,@RequestBody RequestBbs03Vo bean){
		try {
			return ResponseEntity.ok(bbs03Service.updateOne(bean));
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	@DeleteMapping("/{num}")
	public ResponseEntity<?> removeList(@PathVariable int num){
		try {
			return ResponseEntity.ok(bbs03Service.deleteOne(num));
		}catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
}










