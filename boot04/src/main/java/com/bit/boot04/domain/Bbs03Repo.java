package com.bit.boot04.domain;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bit.boot04.domain.entity.Bbs03;

public interface Bbs03Repo extends JpaRepository<Bbs03, Integer>{

	Iterable<Bbs03> findAllByOrderByNumDesc();
	// extends CrudRepository<Bbs03, Integer>{
//	Optional<Bbs03> findByNum(int num);
	Optional<Bbs03> findBySubjectContaining(String subject);
}
