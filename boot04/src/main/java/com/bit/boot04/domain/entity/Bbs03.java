package com.bit.boot04.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "bbs03")
public class Bbs03 {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int num;
	
	@Column(name = "id" ,length = 20, nullable = false)
	String id;
	
	@Column(name="sub",columnDefinition = "varchar(10) not null default '제목없음'")
	String subject;
	
	@Column(name="content", columnDefinition = "text")
	String content;

	public ResponseBbs03Vo toEntity() {
		return ResponseBbs03Vo.builder()
							.num(num)
							.id(id)
							.subject(subject)
							.content(content)
							.build();
	}
}






