package com.bit.boot04.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestBbs03Vo {
	private int num;
	private String id,subject,content;
}
