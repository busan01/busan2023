package com.bit.boot04.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.bit.boot04.domain.Bbs03Repo;
import com.bit.boot04.domain.entity.Bbs03;
import com.bit.boot04.domain.entity.RequestBbs03Vo;
import com.bit.boot04.domain.entity.ResponseBbs03Vo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class Bbs03Service {
	final Bbs03Repo bbs03Repo;
	
	public int insertOne(RequestBbs03Vo bean) {
		
		Bbs03 result = bbs03Repo.save(Bbs03.builder()
							.id(bean.getId())
							.subject(bean.getSubject())
							.content(bean.getContent())
							.build()
						);
		return result.toEntity().getNum();
	}
	
	public List<ResponseBbs03Vo> selectAll(){
		List<ResponseBbs03Vo> list=new ArrayList<>();
//		for(Bbs03 bean : bbs03Repo.findAll()) {
//			list.add(bean.toEntity());
//		}
//		bbs03Repo.findAll().forEach(bean->list.add(bean.toEntity()));
		///////////////
//		Sort sort=Sort.by(Sort.Direction.DESC, "num");
//		bbs03Repo.findAll(sort).forEach(bean->list.add(bean.toEntity()));
		///////////////
		bbs03Repo.findAllByOrderByNumDesc().forEach(bean->list.add(bean.toEntity()));
		return list;
	}
	public ResponseBbs03Vo updateOne(RequestBbs03Vo bean) {
		return bbs03Repo.save(Bbs03.builder()
				.id(bean.getId())
				.num(bean.getNum())
				.subject(bean.getSubject())
				.content(bean.getContent())
				.build()).toEntity();
	}
	
	public ResponseBbs03Vo selectOne(int num) {
		return bbs03Repo.findById(num).orElseThrow().toEntity();
//		Optional<Bbs03> result = bbs03Repo.findById(num);
//		if(result.isPresent()) {
//			Bbs03 bean=result.get();
//			ResponseBbs03Vo vo = bean.toEntity();
//			return vo;
//		}
//		return null;
	}
	public ResponseBbs03Vo selectOne(String sub) {
		return bbs03Repo.findBySubjectContaining(sub).orElseThrow().toEntity();
	}
	public ResponseBbs03Vo deleteOne(int num) {
//		bbs03Repo.deleteById(num);
		Optional<Bbs03> check = bbs03Repo.findById(num);
		if(check.isPresent())
			bbs03Repo.delete(check.get());
		return check.get().toEntity();
	}
}














