package com.bit.sts02.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.bit.sts02.model.Bbs01Dao;

public class ListController implements Controller {
	Bbs01Dao bbs01Dao;
	
	public void setBbs01Dao(Bbs01Dao bbs01Dao) {
		this.bbs01Dao = bbs01Dao;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav=new ModelAndView("list");
		mav.addObject("list", bbs01Dao.selectAll());
		return mav;
	}

}
