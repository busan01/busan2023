package com.bit.sts02.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractFormController;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.bit.sts02.model.Bbs01Dao;
import com.bit.sts02.model.Bbs01Dto;

public class AddController extends AbstractFormController{
	Bbs01Dao bbs01Dao;
	public void setBbs01Dao(Bbs01Dao bbs01Dao) {
		this.bbs01Dao = bbs01Dao;
	}

//	@Override
//	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
//		
//		bbs01Dao.insertOne(new Bbs01Dto(
//				0,request.getParameter("id")
//				,request.getParameter("sub")
//				,request.getParameter("content"),null
//				));
//		return new ModelAndView("redirect:./");
//	}
	Map<String,String> errs=new HashMap<>();
	@Override
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		errs.clear();
		Bbs01Dto bean=(Bbs01Dto) command;
		if(bean.getSub().isEmpty()) errs.put("sub", "제목없음");
		if(bean.getId().isEmpty()) errs.put("id", "사용자반드시 명시");
//		if(bean.getSub().isEmpty()) errors.addError(new ObjectError("sub", "제목없음"));
//		if(bean.getId().isEmpty()) errors.addError(new ObjectError("id","빈칸입력"));
	}
	
	@Override
	protected ModelAndView showForm(HttpServletRequest request, HttpServletResponse response, BindException errors)
			throws Exception {
		return new ModelAndView("add");
	}

	@Override
	protected ModelAndView processFormSubmission(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors) throws Exception {
		
		if(!errs.isEmpty())return new ModelAndView("add","errs",errs);
		Bbs01Dto bean=(Bbs01Dto) command;		
		bbs01Dao.insertOne(bean);
		return new ModelAndView("redirect:./");
	}

}
