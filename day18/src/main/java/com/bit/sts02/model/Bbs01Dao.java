package com.bit.sts02.model;

import java.sql.SQLException;
import java.util.List;

public interface Bbs01Dao {
	List<Bbs01Dto> selectAll() throws SQLException;
	Bbs01Dto selectOne(int num) throws SQLException;
	int insertOne(Bbs01Dto bean) throws SQLException;
	int updateOne(Bbs01Dto bean) throws SQLException;
	int deleteOne(int num) throws SQLException;
}
