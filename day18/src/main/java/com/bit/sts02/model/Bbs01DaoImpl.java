package com.bit.sts02.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.DefaultTransactionStatus;

public class Bbs01DaoImpl implements Bbs01Dao {
	private JdbcTemplate jdbcTemplate;
	private PlatformTransactionManager transactionManager;
	private RowMapper<Bbs01Dto> rowMapper= new RowMapper<Bbs01Dto>() {

		@Override
		public Bbs01Dto mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Bbs01Dto(
					rs.getInt("num"),rs.getString("id"),rs.getString("sub")
					,rs.getString("content"),rs.getTimestamp("nalja")
					);
		}};
	public void setTransactionManager(PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<Bbs01Dto> selectAll() throws SQLException {
		String sql="select * from bbs01 order by num desc";
		return jdbcTemplate.query(sql,rowMapper);
	}

	@Override
	public Bbs01Dto selectOne(int num) throws SQLException {
		String sql="select * from bbs01 where num=?";
		return jdbcTemplate.queryForObject(sql,rowMapper,num);
	}

	@Override
	public int insertOne(Bbs01Dto bean) throws SQLException {
		String sql="insert into bbs01 (id,sub,content,nalja) values (?,?,?,now())";
		String sql3="insert into bbs01 (num,id,sub,content,nalja) values (?,?,?,?,now())";
		String sql2="select max(num) as cnt from bbs01";
		
		TransactionDefinition definition=new DefaultTransactionDefinition();
		TransactionStatus status=transactionManager.getTransaction(definition);
		try {
		PreparedStatementCreator psc=new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				System.out.println(conn);
				PreparedStatement pstmt= conn.prepareStatement(sql);
				pstmt.setString(1, bean.getId());
				pstmt.setString(2, bean.getSub());
				pstmt.setString(3, bean.getContent());
				return pstmt;
			}
		};
		jdbcTemplate.update(psc);
		psc=new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
				System.out.println(conn);
				PreparedStatement pstmt= conn.prepareStatement(sql2);
				return pstmt;
			}
		};
		Object result=jdbcTemplate.query(psc,new RowMapper() {

						@Override
						public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
							return rs.getInt(1);
						}
					}).get(0);
		transactionManager.commit(status);
		return ((int)result);
		}catch (Exception e) {
			transactionManager.rollback(status);
			return 0;
		}
		
		
//		return jdbcTemplate.update(sql, bean.getId(),bean.getSub(),bean.getContent());
	}

	@Override
	public int updateOne(Bbs01Dto bean) throws SQLException {
		String sql="update bbs01 set sub=?,content=? where num=?";
		return jdbcTemplate.update(sql,bean.getSub(),bean.getContent(),bean.getNum());
	}

	@Override
	public int deleteOne(int num) throws SQLException {
		String sql="delete from bbs01 where num=?";
		return jdbcTemplate.update(sql,num);
	}

}
