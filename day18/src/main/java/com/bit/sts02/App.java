package com.bit.sts02;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bit.sts02.module.Ex01ServiceImpl;

public class App {

	public static void main(String[] args) throws SQLException {
		ApplicationContext ac;
		ac=new ClassPathXmlApplicationContext("/applicationContext.xml");
		
		Ex01ServiceImpl service=(Ex01ServiceImpl) ac.getBean("proxyService");
		service.func01();
		service.func02(1111,3.14,"abcd");
		service.func03();
//		service.func04();
		DataSource dataSource=ac.getBean(DataSource.class);
		dataSource.getConnection().close();
	}

}
