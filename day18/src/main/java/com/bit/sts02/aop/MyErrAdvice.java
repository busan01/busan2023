package com.bit.sts02.aop;

import java.lang.reflect.Method;

import org.springframework.aop.ThrowsAdvice;

public class MyErrAdvice implements ThrowsAdvice {

	public void afterThrowing(Method method, Object[] args, Object target, Exception ex) {
		System.out.println("┗exception 발생");
		System.out.println(ex.toString());
	}
}
