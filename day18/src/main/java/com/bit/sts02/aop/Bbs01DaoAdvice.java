package com.bit.sts02.aop;

import java.sql.SQLException;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class Bbs01DaoAdvice implements MethodInterceptor {

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
//		System.out.println("dao 실행전");
		String name=invocation.getMethod().getName();
		Object val=null;
		try{
			val=invocation.proceed();
//			System.out.println("dao 실행후");
			System.out.println(name+" method return =>"+val);
		}catch(SQLException e) {
//			System.out.println("dao exception 발생");			
		}
		
		return val;
	}

}
