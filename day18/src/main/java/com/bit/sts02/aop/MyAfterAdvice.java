package com.bit.sts02.aop;

import java.lang.reflect.Method;

import org.springframework.aop.AfterAdvice;
import org.springframework.aop.AfterReturningAdvice;

public class MyAfterAdvice implements AfterReturningAdvice{

	@Override
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		System.out.println("┃return value :"+returnValue);
		System.out.println("┗메서드 핵심사항 실행이후┛");
	}

}
