package com.bit.sts02.aop;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;

import org.springframework.aop.MethodBeforeAdvice;

public class MyBeforeAdvice implements MethodBeforeAdvice{

	@Override
	public void before(Method method, Object[] args, Object target) throws Throwable {
		System.out.println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓");
		System.out.println("┃method name :"+method.getName());
		System.out.println("┃return type :"+method.getReturnType());
		for(Parameter param:method.getParameters()) {
			System.out.print("┃"+param.getParameterizedType());
		}
		System.out.println();
		System.out.println(Arrays.toString(args));
		System.out.println(target);
	}

}
