package com.bit.sts02.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class MyAroundAdvice implements MethodInterceptor {

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		System.out.println("┏━━━━━━━around before━━━━━━━━━━━━━━┓");
		Object returnValue=null;
		try {
			returnValue=invocation.proceed();
			System.out.println("┃ success - AfterReturningAdvice");
		}catch(Exception e){
			System.out.println("┃ Err - ThrowsAdvice");
		}
		System.out.println("┗━━━━━━━around after━━━━━━━━━━━━━━━┛");
		return returnValue;
	}

}
