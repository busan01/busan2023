package com.bit.sts02.module;

public class Ex01ServiceImpl {

	public void func01() {
		System.out.println("┃첫번째 기능");
	}
	
	public void func02(int su,double su2,String msg) {
		System.out.println("┃두번째 기능 param:"+su);
	}
	
	public boolean func03() {
		System.out.println("┃세번째 기능");
		return true;
	}
	
	public boolean func04() {
		System.out.println("┃네번째 기능");
		if(true) throw new IllegalStateException();
		return true;
	}
}












