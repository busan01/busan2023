<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="layout/head.jspf" %>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $(".slider").bxSlider({
    	slideWidth:600
    });
  });
</script>

</head>
<body>
<%@ include file="layout/menu.jspf" %>
<ul class="slider">
	<li><img alt="" src="https://dummyimage.com/600x400/000/fff&text=Image01"></li>
	<li><img alt="" src="https://dummyimage.com/600x400/000/fff&text=Image02"></li>
	<li><img alt="" src="https://dummyimage.com/600x400/000/fff&text=Image03"></li>
	<li><img alt="" src="https://dummyimage.com/600x400/000/fff&text=Image04"></li>
	<li><img alt="" src="https://dummyimage.com/600x400/000/fff&text=Image05"></li>
	<li><img alt="" src="https://dummyimage.com/600x400/000/fff&text=Image06"></li>
</ul>
<%@ include file="layout/footer.jspf" %>
</body>
</html>













