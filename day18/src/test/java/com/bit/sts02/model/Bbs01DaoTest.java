package com.bit.sts02.model;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Bbs01DaoTest {
	static ApplicationContext ac;
	static Bbs01Dto target;
	Bbs01Dao bbs01Dao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ac=new ClassPathXmlApplicationContext("/applicationContext.xml");
		target=new Bbs01Dto(1,"guest","sub01","",null);
	}

	@Before
	public void setUp() throws Exception {
		bbs01Dao=ac.getBean(Bbs01Dao.class);
	}

	@Test
	public void test4SelectAll() throws SQLException {
		List<Bbs01Dto> list = bbs01Dao.selectAll();
		assertNotNull(list);
		assertTrue(list.size()>0);
	}
	
	@Test //dummy [num=1, id=guest, sub=sub01, content=]
	public void test2SelectOne() throws SQLException {
		Bbs01Dto bean=bbs01Dao.selectOne(target.getNum());
		assertNotNull(bean);
		assertEquals(target, bean);
	}
	
	@Test
	public void test3InsertOne() throws SQLException {
		Bbs01Dto bean = new Bbs01Dto(0,"tester","test","test msg",null);
		int cnt=bbs01Dao.selectAll().size();
		assertSame(1, bbs01Dao.insertOne(bean));
		assertSame(cnt+1,bbs01Dao.selectAll().size());
	}
	
	@Test
	public void test1UpdateOne() throws SQLException {
		target.setContent("edit");
		int cnt=bbs01Dao.updateOne(target);
		assertSame(1, cnt);
		assertEquals(target, bbs01Dao.selectOne(target.getNum()));
	}
	
	@Test
	public void test5deleteOne() throws SQLException {
		assertNotSame(1, bbs01Dao.deleteOne(40));
	}

}
















