import { PrismaClient } from "@prisma/client";
const db = new PrismaClient();

async function seed() {
  await Promise.all(
    getDept().map((dept) => {
      return db.dept.create({ data: dept });
    })
  );
}

seed();

function getDept() {
  // shout-out to https://icanhazdadjoke.com/

  return [
    {
      dname: "테스트1",
      loc: `test`,
    },
    {
      dname: "테스트2",
      loc: `test`,
    },
    {
      dname: "테스트3",
      loc: `test`,
    },
    {
      dname: "테스트4",
      loc: `test`,
    },
  ];
}
