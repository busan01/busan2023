import { createCookieSessionStorage,redirect } from "@remix-run/node";

const storage = createCookieSessionStorage({
    cookie: {
      name: "RJ_session",
      secure: process.env.NODE_ENV === "production",
      secrets: ['sessionSecret'],
      sameSite: "lax",
      path: "/",
      maxAge: 60 * 60 * 24 * 30,
      httpOnly: true,
    },
  });
function getUserSession(request) {
    return storage.getSession(request.headers.get("Cookie"));
  }
export async function requireUserId(
    request,
    redirectTo= new URL(request.url).pathname
  ) {
    const session = await getUserSession(request);
    const deptNo = session.get("deptNo");
    if (!deptNo) {
      const searchParams = new URLSearchParams([
        ["redirectTo", redirectTo],
      ]);
      throw redirect(`/login?${searchParams}`);
    }
    return deptNo;
  }