import { ActionArgs,redirect } from "@remix-run/node";
import { useActionData, useLoaderData } from "@remix-run/react";

import { db } from "~/utils/db.server";

export const action = async ({request,context,params}) => {
    console.log(request);
    const form = await request.formData();
    console.log(form);
  const loc = form.get("loc");
  const dname = form.get("dname");
  if (
    typeof loc !== "string" ||
    typeof dname !== "string"
  ) {
    throw new Error("Form not submitted correctly.");
  }

  const fields = { loc, dname };

  const dept = await db.dept.create({ data: fields });
  return redirect(`/dept`);
};

export default function () {
    return (
      <div>
        <h2>insert dept</h2>
        <form method="post">
          <div>
            <label>
              Dname: <input type="text" name="dname" />
            </label>
          </div>
          <div>
            <label>
              Loc: <textarea name="loc" />
            </label>
          </div>
          <div>
            <button type="submit" className="button">
              Add
            </button>
          </div>
        </form>
      </div>
    );
  }
  