import { json } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";

import { db } from "~/utils/db.server";

export const loader = async () => {
  return json({
    depts: await db.dept.findMany({
        orderBy: { deptNo: "desc" },
        select: { loc: true, dname: true,deptNo:true },
        take: 5,
      }),
  });
};

export default function() {
  const data = useLoaderData();
  return (
    <div>
        <ul>{data.depts.map((dept) => (
            <li key={dept.deptNo}><Link to={`./${dept.deptNo}`}>{dept.dname}</Link></li>
        ))}</ul>
        <Link to={'./new'} className="button">Add</Link>
    </div>
  );
}
