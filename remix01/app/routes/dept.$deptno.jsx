import { LoaderArgs, redirect,json } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";

import { db } from "~/utils/db.server";
import { requireUserId } from "~/utils/session.server";

export const loader = async ({ params }) => {
    console.log(params);
    const dept = await db.dept.findUnique({
        where: { deptNo:Number(params.deptno) },
    });
    return json({dept});
};

export default function JokeRoute() {
    const data = useLoaderData();
  return (
    <div>
      <h3>{data.dept.deptNo}</h3>
      <p>{data.dept.dname}</p>
      <form method="post">
        <button className="button" name="intent" type="submit" value="delete">
          Delete
        </button>
      </form>
    </div>
  );
}

export const action = async ({params,request,}) => {
  const form = await request.formData();
  if (form.get("intent") !== "delete") {
    throw new Response(
      `The intent ${form.get("intent")} is not supported`,
      { status: 400 }
    );
  }
  // const userId = await requireUserId(request);
  
  await db.dept.delete({
    where: { deptNo:Number(params.deptno) },
  });
  return redirect("/dept");
};
