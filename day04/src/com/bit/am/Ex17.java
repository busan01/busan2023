package com.bit.am;

public class Ex17 {

	public static void main(String[] args) {
		// 순서o 중복허용
		// 순서x 중복불허
		java.util.List list=new java.util.ArrayList();
		list=new java.util.LinkedList();
		list=new java.util.Vector();
		
		list.add("첫번째");
		list.add("두번째");
		list.add("세번째");
		list.add("네번째");
		list.add("다섯번째");
		
		list.set(4, "5번째");
		list.remove(3);
		list.add(3, "4번째");
		System.out.println(list.get(2));
		System.out.println("---------------------------------");
		for(int i=0; i<list.size(); i++)
			System.out.println(list.get(i));
	}

}











