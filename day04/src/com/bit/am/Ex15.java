package com.bit.am;


class Node{
	Object val;
	Node nxt;
}

class BitLinked{
	Node begin;
	int cnt=0;
	
	int size() {
		return cnt;
	}
	
	void add(Object su) {
		cnt++;
		Node node=new Node();
		node.val=su;
		if(cnt==1)
			begin=node;
		else {
			Node temp=begin;
			while(true) {
				if(temp.nxt==null)break;
				temp=temp.nxt;
			}
			temp.nxt=node;
		}
	}
	
	Object get(int idx) {
		Node temp=begin;
		for(int i=0; i<idx; i++) {
			temp=temp.nxt;
		}
		return temp.val;
	}
}

public class Ex15 {

	public static void main(String[] args) {
		java.util.LinkedList list=new java.util.LinkedList();
//		BitLinked list=new BitLinked();
		list.add("첫번째");
		list.add("두번째");
		list.add("세번째");
		list.add("네번째");
		
		for(int i=0; i<list.size(); i++)
			System.out.println(list.get(i));

	}

}
