package com.bit.am;

class BitArray{
	Object[] data;
	int cnt;
	
	public BitArray() {
		data=new Object[1];
		cnt=0;
	}
	
	int size() {return cnt;}
	
	void add(Object su) {
		Object[] before=data;
		cnt++;
		data=new Object[cnt];
		for(int i=0; i<cnt-1; i++) {
			data[i]=before[i];
		}
		data[data.length-1]=su;
	}
	
	Object get(int idx) {
		return data[idx];
	}
	
}

public class Ex14 {

	public static void main(String[] args) {
		BitArray arr=new BitArray();
		arr.add("첫번째");
		arr.add("두번째");
		arr.add("세번째");
		arr.add("네번째");

		for(int i=0; i<arr.size(); i++) {
			System.out.println(arr.get(i));
		}
	}

}













