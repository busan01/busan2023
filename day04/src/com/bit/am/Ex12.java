package com.bit.am;

public class Ex12 {

	public static void main(String[] args) {
		// Q1. 학생성적관리프로그램(ver 0.0.1)
//		과목 : 국어 영어 수학
//		예시
//		총원>4
//		1.목록 2.입력 0.종료>1
//		---------------------------------------------
//		학번	| 국어		| 영어		| 수학 
//		---------------------------------------------
//		1			90			80			70
//		2			60			50			40
//		---------------------------------------------
//		1.목록 2.입력 0.종료>2
		java.util.Scanner sc=new java.util.Scanner(System.in);
		System.out.print("총원>");
		int input=sc.nextInt();
		int[][] data=new int[input][];
		int cnt=0;
		while(true) {
			System.out.print("1.목록 2.입력 0.종료>");
			input=sc.nextInt();
			if(input==0)break;
			if(input==1) {
				System.out.println("---------------------------------------------\r\n"
						+ "학번	| 국어	| 영어	| 수학 \r\n"
						+ "---------------------------------------------");
				for(int i=0; i<data.length; i++) {
					int[] stu=data[i];
					if(stu!=null)
					System.out.println(stu[0]+"\t"+stu[1]+"\t"+stu[2]+"\t"+stu[3]);
				}
			}
			if(input==2 && cnt<data.length) {
				System.out.print("국어>");
				input=sc.nextInt();
				int kor=input;
				System.out.print("영어>");
				input=sc.nextInt();
				int eng=input;
				System.out.print("수학>");
				input=sc.nextInt();
				int math=input;
				int[] stu= {cnt+1,kor,eng,math};
				data[cnt++]=stu;
			}
		}
	}

}









