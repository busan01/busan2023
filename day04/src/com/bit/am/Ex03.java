package com.bit.am;

class Lec03{
	public Lec03(int a) {
	}
	public void func01() {
		System.out.println("lec03-func01");
	}
}

public class Ex03 extends Lec03 {
	
	public Ex03() {
		super(1111);
//		func01();
	}

	//메서드 오버라이드
	@Override
	public void func01() {
		System.out.println("Ex03-func01");
	}

	public static void main(String[] args) {
		Ex03 me=new Ex03();
		me.func01();
	}

}
