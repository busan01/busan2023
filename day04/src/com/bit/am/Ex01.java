package com.bit.am;

class Lec01 extends Object{
	int money=1000;
	
	public static void func01() {
		System.out.println("static function");
	}
	
	public void func02() {
		System.out.println("non-static function");
//		func03();
	}
}
class Lec02{}
public class Ex01 extends Lec01 {
	public void func03() {}
	public static void main(String[] args) {
//		Lec01 lec=new Lec01();
//		System.out.println(lec.money);
//		lec.func02();
		func01();
		Ex01 me =new Ex01();
		System.out.println(me.money);
		me.func02();
	}

}
