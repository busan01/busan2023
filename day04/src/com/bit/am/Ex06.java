package com.bit.am;

//추상클래스 - 추상메서드를 0개이상 갖는 클래스
abstract class Lec06{
	int su=1111;
	
	public Lec06(int a) {
	
	}
	
	void func01() {
		System.out.println("부모1");
	}
	abstract void func02(); //추상메서드
}

public class Ex06 extends Lec06{

	public Ex06() {
		super(111111);
	}

	public static void main(String[] args) {
//		Lec06 lec=new Lec06();
//		Ex06 me=new Ex06();
//		me.func02();
//		Lec06 you=new Ex06();
//		you.func02();
	}

	@Override
	void func02() {
		System.out.println("자식이 구현한 기능");
	}

}









