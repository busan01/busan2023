package com.bit.am;

import java.util.Iterator;

public class Ex20 {

	public static void main(String[] args) {
		java.util.Set set=new java.util.HashSet();
		set=new java.util.TreeSet();
		set.add("첫번째");
		set.add("두번째");
		set.add("세번째");
		set.add("네번째");
		set.add("다섯번째");
		set.add(new Ex20());

		java.util.Iterator ite = set.iterator();
		while(ite.hasNext())
			System.out.println(ite.next());
	}

}
