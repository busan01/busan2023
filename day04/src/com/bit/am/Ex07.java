package com.bit.am;

interface Inter01 extends Inter03,Inter04{
	public static final int su1=1111;
	static final int su2=2222;
	final int su3=3333;
	int su4=4444;
	
	public abstract void func01();
	public void func02();
	void func03();
}

interface Inter02 extends Inter03,Inter04{
	int su1=1111;
	void func01();
}
interface Inter03{
	void func02();
}
interface Inter04{}

public class Ex07 extends Object implements Inter02 {
	public void func11() {}

	public static void main(String[] args) {
		Inter02 me=new Ex07();
		me.func01();
//		me.func11();
	}

	@Override
	public void func01() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void func02() {
		// TODO Auto-generated method stub
		
	}

}
