package com.bit.am;

import java.util.*;

public class Ex16 {

	public static void main(String[] args) {
		List list=new ArrayList();
		list=new LinkedList();
		
		for(int i=0; i<100000; i++)
			list.add("추가"+i);
		Object obj=null;
		long before=System.currentTimeMillis();
		for(int i=0; i<100000; i++)
			obj=list.get(i);
		long after=System.currentTimeMillis();
		System.out.println(after-before+"m/s");
	}

}
