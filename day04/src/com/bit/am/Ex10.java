package com.bit.am;

public class Ex10 {
	
	public void func01() {
		System.out.println("func");
	}

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
//		Ex10 me=new Ex10();
//		me.func01();

		String info="com.bit.am.Ex10";
		Class clz=Class.forName(info);
		Ex10 obj=(Ex10)clz.newInstance();
		obj.func01();
	}

}
