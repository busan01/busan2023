package com.bit.am;

public class Ex09 {

	public static void main(String[] args) {
		Object obj=new Object();
		Class clz=obj.getClass();
		Class clz2=Object.class;
		
		System.out.println(obj);
		System.out.println(obj.toString());
		System.out.println(obj.hashCode());
		System.out.println(Integer.toHexString(obj.hashCode()));
		System.out.println("-----------------------------------------------------------");
		Object obj2=new Object();
		System.out.println(obj==obj2);
		System.out.println(obj.equals(obj2));
		Ex09 me=new Ex09();
		Ex09 you=new Ex09();
		System.out.println(me==you);
		System.out.println(me.equals(you));
	}
	@Override
	public boolean equals(Object obj) {
		return true;
	}
}












