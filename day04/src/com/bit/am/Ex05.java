package com.bit.am;

abstract class Machine{
	void on() {System.out.println("켜다");}
	void off() {System.out.println("끄다");}
//	void work() {System.out.println("작동을 한다");}
	abstract void work();
}

class Tv extends Machine{
	@Override
	void work() {System.out.println("주파수를 잡아 화면을 보여주다");}
}

class Radio extends Machine{
	@Override
	void work() {System.out.println("주파수를 잡아 소리를 들려주다");}
}

class Audio extends Machine{
	@Override
	void work() {
		System.out.println("음악을 들려준다");
	}
}

public class Ex05 {

	public static void main(String[] args) {
		Machine remote=null;
		java.util.Scanner sc=new java.util.Scanner(System.in);
		System.out.print("1.tv 2.radio 3.audio 0.exit>");
		int input=sc.nextInt();
		if(input==1)
			remote=new Tv();
		else if(input==2)
			remote=new Radio();
		else if(input==3)
			remote=new Audio();
		
		remote.on();
		remote.work();
		remote.off();
	}

}











