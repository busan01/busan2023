package com.bit.am;
class Lec04{
	int su=1111;
	void func01() {
		System.out.println("부모기능1:"+su);
	}
	void func02() {
		System.out.println("부모기능2");
	}
}
public class Ex04 extends Lec04 {
	int su=2222;

	public static void main(String[] args) {
		Lec04 ex1=new Lec04();
		System.out.println(ex1.su);
		ex1.func01();
		Ex04 me=new Ex04();
		System.out.println(me.su);
		me.func01();
		// 다형성
		Lec04 ex2=new Ex04();
		System.out.println(ex2.su);
		ex2.func01();
//		Ex04 me2=new Lec04();
	}
	@Override
	void func01() {
		System.out.println("자식기능1:"+su);
	}
}










