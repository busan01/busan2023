package com.bit.sts06.model;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.bit.sts06.model.entity.Bbs01Vo;

@Mapper
public interface Bbs01Dao {

	@Select("select * from bbs01 order by num desc")
	List<Bbs01Vo> findAll();

	@Select("select * from bbs01 where num=#{num}")
	Bbs01Vo findOne(int num);
	
	@Insert("insert into bbs01 (id,sub,content,nalja) values (#{id},#{sub},#{content},now())")
	void insertOne(Bbs01Vo bean);
	
	@Update("update bbs01 set sub=#{sub},content=#{content} where num=#{num}")
	int updateOne(Bbs01Vo bean);
	
	@Delete("delete from bbs01 where num=#{num}")
	int deleteOne(int num);
}









