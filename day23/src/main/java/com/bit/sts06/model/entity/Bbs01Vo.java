package com.bit.sts06.model.entity;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bbs01Vo {
	private int num;
	private String id,sub,content;
	private Timestamp nalja;
}
