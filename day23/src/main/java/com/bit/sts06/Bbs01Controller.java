package com.bit.sts06;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bit.sts06.model.Bbs01Dao;
import com.bit.sts06.model.entity.Bbs01Vo;

@RestController
@RequestMapping("/bbs")
public class Bbs01Controller {
	@Autowired
	SqlSession sqlSession;
	
	@GetMapping("/")
	public List<Bbs01Vo> list(){
			Bbs01Dao dao=sqlSession.getMapper(Bbs01Dao.class);
			return dao.findAll();
	}
	
	@PostMapping("/")
	public String add(@ModelAttribute Bbs01Vo bean) {
		Bbs01Dao bbs01Dao=sqlSession.getMapper(Bbs01Dao.class);
		bbs01Dao.insertOne(bean);
		return "success";
	}
	@GetMapping("/{num}")
	public Bbs01Vo detail(@PathVariable int num) {
		Bbs01Dao bbs01Dao=sqlSession.getMapper(Bbs01Dao.class);
		return bbs01Dao.findOne(num);
	}
	
	@PutMapping("/{num}")
	public String update(@PathVariable int num, @RequestBody Bbs01Vo bean) {
		Bbs01Dao bbs01Dao=sqlSession.getMapper(Bbs01Dao.class);
		bbs01Dao.updateOne(bean);
		return "seccess";
	}
	
	@DeleteMapping("/{num}")
	public String delete(@PathVariable int num) {
		Bbs01Dao bbs01Dao=sqlSession.getMapper(Bbs01Dao.class);
		bbs01Dao.deleteOne(num);
		return "seccess";
	}
}












