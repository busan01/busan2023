package com.bit.boot06.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class APIController {

	@GetMapping("/")
	public List<String> list(){
		return List.of("item1","item2","item3","item4");
	}
	
	@GetMapping("/info")
	public String info() {
		return "user infomation...";
	}
}









