package com.bit.boot06.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

	@GetMapping("/intro")
	@ResponseBody
	public String intro() {
		return "intro page";
	}
	
	@GetMapping("/sign")
	public String login() {
		return "login";
	}
}








