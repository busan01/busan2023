package com.bit.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BbsDao {
	Connection conn;

	public BbsDao() {
		String driver="com.mysql.cj.jdbc.Driver";
		String url="jdbc:mysql://localhost:3306/xe";
		String user="scott";
		String password="tiger";
		try {
			Class.forName(driver);
			conn=DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			System.err.println("드라이버 확인하세요");
		} catch (SQLException e) {
			System.err.println("데이터베이스 접속정보를 확인하시고 db실행을 체크하세요");
		}
	}
	
	public List getList() throws SQLException {
		List<BbsDto> list=new ArrayList<BbsDto>();
		String sql="select num,sub,id,nalja from bbs01 order by num desc";
		try(
				Statement stmt=conn.createStatement();
				ResultSet rs=stmt.executeQuery(sql);
				){
			while(rs.next()) {
				BbsDto bean=new BbsDto();
				bean.setNum(rs.getInt(1));
				bean.setSub(rs.getString(2));
				bean.setId(rs.getString(3));
				bean.setNalja(rs.getDate(4));
				list.add(bean);
			}
		}
		return list;
	}

	public void insertOne(String sub, String id, String content) throws SQLException {
		String sql="insert into bbs01 (nalja,sub,id,content) values (now(),'"+sub+"','"+id+"','"+content+"')";
		try(Statement stmt=conn.createStatement()){
			stmt.executeUpdate(sql);
		}
		
	}

	public BbsDto selectOne(int num) throws SQLException {
		String sql="select * from bbs01 where num="+num;
		try(
				Statement stmt=conn.createStatement();
				ResultSet rs=stmt.executeQuery(sql);
				){
			if(rs.next()) {
				BbsDto bean=new BbsDto();
				bean.setNum(rs.getInt("num"));
				bean.setContent(rs.getString("content"));
				bean.setId(rs.getString("id"));
				bean.setNalja(rs.getDate("nalja"));
				bean.setSub(rs.getString("sub"));
				return bean;
			}
		}
		return null;
	}
}






