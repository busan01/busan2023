package com.bit.model;

import java.util.Date;

public class BbsDto {
	private int num;
	private String id;
	private String sub;
	private String content;
	private Date nalja;
	
	public BbsDto() {
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getNalja() {
		return nalja;
	}

	public void setNalja(Date nalja) {
		this.nalja = nalja;
	}

	@Override
	public String toString() {
		return "BbsDto [num=" + num + ", id=" + id + ", sub=" + sub + ", content=" + content + "]";
	}
	
}
