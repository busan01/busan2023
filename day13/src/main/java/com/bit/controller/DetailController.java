package com.bit.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bit.model.BbsDao;
import com.bit.model.BbsDto;

public class DetailController extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		BbsDao dao=new BbsDao();
		try {
			int num=Integer.parseInt(req.getParameter("num"));
			BbsDto bean= dao.selectOne(num);
			req.setAttribute("bean", bean);
		} catch (SQLException e) {
			System.err.println(e.toString());
		}
		req.getRequestDispatcher("/bbs/detail.jsp").forward(req, resp);
	}
}
