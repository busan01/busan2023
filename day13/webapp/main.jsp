<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="template/head.jspf" %>
<script type="text/javascript" src="js/carousel.js"></script>
<script type="text/javascript">
	
</script>
</head>
<body>
<%@ include file="template/menu.jspf" %>
		<div class="imgRolling">
			<ul>
				<li><img src="https://dummyimage.com/600x400/000/fff&text=image01"></li>
				<li><img src="https://dummyimage.com/600x400/000/fff&text=image02"></li>
				<li><img src="https://dummyimage.com/600x400/000/fff&text=image03"></li>
				<li><img src="https://dummyimage.com/600x400/000/fff&text=image04"></li>
				<li><img src="https://dummyimage.com/600x400/000/fff&text=image05"></li>
				<li><img src="https://dummyimage.com/600x400/000/fff&text=image06"></li>
			</ul>
		</div>	
<%@ include file="template/footer.jspf" %>
</body>
</html>