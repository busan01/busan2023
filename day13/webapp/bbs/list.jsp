<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../template/head.jspf" %>
<style type="text/css">
	#content table{
		width: 80%;
		margin: 10px auto;
		border-collapse: collapse;
	}
	#content table, #content table th, #content table td{
		border: 1px solid gray;
	}
	#content table th:nth-child(1){
		width: 80px;
	}
	#content table th:nth-child(3),#content table th:nth-child(4){
		width: 100px;
	}
	#content table td>a{
		display: block;
		color: #333333;
		text-decoration: none;
		height: 30px;
	}
</style>
</head>
<body>
<%@ include file="../template/menu.jspf" %>
<h1>게시판</h1>
<table>
	<thead>
		<tr>
			<th>글번호</th>
			<th>제목</th>
			<th>글쓴이</th>
			<th>날짜</th>
		</tr>
	</thead>
	<tbody>
	<%@page import="com.bit.model.BbsDto" %>
		<%
		java.util.List<BbsDto> list=(java.util.List<BbsDto>)request.getAttribute("alist");
		
		for(BbsDto bean : list){ 
		%>
		<tr>
			<td><a href="detail.do?num=<%=bean.getNum() %>"><%=bean.getNum() %></a></td>
			<td><a href="detail.do?num=<%=bean.getNum() %>"><%=bean.getSub() %></a></td>
			<td><a href="detail.do?num=<%=bean.getNum() %>"><%=bean.getId() %></a></td>
			<td><a href="detail.do?num=<%=bean.getNum() %>"><%=bean.getNalja() %></a></td>
		</tr>
		<%} %>
	</tbody>
</table>
<%@ include file="../template/footer.jspf" %>
</body>
</html>