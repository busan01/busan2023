<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../template/head.jspf" %>
<style type="text/css">
#content{}
#content>form{
	margin: 10px auto;
	width: 60%;
}
#content>form>div{}
#content>form>div>label{
	display: inline-block;
	width: 20%;
}
#content>form>div>input{
	width: 70%;
	border-width: 0px;
	border-bottom-width: 3px;
}
#content>form>div>textarea{
	width: 90%;
	height: 200px;
	margin-top: 30px;
}
#content>form>div>.btngroup{
	overflow: hidden;
	display: inline-block;
	width: 90%;
	border-radius: 20px;
}
#content>form>div>.btngroup>button{
	display:inline-block;
	width: 33%;
	height: 35px;
	float: left;
	background-color: white;
	border: 1px solid gray;
}
#content>form>div>.btngroup>button:first-child{
	border-radius: 20px 0px 0px 20px;
	background-color: skyblue;
}
#content>form>div>.btngroup>button:last-child{
	border-radius: 0px 20px 20px 0px;
	background-color: red;
}
</style>
</head>
<body>
<%@ include file="../template/menu.jspf" %>
<h1>입력페이지</h1>
<form method="post">
	<div>
		<label>제목</label><input name="sub"/>
	</div>
	<div>
		<label>글쓴이</label><input name="id"/>
	</div>
	<div>
		<textarea name="content"></textarea>
	</div>
	<div>
		<div class="btngroup">
		<button class="primary">입력</button>
		<button type="reset">취소</button>
		<button class="danger" type="button">뒤로</button>
		</div>
	</div>
</form>
<%@ include file="../template/footer.jspf" %>
</body>
</html>