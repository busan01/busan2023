<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@ include file="../template/head.jspf" %>
<style type="text/css">
	.table{
		width: 80%;
		margin: 10px auto;
	}
	.table>div{}
	.table>div>.label{
		display: inline-block;
		width: 100px;
		background-color: #999999;
		color: white;
	}
	.table>div>.label+span{
		display: inline-block;
	}
	.table>div:nth-child(2)>.label+span{
		text-indent: 30px;
	}
	.table>div:nth-child(3){
		height: 300px;
	}
	.table>div>a{
		display:inline-block;
		text-align: center;
		padding: 10px;
		width: 150px;
		border:1px solid gray;
		margin: 30px;
		text-decoration: none;
		color:#666666;
		background-color: #cccccc;
	}
</style>
<script type="text/javascript">
	$(()=>{
		$('.table>div>a').last().click(e=>{ window.history.back();return false;});
	});
</script>
</head>
<body>
<%@ include file="../template/menu.jspf" %>
<%com.bit.model.BbsDto bean=(com.bit.model.BbsDto) request.getAttribute("bean"); %>
<h1>상세페이지</h1>
<div>
	<div class="table">
		<div>
			<span class="label">글번호</span><span><%=bean.getNum() %></span>
			<span class="label">글쓴이</span><span><%=bean.getId() %></span>
			<span class="label">날짜</span><span><%=bean.getNalja() %></span>
		</div>
		<div>
			<span class="label">제목</span><span><%=bean.getSub() %></span>
		</div>
		<div>
			<%=bean.getContent() %>
		</div>
		<div>
			<a href="edit.do">수정</a>
			<a href="delete.do">삭제</a>
			<a href="#">뒤로</a>
		</div>
	</div>
</div>
<%@ include file="../template/footer.jspf" %>
</body>
</html>