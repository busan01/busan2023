$(document).ready(function(){
		$('<a href="#"/>').html('&gt;').prependTo('.imgRolling');
		$('<a href="#"/>').html('&lt;').prependTo('.imgRolling');
		
		$('.container>#content>.imgRolling').css({
			width: '600px',
			overflow: 'hidden',
			margin: '10px auto',
			position: 'relative'
		});
		$('.container>#content>.imgRolling>a').css({
			position: 'absolute',
			width:'50px',
			height: '400px',
			backgroundColor: 'white',
			top: '16px',
			opacity: '0.3',
			textAlign: 'center',
			textDecoration: 'none',
			lineHeight: '400px',
			fontWeight: 'bold',
			fontSize: '70px',
			color: 'black'
		});
		$('.container>#content>.imgRolling>a:nth-child(2)').css({
			left: '550px' 
		});
		$('.container>#content>.imgRolling>ul').css({
			listStyle: 'none',
			padding: '0px',
			width: '3600px'
		});
		$('.container>#content>.imgRolling>ul>li').css({
			float: 'left'
		});
		$('.container>#content>.imgRolling>ul>li>img').css({
			width: '600px'
		});
		
		
		$('.container>#content>.imgRolling>a').eq(1).click(function(e){
			var now=parseInt($(e.target).next().css('margin-left'));
			$(e.target).next().css('margin-left',now-600>-3600?now-600:0);
			return false;
		});
		$('.container>#content>.imgRolling>a').eq(0).click(function(e){
			var now=parseInt($(e.target).next().next().css('margin-left'));
			$(e.target).next().next().css('margin-left',now+600>0?-3000:now+600);
			return false;
		});

});