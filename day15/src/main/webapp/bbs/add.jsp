<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/body.css">
<style type="text/css">
.container{}
.container>form{
	display: block; width: 500px; margin: 0px auto; border: 1px solid gray;
	border-radius: 10px; padding: 50px; margin-top: 50px;
}
.container>form>div{
	border-bottom: 1px dotted gray; text-align: center; margin-top: 50px;
}
.container>form>div>label{ display: inline-block; width: 200px;}
.container>form>div>input{ width: 250px;}
.container>form>div>textarea{ width: 450px;}
.container>form>div>button{ width: 150px;}

</style>
</head>
<body>
<nav>
	<div>
		<h1>비트교육센터</h1>
	</div>
	<ul>
		<li><a href="../index.do">HOME</a></li>
		<li><a href="list.do">BBS</a></li>
		<li><a href="../login/form.do">LOGIN</a></li>
	</ul>
	<form>
		<label>id</label><input/>
		<label>pw</label><input type="password"/>
		<button>login</button>
		<button type="button">join</button>
	</form>
</nav>
<script type="text/javascript">
$(()=>{
	if(${view eq 'edit'? 'true':'false'}){
		$('.container input').prop('readonly',true);
		$('.container form').one('submit',function(e){
			$('.container input').removeProp('readonly');
			return false;
		});
	}
});
</script>
<div class="container">
	<form method="post" action="${view eq 'edit' ? 'edit':'add' }.do">
		<input type="hidden" name="num" value="${bean.num }">
		<div>
			<label>제목</label><input name="sub" value="${bean.sub }"/>
		</div>
		<div>
			<label>글쓴이</label><input name="id" value="${bean.id }"/>
		</div>
		<div>
			<textarea name="content">${bean.content }</textarea>
		</div>
		<div>
			<button>${view eq 'edit' ? '수정':'입력' }</button>
			<button type="reset">취소</button>
			<button type="button" onclick="history.back();">뒤로</button>
		</div>
	</form>
</div>
</body>
</html>