package com.bit.bbs02.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bit.bbs02.util.MysqlSingleton;

public class Bbs02Dao {

	public Bbs02Dao() {
	}
	
	public List<Bbs02Dto> selectAll() throws SQLException{
		String sql="select * from bbs01 order by num desc";
		List<Bbs02Dto> list=new ArrayList<>();
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			conn=MysqlSingleton.getConnection();
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next())
				list.add(new Bbs02Dto(
						rs.getInt("num")
						,rs.getString("id")
						,rs.getString("sub")
						,null
						,rs.getTimestamp("nalja")
						));
		}finally {
			if(rs!=null)rs.close();
			if(pstmt!=null)pstmt.close();
			if(conn!=null)conn.close();
		}
		
		return list;
	}
	
	public Bbs02Dto selectOne(int num) throws SQLException {
		String sql="select * from bbs01 where num=?";
		Bbs02Dto bean=new Bbs02Dto();
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			conn=MysqlSingleton.getConnection();
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			rs=pstmt.executeQuery();
			if(rs.next()) {
				bean.setContent(rs.getString("content"));
				bean.setId(rs.getString("id"));
				bean.setNalja(rs.getTimestamp("nalja"));
				bean.setNum(rs.getInt("num"));
				bean.setSub(rs.getString("sub"));
			}
		}finally {
			if(rs!=null)rs.close();
			if(pstmt!=null)pstmt.close();
			if(conn!=null)conn.close();
		}
		return bean;
	}
	
	public int insertOne(String id,String sub,String content) throws SQLException {
		String sql="insert into bbs01 (id,sub,content,nalja) values (?,?,?,now())";
		System.out.println(sql);
		Connection conn=null;
		PreparedStatement pstmt=null;
		try {
			conn=MysqlSingleton.getConnection();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.setString(2, sub);
			pstmt.setString(3, content);
			return pstmt.executeUpdate();
		}finally {
			if(pstmt!=null)pstmt.close();
			if(conn!=null)conn.close();
		}
	}
	
	public int updateOne(int num,String sub,String content) throws SQLException {
		String sql="update bbs01 set sub=?,content=? where num=?";
		Connection conn=null;
		PreparedStatement pstmt=null;
		try {
			conn=MysqlSingleton.getConnection();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, sub);
			pstmt.setString(2, content);
			pstmt.setInt(3, num);
			return pstmt.executeUpdate();
		}finally {
			if(pstmt!=null)pstmt.close();
			if(conn!=null)conn.close();
		}
	}
	
	public int deleteOne(int num) throws SQLException {
		String sql="delete from bbs01 where num=?";
		Connection conn=null;
		PreparedStatement pstmt=null;
		try {
			conn=MysqlSingleton.getConnection();
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, num);
			return pstmt.executeUpdate();
		}finally {
			if(pstmt!=null)pstmt.close();
			if(conn!=null)conn.close();
		}
	}
}
