package com.bit.bbs02.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class CtxListener implements ServletContextListener{

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println("contextListener init");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		System.out.println("contextListener destroy");
	}

}
