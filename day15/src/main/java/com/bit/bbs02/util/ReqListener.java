package com.bit.bbs02.util;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

public class ReqListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		System.out.println("req destroy");
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		System.out.println("req init");
	}


}
