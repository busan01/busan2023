package com.bit.bbs02.util;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

public class Ex02Filter implements javax.servlet.Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//System.out.println("Ex02Filter init...");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("before ex02 filter...");
		chain.doFilter(request, response);
		System.out.println("after ex02 filter...");
	}

	@Override
	public void destroy() {
		//System.out.println("Ex02Filter destory...");
	}

}
