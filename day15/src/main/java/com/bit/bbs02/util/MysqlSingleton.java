package com.bit.bbs02.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MysqlSingleton {
	static Logger log=Logger.getGlobal();
	
	private static Connection conn;
	private static String driver="com.mysql.cj.jdbc.Driver";
	private static String url="jdbc:mysql://localhost:3306/xe";

	private MysqlSingleton() {}
	
	public static Connection getConnection() {
		Map<String, String> env = System.getenv();
		String id=env.get("MYSQL_USER");
		String pw=env.get("MYSQL_PW");
		log.setLevel(Level.WARNING);
		log.info("user:"+id);
		log.info("pw:"+pw);
//		log.warning("주의");
//		log.severe("심각");
		try {
			Class.forName(driver);
			if(conn==null) {
					conn=DriverManager.getConnection(url,id,pw);
			}
			if(conn.isClosed()){
					conn=DriverManager.getConnection(url,id,pw);
			}
		} catch (SQLException e) {
			log.severe("드라이버 정보확인");
		} catch (ClassNotFoundException e) {
			log.severe("드라이버 확인");
		}
		
		return conn;
	}
}
