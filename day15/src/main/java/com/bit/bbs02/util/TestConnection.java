package com.bit.bbs02.util;

import java.sql.Connection;
import java.sql.SQLException;

public class TestConnection {

	public static void main(String[] args) throws SQLException {
		Connection conn=MysqlSingleton.getConnection();
		System.out.println("connection 객체 : "+(conn!=null));
		System.out.println("close 안됨 : "+!conn.isClosed());

	}

}
