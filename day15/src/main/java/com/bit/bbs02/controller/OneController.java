package com.bit.bbs02.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bit.bbs02.model.Bbs02Dao;

@WebServlet("/bbs/detail.do")
public class OneController extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int num=Integer.parseInt(req.getParameter("num"));
		Bbs02Dao dao=new Bbs02Dao();
		try {
			req.setAttribute("bean", dao.selectOne(num));
			req.setAttribute("view", "edit");
			req.getRequestDispatcher("add.jsp").forward(req, resp);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}








