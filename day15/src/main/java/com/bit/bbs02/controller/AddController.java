package com.bit.bbs02.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bit.bbs02.model.Bbs02Dao;

@WebServlet("/bbs/add.do")
public class AddController extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("add.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String sub=req.getParameter("sub");
		String id=req.getParameter("id");
		String content=req.getParameter("content");
		Bbs02Dao dao=new Bbs02Dao();
		try {
			dao.insertOne(id, sub, content);
		} catch (SQLException e) {
//			req.getRequestDispatcher("add.jsp").forward(req, resp);
		}
		resp.sendRedirect("list.do");
	}
}








