package com.bit.bbs02.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bit.bbs02.model.Bbs02Dao;

@WebServlet("/bbs/edit.do")
public class EditController extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String sub=req.getParameter("sub");
		String content=req.getParameter("content");
		int num=Integer.parseInt(req.getParameter("num"));
		Bbs02Dao dao=new Bbs02Dao();
		try {
			dao.updateOne(num, sub, content);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		resp.sendRedirect("list.do");
	}
}









