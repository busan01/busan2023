package com.bit.Netflix01;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/dept")
public class HomeController {

	@PostMapping("/")
	public ResponseEntity<?> home(@RequestParam(defaultValue = "") String msg) {
		return ResponseEntity.ok("[\r\n"
				+ "  {\r\n"
				+ "    \"deptNum\": 4,\r\n"
				+ "    \"dname\": \"user04\",\r\n"
				+ "    \"loc\": testarea\r\n"
				+ "  },\r\n"
				+ "  {\r\n"
				+ "    \"userId\": 5,\r\n"
				+ "    \"dname\": \"user05\",\r\n"
				+ "    \"loc\": testarea\r\n"
				+ "  }]");
	}
}
