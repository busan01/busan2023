import { Provider} from 'react-redux';
import './App.css';
import { App03 } from './components/App03';
import { App02 } from './components/App02';
import { App01 } from './components/App01';
import { store } from './components/context';

function App() {
  return (
    <Provider store={store}>
    <App01 />
    <App02 />
    <App03 />
    </Provider>
  );
}

export default App;
