import { useDispatch } from "react-redux";
import { DOWN } from "./context";

export function App03(){
    const dispatch=useDispatch();
    return (
      <>
        <div><button onClick={e=>dispatch({type:DOWN})}>-1</button></div>
      </>
    );
  }