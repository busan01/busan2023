import { createStore } from "redux";

export const UP='up';
export const DOWN='minus';

const reducer=(state={val:1,su:1},action)=>{
  if(action.type===UP) return {...state,val:state.val+action.su};
  if(action.type===DOWN) return {...state,val:state.val-1};
  return state;
};

export const store=createStore(reducer);