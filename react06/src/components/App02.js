import { useRef } from "react";
import { useDispatch } from "react-redux";


export function App02(){
    const ip=useRef();
    const dispatch=useDispatch();
    return (
      <>
        <div>
          <input ref={ip}/>
          <button onClick={e=>{
            const su=Number(ip.current.value);
            dispatch({type:'up',su})
            }}>+1</button>
        </div>
      </>
    );
  }