import Home from './routes/Home.svelte'
import Depts from './routes/Depts.svelte'
import Dept from './routes/Dept.svelte'
import NotFound from './routes/NotFound.svelte'
import InsertUser from './routes/InsertUser.svelte'

export default {
    // Exact path
    '/': Home,
    '/dept': Depts,
    '/dept/add': InsertUser,
    '/dept/:deptno': Dept,

    // Catch-all
    // This is optional, but if present it must be the last
    '*': NotFound,
}