package com.bit.boot03;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebConfig{
	
	@Bean
	HiddenHttpMethodFilter getFilter() {
		return new HiddenHttpMethodFilter();
	}

}
