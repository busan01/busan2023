package com.bit.boot03.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.bit.boot03.domain.Bbs02Entity;

@Mapper
public interface Bbs02Mapper {

	@Select("select * from bbs02")
	public List<Bbs02Entity> findAll();

	@Insert("insert into bbs02 (id,sub,content,nalja) values (#{id},#{sub},#{content},now())")
	public void save(Bbs02Entity bean);

	@Select("select * from bbs02 where num=#{num}")
	public Bbs02Entity findOne(int num);

	@Update("update bbs02 set sub=#{sub}, content=#{content} where num=#{num}")
	public int update(Bbs02Entity bean);
	
	@Delete("delete from bbs02 where num=#{num}")
	public int delete(int num); 
}
