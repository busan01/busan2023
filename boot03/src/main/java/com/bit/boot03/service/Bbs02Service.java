package com.bit.boot03.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.bit.boot03.domain.Bbs02Entity;
import com.bit.boot03.mapper.Bbs02Mapper;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class Bbs02Service {
	final SqlSessionFactory sqlSessionFactory;  
	
	public boolean getList(Model model) {
		try(
				SqlSession sqlSession=sqlSessionFactory.openSession();
				){
			Bbs02Mapper mapper = sqlSession.getMapper(Bbs02Mapper.class);
			model.addAttribute("list", mapper.findAll());
			return true;
		}catch (Exception e) {
			return false;
		}
	}

	public void addList(Bbs02Entity bean) {
		try(
				SqlSession sqlSession=sqlSessionFactory.openSession();
				){
			Bbs02Mapper mapper = sqlSession.getMapper(Bbs02Mapper.class);
			mapper.save(bean);
		}
		
	}

	public void pollList(int num, Model model) {
		try(
				SqlSession sqlSession=sqlSessionFactory.openSession();
				){
			Bbs02Mapper mapper=sqlSession.getMapper(Bbs02Mapper.class);
			model.addAttribute("bean", mapper.findOne(num));
		}
	}

	public void setList(Bbs02Entity bean) {
		try(
				SqlSession sqlSession=sqlSessionFactory.openSession();
				){
			Bbs02Mapper mapper=sqlSession.getMapper(Bbs02Mapper.class);
			mapper.update(bean);
		}
	}

	public void removeList(int num) {
		try(
				SqlSession sqlSession=sqlSessionFactory.openSession();
				){
			Bbs02Mapper mapper=sqlSession.getMapper(Bbs02Mapper.class);
			mapper.delete(num);
		}
	}

}











