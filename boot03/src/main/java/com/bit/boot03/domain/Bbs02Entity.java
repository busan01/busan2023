package com.bit.boot03.domain;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bbs02Entity {
	private long num;
	private String id,sub,content;
	private Timestamp nalja;
}
