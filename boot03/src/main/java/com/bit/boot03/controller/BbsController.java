package com.bit.boot03.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bit.boot03.domain.Bbs02Entity;
import com.bit.boot03.service.Bbs02Service;

import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping("/bbs")
@RequiredArgsConstructor
public class BbsController {
	final Bbs02Service bbs02Service;

	@GetMapping("/")
	public String list(Model model) {
		bbs02Service.getList(model);
		return "bbs/index";
	}
	
	@GetMapping("/add")
	public String add() {
		return "bbs/add";
	}
	
	@PostMapping("/")
	public String add(@ModelAttribute Bbs02Entity bean) {
		bbs02Service.addList(bean);
		return "redirect:./";
	}
	
	@GetMapping("/{num}")
	public String detail(@PathVariable int num, Model model) {
		bbs02Service.pollList(num,model);
		return "bbs/detail";
	}
	
	@PutMapping("/{num}")
	public String edit(@PathVariable int num, @ModelAttribute Bbs02Entity bean) {
		bbs02Service.setList(bean);
		return "redirect:./";
	}
	
	@DeleteMapping("/{num}")
	public String delete(@PathVariable int num) {
		bbs02Service.removeList(num);
		return "redirect:./";
	}
}









