package com.bit.Netflix04;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;

public class GgatewayFilter extends AbstractGatewayFilterFactory{

	@Override
	public GatewayFilter apply(Object config) {
		return (serverWebExchange,gatewayFilterChain)->{
			ServerHttpRequest req = serverWebExchange.getRequest();
			ServerHttpResponse res = serverWebExchange.getResponse();
			System.out.println(req.getMethodValue());
			return gatewayFilterChain.filter(serverWebExchange);
		};
	}

}
