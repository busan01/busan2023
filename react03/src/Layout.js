import React from 'react'
import { Link, Outlet } from 'react-router-dom'

function Layout() {
  return (
    <>
      <nav className="navbar  navbar-inverse">
        <div className="container-fluid">
            <div className="navbar-header">
            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
            </button>
            <Link to='/' className="navbar-brand">비트교육센터</Link>
            </div>

            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav">
                <li className="active"><Link to='/'>Home <span className="sr-only">(current)</span></Link></li>
                <li><Link to='/intro'>Intro</Link></li>
                <li><Link to='/bbs'>BBS</Link></li>
            </ul>
            
            </div>
        </div>
        </nav>
        <div className='container'>
            <div className="row">
                <div className="col-md-12">
                    <Outlet />
                </div>
            </div>
            <div className="row">
                <div className="col-md-12">
                <address>
                <strong>Copyright by bitacademy co.ltd. All rights reserved.</strong><br/>
                서울특별시 서초구 서초대로74길33 비트빌 3층<br/>
                </address>
                </div>
            </div>
        </div>
    </>
  )
}

export default Layout