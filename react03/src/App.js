import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Layout from './Layout';
import Home from './page/Home';
import Intro from './page/Intro';
import List from './page/List';
import Add from './page/Add';
import Detail from './page/Detail';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="intro" element={<Intro />} />
          <Route path="bbs/add" element={<Add />} />
          <Route path="bbs/:num" element={<Detail />} />
          <Route path="bbs/" element={<List />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
