import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

function List() {
    const [list,setList]=useState([]);
    const [load1,setLoad1]=useState(true);

    useEffect(()=>{
        fetch('/bbs/v1/')
        .then(response => response.json())
        .then(data =>setList(data))
        .finally(setLoad1(false))
    },[list]);

  return (
      <>
        <div className="page-header">
        <h1>게시판{load1&&<span>loading...</span>}</h1>
        </div>
        <div className="list-group">
        <div href="#" className="list-group-item active">
            <h4 className="list-group-item-heading">subject</h4>
            <p className="list-group-item-text">content</p>
        </div>
        {list.map((ele,idx)=>
        <Link key={ele.num} to={'/bbs/'+ele.num} className="list-group-item">
            <h4 className="list-group-item-heading">{ele.subject}</h4>
            <p className="list-group-item-text">
                {ele.content.length>10
                ?ele.content.substring(0,10)+'...':ele.content}
            </p>
        </Link>
        )}
        </div>
        <Link to={'add'} className='btn btn-primary btn-block' role='button' >입력</Link>
    </>
  )
}

export default List