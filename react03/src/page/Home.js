import React from 'react'
import Img01 from '../imgs/img01.jpg'
import Img02 from '../imgs/img02.jpg'
import Img03 from '../imgs/img03.jpg'

function Home() {
  return (
    <>
        <div id="carousel-example-generic" className="carousel slide" data-ride="carousel">     
        <ol className="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" className="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>
 
        <div className="carousel-inner" role="listbox">
            <div className="item active">
                <img src={Img01} alt="..."/>
            </div>
            <div className="item">
                <img src={Img02} alt="..."/>
            </div>
            <div className="item">
                <img src={Img03} alt="..."/>
            </div>
        </div>

        
        <a className="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
        </a>
        <a className="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
        </a>
        </div>
    </>
  )
}

export default Home