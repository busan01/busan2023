import React, { useEffect, useState } from 'react'
import UserInput from '../component/UserInput'
import UserTextarea from '../component/UserTextarea'
import UserFormBtn from '../component/UserFormBtn'
import { useNavigate, useParams } from 'react-router-dom'

function Detail() {
    const navigate=useNavigate();
    const [read,setRead]=useState('true');
    const [sub,setSub]=useState('-');
    const [writer,setWriter]=useState('-');
    const [content,setContent]=useState('-');
    const {num}=useParams();
    useEffect(()=>{
        fetch('/bbs/v1/'+num)
            .then(data=>data.json())
            .then(json=>{
                setSub(json.subject);
                setWriter(json.id);
                setContent(json.content);
            });
    },[]);
    const UserSubmit=e=>{
        e.preventDefault(); 
        if(read){
            setRead(!read);
        }else{
            fetch('/bbs/v1/'+num,{
                method:'PUT',
                headers:{
                    'Content-Type':'application/json'
                },
                body:JSON.stringify({num,subject:sub,id:writer,content})
            }).then(e=>e.json()).then(data=>console.log(data))
            .finally(()=>navigate('/bbs/'));
        }
    }
  return (
    <form onSubmit={UserSubmit}>
        <UserInput val={sub} setVal={setSub} read={read} inputName={'sub'} />
        <UserInput val={writer} setVal={setWriter} read={read} inputName={'id'}/>
        <UserTextarea val={content} setVal={setContent} read={read}/>
        <UserFormBtn action={read} num={num}/>
    </form>
  )
}

export default Detail