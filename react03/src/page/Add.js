import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import UserInput from '../component/UserInput';
import UserTextarea from '../component/UserTextarea';
import UserFormBtn from '../component/UserFormBtn';

function Add() {
    const [sub,setSub] =useState('');
    const [writer,setWriter] =useState('');
    const [content,setContent] =useState('');
    const navigate=useNavigate();
    const pushList=e=>{
        e.preventDefault();
        let param='subject='+sub+'&id='+writer+'&content='+content;
        console.log(param);
        fetch('/bbs/v1/',{
            method:"POST",
            headers:{
                'Content-Type':'application/x-www-form-urlencoded'
            },
            body:param
        }).then(result=>result.json())
        .then(json=>console.log(json))
        .catch(e=>console.log(e))
        .finally(navigate('/bbs/'));
    }
  return (
    <>
        <div className="page-header">
        <h1>입력페이지</h1>
        </div>
        <form onSubmit={pushList} className="form-horizontal">
        <UserInput inputName='sub' val={sub} setVal={setSub}/>
        <UserInput inputName='id' val={writer} setVal={setWriter}/>
        <UserTextarea inputName={'content'} val={content} setVal={setContent} />
        <UserFormBtn/>
        </form>
    </>
  )
}

export default Add