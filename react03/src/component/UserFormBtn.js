import React from 'react'
import { useNavigate } from 'react-router-dom';

function UserFormBtn({action,num}) {
    const navigate=useNavigate();
    const hisback=e=>{
        navigate(-1);
    }
    const delBtn=e=>{
      if(action){
        fetch('/bbs/v1/'+num,{
          method:'delete',
          headers:{
            'Content-Type':'application/json'
          }
        })
        .then(data=>data.json())
        .then(json=>console.log(json))
        .finally(()=>{navigate('/bbs/')});
      }
    }
  return (
    <div className="form-group">
        <div className="col-sm-offset-2 col-sm-10">
        <button type="submit" className="btn btn-primary">{action==null?'입력':'수정'}</button>
        <button onClick={delBtn} 
          type={action==null?'reset':action?'button':'reset'} 
          className={action?"btn btn-danger":"btn btn-default"}>{action==null?'취소':action?'삭제':'취소'}</button>
        <button onClick={hisback} type="button" 
          className="btn btn-default">뒤로</button>
        </div>
    </div>
  )
}

export default UserFormBtn