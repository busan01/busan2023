import React from 'react'
import inputChange from './InputChange'

function UserInput({inputName,val,setVal,read}) {
  return (
    <div className="form-group">
        <label className="col-sm-2 control-label">{inputName}</label>
        <div className="col-sm-10">
        <input readOnly={read &&'readonly'} 
          onChange={e=>{inputChange(setVal,e)}} 
          value={val} className="form-control" name={inputName}/>
        </div>
    </div>
  )
}

export default UserInput