import React from 'react'
import inputChange from './InputChange'

function UserTextarea({inputName,val,setVal,read}) {

  return (
    <div className="form-group">
        <div className="col-sm-offset-2 col-sm-10">
        <textarea readOnly={read &&'readonly'} 
        onChange={e=>inputChange(setVal,e)} 
        value={val} className="form-control" name={inputName}/>
        </div>
    </div>
  )
}

export default UserTextarea