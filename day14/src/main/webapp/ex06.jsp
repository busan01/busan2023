<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>제어문1 조건문</h1>
	<c:set var="var1" value="true"></c:set>
	<c:if test="${var1 }">
	<div>오직 if문만..</div>
	</c:if>
	
	<c:choose>
		<c:when test="${false }">1번째</c:when>
		<c:when test="${false }">2번째</c:when>
		<c:when test="${false }">3번째</c:when>
		<c:otherwise>이도저도 아니면</c:otherwise>
	</c:choose>
</body>
</html>









