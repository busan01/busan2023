<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>자료타입별 전달</h1>
	<ul>
		<li>${arr1[0] }</li>
		<li>${arr1[1] }</li>
		<li>${arr1[2] }</li>
	</ul>
	<ol>
		<li>${arr2[0] }</li>
		<li>${arr2[1] }</li>
		<li>${arr2[2] }</li>
	</ol>
	<ul>
		<li>${arr2.get(0) }</li>
		<li>${arr2.get(1) }</li>
		<li>${arr2.get(2) }</li>
	</ul>
	<div>${arr3 }</div>
	<div>${hmap }</div>
	<ul>
		<li>${hmap.key1 }</li>
		<li>${hmap.key2 }</li>
		<li>${hmap.key3 }</li>
		<li>${hmap.key4 }</li>
	</ul>
	<div>${bean }</div>
	<ul>
		<li>${bean.num }</li>
		<li>${bean.id }</li>
		<li>${bean.sub }</li>
		<li>${bean.content }</li>
		<li>${bean.nalja }</li>
	</ul>
</body>
</html>








