<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%	
	pageContext.setAttribute("msg", "val1");	//1
	request.setAttribute("msg", "val2");		//2
	session.setAttribute("msg", "val3");		//3
	application.setAttribute("msg", "val4");	//4
%>
<ul>
	<li>${msg }</li>
	<li>${pageScope.msg }</li>
	<li>${requestScope.msg }</li>
	<li>${sessionScope.msg }</li>
	<li>${applicationScope.msg }</li>
</ul>
<div>${pageContext }</div>
<div>${pageContext.request }</div>
<div>${pageContext.request.servletContext.contextPath }</div>
<div><%=request.getContextPath() %></div>
</body>
</html>







