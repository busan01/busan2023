<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
String msg="문자열";
pageContext.setAttribute("msg2", msg);
%>
<jsp:useBean id="nalja" class="java.util.Date" scope="page"></jsp:useBean>
	<h1>el 표현식</h1>
	<p>msg:<%=pageContext.getAttribute("msg2") %></p>
	<p>msg:${msg2 }</p>
	<p>msg3:${msg3 }</p>
	<p>msg4:${msg4 }</p>
	<p>msg4:<%=session.getAttribute("msg4") %></p>
	<p>nalja:${nalja }</p>
	<p>obj:${obj }</p>
</body>
</html>