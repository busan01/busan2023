<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>jstl lib</h1>
	<ul>
		<li><c:out value="1234">텍스트노드</c:out> </li>
		<li><c:out value="홍길동">텍스트노드</c:out> </li>
		<li><c:out value="true">텍스트노드</c:out> </li>
		<li><c:out value="null">1234</c:out> </li>
		<li><c:out value="false">홍길동</c:out> </li>
		<li><c:out value="0">true</c:out> </li>
		<li><c:out value="${null }">1234</c:out> </li>
		<li><c:out value="${false }">홍길동</c:out> </li>
		<li><c:out value="${arr }">true</c:out> </li>
	</ul>
	<ul>
		<li><c:set var="var1" value="1234"></c:set></li>
		<li><c:set var="var2" value="3.14"></c:set></li>
		<li><c:set var="var3" value="true"></c:set></li>
		<li><c:out value="var1"></c:out></li>
		<li><c:out value="${var1 }"></c:out></li>
		<li><c:out value="${arr1 }">대체</c:out></li>
		<li>${arr1 eq null ? "대체":arr1 }</li>
		<li>${var1+1 }</li>
		<li>${var2+1 }</li>
		<li>${var3 && true }</li>
		<li>${"문자" }${ "열" }</li>
	</ul>
</body>
</html>