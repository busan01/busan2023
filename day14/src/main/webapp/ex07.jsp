<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
pageContext.setAttribute("arr1"
		,new String[]{"item1","item2","item3"});
pageContext.setAttribute("arr2"
		,java.util.Arrays.asList("item1","item2","item3"));
pageContext.setAttribute("arr3"
		,new java.util.HashSet(java.util.Arrays.asList("item1","item2","item3")));
java.util.Map<String,String> map;
map=new java.util.HashMap<>();
map.put("key1", "val1");
map.put("key2", "val2");
map.put("key3", "val3");
map.put("key4", "val4");
pageContext.setAttribute("map", map);

String msg="java,web,db,,framework";
pageContext.setAttribute("arr4", msg);
%>
	<h1>반복문</h1>
	<ul>
	<c:forEach begin="1" end="5" var="i">
		<li>반복${i }</li>
	</c:forEach>		
	</ul>
	<ul>
		<c:forEach items="${arr1 }" var="ele">
		<li>반복-${ele }</li>
		</c:forEach>
	</ul>
	<ul>
		<c:forEach items="${arr2 }" var="ele">
		<li>반복-${ele }</li>
		</c:forEach>
	</ul>
	<ul>
		<c:forEach items="${arr3 }" var="ele">
		<li>반복-${ele }</li>
		</c:forEach>
	</ul>
	<ul>
		<c:forEach items="${map }" var="ele">
		<li>${ele } - ${ele.key } : ${ele.value }</li>
		</c:forEach>
	</ul>
	<ul>
	<c:forTokens items="${arr4 }" delims="," var="str">
		<li>${str }</li>	
	</c:forTokens>
	</ul>
</body>
</html>







