<%@page import="javax.print.attribute.standard.PagesPerMinuteColor"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
pageContext.setAttribute("su", 5);
pageContext.setAttribute("msg", null);
%>
	<h1>EL 표현식</h1>
	<table width='80%' border="1">
		<tr>
			<td>자료형</td>
			<td>jsp표현</td>
			<td>표기법</td>
			<td>EL</td>
		</tr>
		<tr>
			<td>10진수 정수</td>
			<td><%=1234+1 %></td>
			<td>＄{su+1}</td>
			<td>${su+1}</td>
		</tr>
		<tr>
			<td>10진수 실수</td>
			<td><%=3.14+1 %></td>
			<td>＄{3.14+1}</td>
			<td>${3.14+1 }</td>
		</tr>
		<tr>
			<td>+</td>
			<td><%=1+2 %></td>
			<td>＄{1+2}</td>
			<td>${1+2 }</td>
		</tr>
		<tr>
			<td>-</td>
			<td><%=6-2 %></td>
			<td>＄{6-2}</td>
			<td>${6-2 }</td>
		</tr>
		<tr>
			<td>x</td>
			<td><%=6*2 %></td>
			<td>＄{6*2}</td>
			<td>${6*2 }</td>
		</tr>
		<tr>
			<td>/</td>
			<td><%=6/2 %></td>
			<td>＄{6/2}</td>
			<td>${6/2 }</td>
		</tr>
		</tr>
		<tr>
			<td>div</td>
			<td><%=6/2 %></td>
			<td>＄{6 div 2}</td>
			<td>${6 div 2 }</td>
		</tr>
		<tr>
			<td>%</td>
			<td><%=5%2 %></td>
			<td>＄{5%2}</td>
			<td>${5%2 }</td>
		</tr>
		<tr>
			<td>mod</td>
			<td><%=5%2 %></td>
			<td>＄{su mod 2}</td>
			<td>${su mod 2 }</td>
		</tr>
		<tr>
			<td>비교 ==</td>
			<td><%=4==4 %></td>
			<td>＄{4 == 4}</td>
			<td>${4==4 }</td>
		</tr>
		<tr>
			<td>비교 eq</td>
			<td><%=4==4 %></td>
			<td>＄{su eq 5}</td>
			<td>${su eq 5 }</td>
		</tr>
		<tr>
			<td>비교 &gt; </td>
			<td><%=5>3 %></td>
			<td>＄{ su gt 3}</td>
			<td>${su gt 2 }</td>
		</tr>
		<tr>
			<td>비교 &lt; </td>
			<td><%=5<3 %></td>
			<td>＄{ su lt 3}</td>
			<td>${su lt 2 }</td>
		</tr>
		<tr>
			<td>비교 &lt;= </td>
			<td><%=5<=3 %></td>
			<td>＄{ su le 3}</td>
			<td>${su le 2 }</td>
		</tr>
		<tr>
			<td>비교 &gt;= </td>
			<td><%=5>=3 %></td>
			<td>＄{ su ge 3}</td>
			<td>${su ge 2 }</td>
		</tr>
		<tr>
			<td>문자열</td>
			<td><%="문자열" %></td>
			<td>＄{'"문자"열'}</td>
			<td>${'"문자"열' }</td>
		</tr>
		<tr>
			<td>문자열</td>
			<td><%="" %></td>
			<td>＄{''}</td>
			<td>${'' }</td>
		</tr>
		<tr>
			<td>객체</td>
			<td><%="null" %></td>
			<td>＄{msg2}</td>
			<td>${msg2 }</td>
		</tr>
		<tr>
			<td>boolean</td>
			<td><%=true %></td>
			<td>＄{true}</td>
			<td>${true }</td>
		</tr>
	</table>
</body>
</html>