package com.bit.controller;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bit.model.BbsDto;

public class ElTestController extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute("arr1", new String[] {"item1","item2","item3"});
		java.util.List<String> list=java.util.Arrays.asList("item1","item2","item3");
		req.setAttribute("arr2", list);
		java.util.Set<String> set=new java.util.HashSet(list);
		req.setAttribute("arr3", set);
		java.util.Map<String,String> map=new java.util.HashMap<>();
		map.put("key1", "val1");
		map.put("key2", "val2");
		map.put("key3", "val3");
		map.put("key4", "val4");
		req.setAttribute("hmap",map);
		
		BbsDto bean=new BbsDto();
		bean.setNum(1234);
		bean.setId("guest");
		bean.setSub("제목없음");
		bean.setNalja(new Timestamp(System.currentTimeMillis()));
		req.setAttribute("bean", bean);
		
		req.getRequestDispatcher("ex03.jsp").forward(req, resp);
	}
}









