package com.bit.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//@WebServlet(value = {"/ex09.do","/ex09.bit"})
//@WebServlet(value = "/ex09.do")
//@WebServlet("/ex09.do")
@WebServlet(value = {"/ex09.do"}
	,initParams = {
			@WebInitParam(name = "param1",value = "value1")
			,@WebInitParam(name = "param2",value = "value2")
			})
public class Ex09Controller extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String val1=this.getInitParameter("param1");
		String val2=this.getInitParameter("param2");
		System.out.println(val1);
		System.out.println(val2);
		req.getRequestDispatcher("ex01.jsp").forward(req, resp);
	}
}
