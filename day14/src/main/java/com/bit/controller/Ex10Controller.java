package com.bit.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Ex10Controller extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Enumeration<String> enu = this.getInitParameterNames();
		while(enu.hasMoreElements())
			System.out.println(enu.nextElement());
		String val1=this.getInitParameter("param1");
		String val2=this.getInitParameter("param2");
		System.out.println(val1);
		System.out.println(val2);
		ServletContext ctxt = req.getServletContext();
		Enumeration<String> enu2 = ctxt.getInitParameterNames();
		
		String val4=ctxt.getInitParameter("param4");
		System.out.println(val4);
		req.getRequestDispatcher("ex02.jsp").forward(req, resp);
	}
}











