package com.bit.boot05.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {

	@GetMapping("/")
	public String info1() {
		return "info1";
	}
	
	@GetMapping("/2")
	public String info2() {
		return "info2";
	}
	
	@GetMapping("/3")
	public String info3() {
		return "info3";
	}
}
