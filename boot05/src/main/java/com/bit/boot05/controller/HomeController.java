package com.bit.boot05.controller;
import java.net.HttpCookie;

import javax.servlet.http.Cookie;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

@RestController

public class HomeController {

	@RequestMapping("/")
	public String index() {
		
		return "test page";
	}
	@RequestMapping("/info")
	public String tokenInfo(String token) {
		DecodedJWT decodedJWT=null;
		try {
		    Algorithm algorithm = Algorithm.HMAC256("123456789abcdefg");
		    JWTVerifier verifier = JWT.require(algorithm).build(); //Reusable verifier instance
		    DecodedJWT jwt = verifier.verify(token);
		    return jwt.getClaim("username").asString()+"<br/>"+jwt.getSubject();
		} catch (JWTVerificationException exception){
		    // Invalid signature/claims
		}
		return null;
	}
//	@RequestMapping("/create2")
//	public String getToken2(Cookie cookie) {
//		cookie.setValue(getToken())
//	}
	
	@RequestMapping("/create")
	public String getToken() {
		String token=null;
		try {
		    Algorithm algorithm = Algorithm.HMAC256("123456789abcdefg");
		    token = JWT.create()
		    	.withSubject("mySubject")
		        .withIssuer("tiger")
		        .withClaim("username","scott")
		        .withExpiresAt(new java.util.Date(System.currentTimeMillis()+1000*60*5))
		        .sign(algorithm);
		} catch (JWTCreationException exception){
		    // Invalid Signing configuration / Couldn't convert Claims.
		}
		return token;
	}
}
