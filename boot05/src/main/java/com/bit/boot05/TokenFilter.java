package com.bit.boot05;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.filter.OrderedFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

public class TokenFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req=(HttpServletRequest) request;
		HttpServletResponse res=(HttpServletResponse) response;
		
//		req.getCookies()
		
		if(req.getRequestURI().startsWith("/api/")) {
			String auth = req.getHeader("Authorization");
			String token=auth.replace("Bearer ", "");
			DecodedJWT decodedJWT=null;
			String username="guest";
			long limit=0;
			try {
			    Algorithm algorithm = Algorithm.HMAC256("123456789abcdefg");
			    JWTVerifier verifier = JWT.require(algorithm).build(); //Reusable verifier instance
			    DecodedJWT jwt = verifier.verify(token);
			    username=jwt.getClaim("username").asString();
			    limit=jwt.getExpiresAt().getTime();
			} catch (JWTVerificationException exception){
			}
				if(username.equals("scott")&&limit>=System.currentTimeMillis()) {
					chain.doFilter(request, response);
				}else{
					res.sendError(400);
				}
		}else {
			chain.doFilter(request, response);
		}
	}

}
