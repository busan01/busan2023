'use client'
import { useRouter } from "next/navigation";

export default function Page() {
    const router = useRouter()
    return (
        <>
            <h2>/step01 page</h2>
            <button onClick={()=>{router.push('/step01/ex01')}}>next</button>
        </>
    );
}