import Link from "next/link";

export default function Step01Layout({
    children, // will be a page or nested layout
  }) {
    return (
      <section>
        {/* Include shared UI here e.g. a header or sidebar */}   
        {children}
      </section>
    )
  }