import { memo } from "react";
// props 변화를 확인후 재랜더링을 결정
const Todos = ({ todos,addTodo }) => {
    console.log("child render");
    return (
        <>
        <h2>My Todos</h2>
        {todos.map((todo, index) => {
          return <p key={index}>{todo}</p>;
        })}
        <button onClick={addTodo}>Add Todo</button>
      </>
    );
  };
  
  export default memo( Todos);