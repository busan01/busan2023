import React, { useRef, useState } from 'react'

function Ex03() {
    const [msg,setMsg]=useState();
    var inn=useRef();
    let su=useRef(1234);
  return (
    <>
        <input value={msg} onChange={e=>setMsg(e.target.value)}/>
        <button onClick={e=>console.log(su.current++)}>클릭</button>
    </>
  )
}

export default Ex03