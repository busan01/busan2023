import React, { useReducer, useState } from 'react'
import { init,reducer } from './myReducer';


function Ex04() {
    const [num, dispatch]=useReducer(reducer,init);
  return (
    <>
        <h1>{num.su}</h1>
        <button onClick={e=>{
        dispatch({type:'plus',payload:1});
        }}>+1</button>
        <button onClick={e=>{
        dispatch({type:'minus'});
        }}>-1</button>
    </>
  )
}

export default Ex04